import numpy as np
from matplotlib import pyplot

from pipeline import ReadDefaults
from pipeline.Tools import Parsing
from pipeline.Observatory.RFI import Satellites
import sys
import time

from mpi4py import MPI

from pipeline.Observatory.Receivers import Noise
from pipeline.Observatory.Sky import Atmosphere

from pipeline.Tools import MPITools
from pipeline.Tools import FileTools
from pipeline.Tools import Filtering


import pipeline.Control as Control
from pipeline.MapMaking.Naive import NaiveMapMaker

T_cmb = 0. #2.73

def MPISum2Root(dshare, droot, Nodes):
    # Switch on MPI 
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()


    if rank == 0:
        droot += dshare
    for node in Nodes:
        if node == 0:
            continue

        if rank == node:
            comm.Send([dshare, MPI.DOUBLE], dest=0, tag=node)
        if rank == 0:
            comm.Recv([dshare, MPI.DOUBLE], source=node, tag=node)
            droot += dshare

def main(paramFile, defaultFile=None):
    comm = MPI.COMM_WORLD
    rank = comm.rank
    size = comm.size 
    
    MPITools.AssignRandomSeeds()
    
    # Overwrite with the user parameter file
    Parameters = Parsing.Parser(paramFile)
    for key, value in ReadDefaults.DefaultParameters(defaultFile).infodict.items():
        if key not in Parameters.infodict: # Checks that headers are there
            Parameters[key] = value # Assigns all defaults
        else: 
            for k2, v2 in value.items():
                if k2 not in Parameters[key]:
                    Parameters[key][k2] = v2
                
    #############################################################
    # CHECKS DIRECTORY STRUCTURE, CREATES DIRECTORIES IF NEEDED #
    #############################################################
    Control.GenerateFileStructure()

    #################################################################
    # simInfo is a class that keeps tabs on where all the data is for each node. #
    #################################################################    
    simInfo = Control.SimInfo(Parameters)
    
    
    if rank == 0:
        print('BEGIN!')

    #####---------------------------------------------------######
    #####---------------------------------------------------######
    #  THIS SECTION CONTROL INPUT MAP GENERATION + CONVOLUTIONS  #
    #####%%%%%%%%%%%%%%----------------------%%%%%%%%%%%%%%%######        
    #####%%%%%%%%%%%%%%%%%%%%%%%----%%%%%%%%%%%%%%%%%%%%%%%%###### 
    Sky = Control.GenerateSky(simInfo)
    
    comm.Barrier() # Let everyone catch up after generating the Sky

    #####---------------------------------------------------######
    #####---------------------------------------------------######
    #  HERE WE WILL JUST READ IN THE SATELLITE TLEs IF REQUIRED  #
    #####%%%%%%%%%%%%%%----------------------%%%%%%%%%%%%%%%######        
    #####%%%%%%%%%%%%%%%%%%%%%%%----%%%%%%%%%%%%%%%%%%%%%%%%###### 
    if simInfo.Parameters['Inputs']['sim_RFI']:
        # Create satellite object
        SatExec = Satellites.Satellites(simInfo.nu/1e6, # input should be MHz 
                                        simInfo.maxBlockSize,
                                        simInfo.Parameters['RFI']['minAngle'],
                                        simInfo.Parameters['RFI']['gnssfiles'])

    #####----------------------------------------------------------######
    #####----------------------------------------------------------######
    # THIS SECTION OF THE CODE DEALS WITH TOD AND OUTPUT MAP GENERATION #
    #####%%%%%%%%%%%%%%-----------------------------%%%%%%%%%%%%%%%######        
    #####%%%%%%%%%%%%%%%%%%%%%%%-----------%%%%%%%%%%%%%%%%%%%%%%%%######     
    writeTsysFile   = '{}/{}_{}_Tsys_Sky.fits'.format(simInfo.Parameters['Outputs']['outputdir'], simInfo.Parameters['Inputs']['sim_ID'],
                                                   simInfo.Parameters['Inputs']['sim_IDtod'] )   
    writeGainMapFile= '{}/{}_{}_GainMap_Sky.fits'.format(simInfo.Parameters['Outputs']['outputdir'], simInfo.Parameters['Inputs']['sim_ID'],
                                                      simInfo.Parameters['Inputs']['sim_IDtod'] )   
    writeFNoiseFile = '{}/{}_{}_FNoise_Sky.fits'.format(simInfo.Parameters['Outputs']['outputdir'], simInfo.Parameters['Inputs']['sim_ID'],
                                                     simInfo.Parameters['Inputs']['sim_IDtod'] )
    writeHitsFile   = '{}/{}_{}_Hits_Sky.fits'.format(simInfo.Parameters['Outputs']['outputdir'], simInfo.Parameters['Inputs']['sim_ID'],
                                                   simInfo.Parameters['Inputs']['sim_IDtod'] )
    writeRFIFile    = '{}/{}_{}_RFI_Sky.fits'.format(simInfo.Parameters['Outputs']['outputdir'], simInfo.Parameters['Inputs']['sim_ID'],
                                                  simInfo.Parameters['Inputs']['sim_IDtod'] )
    writeSkyFile    = '{}/{}_{}_TOD_Sky_Sky.fits'.format(simInfo.Parameters['Outputs']['outputdir'], simInfo.Parameters['Inputs']['sim_ID'],
                                                      simInfo.Parameters['Inputs']['sim_IDtod'] )
    writeAllFile    = '{}/{}_{}_ALL_sim_SkyTOD_Realisation{}.hdf5'

    if simInfo.Parameters['Inputs']['sim_TOD']:#not os.path.exists(writeFNoiseFile):

        if rank == 0:
            print ('------------')
        # Need a container to hold the maps at the end on root only!
        Maps = NaiveMapMaker.MapMaking(simInfo.nchannels,  simInfo.npix)

        #################################################################
        # CALCULATE HOW MANY BLOCKS ARE BEING ASSIGNED TO EACH CPU #
        #################################################################
        lo, hi, nMaxSteps = MPITools.DistributeNodes(simInfo.blocks.size, rank=0, doThis=False)
        lo, hi, nSteps    = MPITools.DistributeNodes(simInfo.blocks.size)
        blockRange = hi - lo
        
        # This array holds all the Node IDs + two arrays to say the node status
        Nodes     = np.arange(size, dtype='int')

        #####################################################################
        # Pre-make some large TOD containers assuming the maximum blocksize #
        #####################################################################
        G = np.ones((simInfo.nchannels,1))
        if simInfo.Parameters['Inputs']['sim_Receiver']:
            TOD_All         = np.zeros((simInfo.nchannels, simInfo.maxBlockSize))
        else:
            TOD_All = None

        if (simInfo.Parameters['Inputs']['sim_Fnoise']):        
            TOD_Fnoise      = np.zeros((simInfo.nchannels, simInfo.maxBlockSize))
        else:
            TOD_Fnoise = None

        TOD_RFI         = np.zeros((simInfo.nchannels, simInfo.maxBlockSize))
        mask            = np.ones(simInfo.maxBlockSize).astype('int64')

        if (simInfo.Parameters['RFI']['fullOutput']):
            TOD_RFI_SATCOUNT = np.zeros( simInfo.maxBlockSize)
            TOD_RFI_DIST = np.zeros( simInfo.maxBlockSize)
        else:
            TOD_RFI_SATCOUNT = None
            TOD_RFI_DIST = None

        

        TOD_Coordinates = np.zeros((6, simInfo.maxBlockSize))  
        TOD_Pixels = np.zeros((simInfo.maxBlockSize), dtype='int64')  

        beta = 1./np.sqrt(simInfo.chanWidth)

        # Precompute the power spectrum of the 1/f noise. Calling FNoiseObject just generates new realisations.
        if (simInfo.Parameters['Inputs']['sim_Fnoise']):        
            FNoiseObject = Noise.FNoise(simInfo.Parameters['FNoise']['noiseRatio'],
                                            simInfo.Parameters['FNoise']['noisePower'],
                                            simInfo.Parameters['FNoise']['noiseFreq'],
                                            simInfo.Parameters['FNoise']['alpha'],
                                            simInfo.Parameters['FNoise']['cutoff'],
                                            simInfo.Parameters['FNoise']['beta'],
                                            simInfo.sampleRate,
                                            simInfo.chanWidth,
                                            simInfo.nchannels,
                                            simInfo.maxBlockSize,
                                            fftMode=simInfo.Parameters['FNoise']['fftLib'],
                                            random=simInfo.Parameters['FNoise']['randomLib'],
                                            GSL_RNG_TYPE=simInfo.Parameters['FNoise']['gsl_rng_type'],
                                            planner=simInfo.Parameters['FNoise']['planner'],
                                            threads = simInfo.Parameters['FNoise']['threads'],
                                            whiteNoise=simInfo.Parameters['Inputs']['sim_Wnoise'],
                                            filterScale=simInfo.Parameters['FNoise']['filterScale'])

        try:
            if (simInfo.Parameters['Inputs']['sim_Atmosphere']):        
                AtmosObject = Atmosphere.Atmosphere(simInfo.Parameters['Atmosphere']['meanTau'],
                    simInfo.sampleRate,
                    simInfo.chanWidth,
                    simInfo.nchannels,
                    simInfo.maxBlockSize)
        except KeyError:
            pass
            
        #####################################################################
        ######^^^^^^^^^^^^^^IMPORTANT^^^^^^^^^^^^^^^^^^^^^^^^^^#########
        ##########################################################

        #################################################################
        # LOOP THROUGH EACH 1/F NOISE REALISATION (OR OTHER RANDOM PROCESS)
        #################################################################
        for r in range(int(simInfo.Parameters['FNoise']['irealisation']),
                        int(simInfo.Parameters['FNoise']['realisations'])):
            comm.Barrier() # Let everyone catch up after generating the TOD

            # Open a file
            if simInfo.Parameters['Outputs']['write_TOD']:
                outputFileName = 'TOD/{}_{}_TOD-DATA_realisation{}'.format(simInfo.Parameters['Inputs']['sim_ID'],simInfo.Parameters['Inputs']['sim_IDtod'], r)
                
                try:
                    if 'standard' in simInfo.Parameters['Outputs']['TODMode'].lower():
                        TODFILE = Control.GenerateTODFiles.CreateFileObject(outputFileName, simInfo)
                    else: 
                        TODFILE = Control.GenerateTODFiles.COMAP_CreateFileObject(outputFileName, simInfo)
                except KeyError:
                    TODFILE = Control.GenerateTODFiles.CreateFileObject(outputFileName, simInfo)
                    simInfo.Parameters['Outputs']['TODMode'] = 'standard'
                    
                TODFILEOpen = True
            else:
                TODFILEOpen = False



            # tfull is used to record times of each loop if verbose is requested
            if rank == 0:
                tmean = np.zeros(hi - lo)
                if simInfo.Parameters['Globals']['verbose']:
                    tfull = np.zeros((6, hi - lo))
            #################################################################
            # LOOP THROUGH BLOCKS #
            # E.g. this first loop stops all the TOD being generated at once #
            #################################################################
            for iblock in range(lo, hi):

                # Reset mask
                mask *= 0
                mask += 1

                if rank == 0:
                    T0 = time.time()

                #####################################################
                # Let each node know how big its total allocation is #
                #####################################################
                simInfo.totalSamps = simInfo.blocks.nSamples[iblock]


                #####################################################
                # GENERATE THE COORDINATES PER DATA BLOCK #
                #####################################################


                if (rank == 0) & (iblock == hi-1):
                    print( '# GENERATED TOD COORDINATES')
                Control.GenerateCoordinates(simInfo, TOD_Coordinates, TOD_Pixels, iblock)
                        
                #####################################################
                # You may wish to get the SKY as a TOD stream ... #
                #####################################################
                if simInfo.Parameters['Inputs']['sim_SkyTOD']:
                    #sim_SkyTOD = Control.GenerateSkyTOD(simInfo, Sky, TOD_Pixels[:int(simInfo.totalSamps)]) # Will need Healpix -> TOD and Cart -> TOD
                    sim_SkyTOD = Control.GenerateSkyTOD(simInfo, Sky, TOD_Coordinates[3:5,:int(simInfo.totalSamps)]) # Will need Healpix -> TOD and Cart -> TOD
                else:
                    sim_SkyTOD = 0.
    
                if rank == 0:
                    TCoordinates = time.time()

                #####################################################
                # Add in some RFI signals ?
                #####################################################
                if simInfo.Parameters['Inputs']['sim_RFI']:

                    if (rank == 0) & (iblock == nSteps-1):
                        print( '# GENERATED TOD SATELLITE RFI')
                                
                    TOD_RFI *= 0.
                    iObs = int(simInfo.blocks.iObsIndex[iblock])
                    receivers    = simInfo.receivers[simInfo.recIDs[iObs]]
                    telescopes   = simInfo.telescopes[simInfo.telIDs[iObs]] 

                    SatExec.Run(TOD_RFI, mask, 
                                TOD_Coordinates[0,:]*np.pi/180., TOD_Coordinates[1,:]*np.pi/180., TOD_Coordinates[2,:], 
                                telescopes[1]*np.pi/180., telescopes[0]*np.pi/180.,telescopes[2],
                                simInfo.Beam,
                                satCount = TOD_RFI_SATCOUNT, satCountEl = simInfo.Parameters['RFI']['satCountEl'],
                                dist = TOD_RFI_DIST)
                    #TOD_Satellites, TOD_SatCoords = Control.GenerateSatellitesRFI(simInfo,
                    #                                                              TOD_Coordinates[0,:],
                    #                                                              TOD_Coordinates[1,:],
                    #                                                              TOD_Coordinates[2,:],
                    #                                                              groups,
                    #                                                              lo=lowBlock, hi=highBlock) # Beam applied internally


                if rank == 0:
                    TSats = time.time()

                #####
                # FINAL STEP, COMBINE ALL THE TOD INTO A FINAL MAP
                #####


                thisRec = simInfo.receivers[simInfo.recIDs[simInfo.blocks.iObsIndex[iblock]]]
                tsys = thisRec[0]

                # total: about 16 seconds
                if simInfo.Parameters['Inputs']['sim_Receiver']:

                    TOD_All[:,:int(simInfo.totalSamps)] *= 0.
                    
                    try:
                        if (simInfo.Parameters['Inputs']['sim_Atmosphere']):    
                            atmosEmission, atmosAbsorption = AtmosObject.GenerateAtmosphere(TOD_Coordinates[1,:int(simInfo.totalSamps)])
                            # attenuate sky signal
                            sim_SkyTOD *= atmosAbsorption 
                            TOD_All[:,:int(simInfo.totalSamps)] += atmosEmission
                    except KeyError:
                        pass
                    TOD_All[:,:int(simInfo.totalSamps)] += sim_SkyTOD
                    TOD_All[:,:int(simInfo.totalSamps)] += tsys

                    if simInfo.Parameters['Inputs']['sim_RFI']:
                        TOD_All[:,:int(simInfo.totalSamps)] += TOD_RFI[:,:int(simInfo.totalSamps)]


                    if simInfo.Parameters['Inputs']['sim_Fnoise']: # Needed for 1/f noise

                        # Populate TOD_Fnoise with a new 1/f noise realisations
                        Control.GenerateFNoise(simInfo, TOD_Fnoise[:,:int(simInfo.totalSamps)], FNoiseObject, iblock)

                        # We want the gain fluctuations around 1, so +1
                        TOD_Fnoise[:,:int(simInfo.totalSamps)] += 1.
                        
                        # Multiply the gain fluctuations by the Tsys
                        TOD_All[:,:int(simInfo.totalSamps)] *= TOD_Fnoise[:,:int(simInfo.totalSamps)]
                    
                    else:
                        # If white-noise is requested...
                        # If 1/f + white noise is wanted then the white noise is generated
                        # at the same time as the 1/f noise.
                        if simInfo.Parameters['Inputs']['sim_Wnoise']:
                            TOD_All[:,:int(simInfo.totalSamps)] +=  np.random.normal(scale=(sim_SkyTOD + tsys)*beta*np.sqrt(simInfo.sampleRate), size=(TOD_All.shape[0],int(simInfo.totalSamps)) )
                
                if rank == 0:
                    TFNoise = time.time()

                if rank == 0:
                    TWNoise = time.time() 


                # This section performs filtering on the final TOD.
                # Still many questions about this at the moment, median filtering introduces step functions while butterworth or cosine filters introduce source negatives.
                # Galaxy mask can be used to largely correct for this but it is not ideal.
                # Because filtering involves an FFT this effectively triples the simulations time per block
                if simInfo.Parameters['Filtering']['filterData']:
                    meds = np.zeros(TOD_All.shape)

                    if simInfo.Parameters['Filtering']['type'].lower() == 'median':
                        a = np.copy( TOD_All[0,:int(simInfo.totalSamps)])
                        step = int(simInfo.Parameters['Filtering']['cutoff']*simInfo.sampleRate)
                    
                        nsteps = int(simInfo.totalSamps)/step
                        cbinning.get_median_filter(TOD_All[:,:int(simInfo.totalSamps)], meds[:,:int(simInfo.totalSamps)], step)
                        TOD_All[:,:int(simInfo.totalSamps)] -= meds
                    elif simInfo.Parameters['Filtering']['type'].lower() == 'butterworth':
                        if simInfo.Parameters['Filtering']['maskgal']:
                            rms = np.std(TOD_All[:,1:] - TOD_All[:,:-1])/np.sqrt(2)
                            med = np.reshape(np.median(TOD_All, axis=1), (TOD_All.shape[0], 1))
                            gal = (sim_SkyTOD > np.min(sim_SkyTOD) + rms*5.)
                            meds[:,:int(simInfo.totalSamps)] += med
                            (meds[:,:int(simInfo.totalSamps)])[gal] = np.random.normal(scale=rms, loc=(meds[:,:int(simInfo.totalSamps)])[gal])
                            (meds[:,:int(simInfo.totalSamps)])[~gal] = (TOD_All[:,:int(simInfo.totalSamps)])[~gal]

                            TOD_All[:,:int(simInfo.totalSamps)] -= Filtering.butter_filter(meds[:,:int(simInfo.totalSamps)],
                                                                                           simInfo.Parameters['Filtering']['cutoff'],
                                                                                           simInfo.sampleRate,
                                                                                           simInfo.sampleRate)
                        else:
                            TOD_All[:,:int(simInfo.totalSamps)] -= Filtering.butter_filter(meds,
                                                                                           simInfo.Parameters['Filtering']['cutoff'],
                                                                                           simInfo.sampleRate,
                                                                                           simInfo.sampleRate)

                    elif simInfo.Parameters['Filtering']['type'].lower() == 'fir':
                        pass

                if rank == 0:
                    TFilter = time.time()


                # Here we can perform some data processing procedures.
                try:
                    if simInfo.Parameters['Inputs']['sim_Analysis']:
                        if simInfo.Parameters['Analysis']['atmosphere']:
                            Analysis.Atmosphere.RemoveAtmosphere(simInfo, TOD_All, TOD_Coordinates)
                        if simInfo.Parameters['Analysis']['calibration']:
                            pass
                except:
                    pass
                    # if other analysis steps?

                # This is where we write the TOD out to file.
                if simInfo.Parameters['Outputs']['write_TOD'] & TODFILEOpen:
                    if 'standard' in simInfo.Parameters['Outputs']['TODMode'].lower():
                        Control.GenerateTODFiles.WriteToFileObject(TODFILE,
                                                               simInfo,
                                                               iblock,
                                                               {'TOD':TOD_All,
                                                                'RA': TOD_Coordinates[3,:],
                                                                'DEC':TOD_Coordinates[4,:],
                                                                'PIXELS':TOD_Pixels,
                                                                'AZ':TOD_Coordinates[0,:],
                                                                'EL':TOD_Coordinates[1,:],
                                                                'JD':TOD_Coordinates[2,:],
                                                                'PANG':TOD_Coordinates[5,:],
                                                                'SATCOUNTS':TOD_RFI_SATCOUNT,
                                                                'SATDIST':TOD_RFI_DIST}) # These keys should match those in GenerateTODFiles.py
                    else:                        
                        Control.GenerateTODFiles.COMAP_WriteToFileObject(TODFILE,
                                                               simInfo,
                                                               iblock,
                                                               {'TOD':TOD_All,
                                                                'RA': TOD_Coordinates[3,:],
                                                                'DEC':TOD_Coordinates[4,:],
                                                                'PIXELS':TOD_Pixels,
                                                                'AZ':TOD_Coordinates[0,:],
                                                                'EL':TOD_Coordinates[1,:],
                                                                'JD':TOD_Coordinates[2,:],
                                                                'PANG':TOD_Coordinates[5,:]}) # These keys should match those in GenerateTODFiles.py
                        


                # Using the Naive Map-Making class the maps are generated.
                if simInfo.Parameters['Inputs']['sim_Receiver']:
                    Maps.BinMap(TOD_Pixels[:int(simInfo.totalSamps)], TOD_All[:,:int(simInfo.totalSamps)], weights=mask[:int(simInfo.totalSamps)])

                if rank == 0:
                    TMapping = time.time()

                if rank == 0:
                    tmean[iblock-lo] = time.time() - T0
                    if simInfo.Parameters['Globals']['verbose']:
                        tfull[0,iblock-lo] = TCoordinates - T0
                        tfull[1,iblock-lo] = TSats    - TCoordinates
                        tfull[2,iblock-lo] = TFNoise  - TSats
                        tfull[3,iblock-lo] = TWNoise  - TFNoise
                        tfull[4,iblock-lo] = TFilter  - TWNoise
                        tfull[5,iblock-lo] = TMapping - TFilter
                    if (np.mod(iblock, (hi-lo)//20) == 0): # Print update on every 5% of the loop
                        complete = int( (iblock - lo)/float((hi - lo))*100)
                        print( 'REALISATION {}, {}% COMPLETE, MEAN TIME PER BLOCK: {:.2f} +/- {:.2f} seconds, {} Blocks Per Node ({} Nodes)'.format(r, 
                                                                                                                                                   complete, 
                                                                                                                                                   np.mean(tmean[:iblock-lo+1]),
                                                                                                                                                   np.std(tmean[:iblock-lo+1]),
                                                                                                                                                   hi-lo,
                                                                                                                                                   comm.size))
                        if simInfo.Parameters['Globals']['verbose']:
                            print ('COORDINATES TIME: {:.2f} +/- {:.2f} seconds:'.format(np.mean(tfull[0,:iblock-lo+1]), np.std(tfull[0,:iblock-lo+1])))
                            print( 'SATS TIME: {:.2f} +/- {:.2f} seconds:'.format(np.mean(tfull[1,:iblock-lo+1]), np.std(tfull[1,:iblock-lo+1])))
                            print ('FNOISE TIME: {:.2f} +/- {:.2f} seconds:'.format(np.mean(tfull[2,:iblock-lo+1]), np.std(tfull[2,:iblock-lo+1])))
                            print ('WNOISE TIME: {:.2f} +/- {:.2f} seconds:'.format(np.mean(tfull[3,:iblock-lo+1]), np.std(tfull[3,:iblock-lo+1])))
                            print ('FILTER TIME: {:.2f} +/- {:.2f} seconds:'.format(np.mean(tfull[4,:iblock-lo+1]), np.std(tfull[4,:iblock-lo+1])))
                            print ('MAPPING TIME: {:.2f} +/- {:.2f} seconds:'.format(np.mean(tfull[5,:iblock-lo+1]), np.std(tfull[5,:iblock-lo+1])))

                        #print 'TIME: {}'.format(T1-T0), T2-T1, time.time()-T2, time.time()-T0

                ############# END OF BLOCK LOOP

            if simInfo.Parameters['Outputs']['write_TOD'] & TODFILEOpen:
                Control.GenerateTODFiles.CloseTODObjects(TODFILE)
                TODFILEOpen = False
                 
            if rank ==0:
                print( 'TOTAL TIME TO COMPLETE BLOCK: {:.2f} seconds'.format(np.sum(tmean)))

            MPISum2Root(Maps.sw  , Maps.sw_all  , Nodes)
            MPISum2Root(Maps.w   , Maps.w_all   , Nodes)
            MPISum2Root(Maps.hits, Maps.hits_all, Nodes)

            # Only at the end when the maps of each block are finished are they added to form the final map.
            if rank == 0:
                print( '-----------')
                Sky.Hits = Maps.hits_all
                print ('# WRITING OBSERVED SKY REALISATION {}'.format(r))
                Sky.All = Maps.sw_all/Maps.w_all


                if simInfo.Parameters['Mapping']['mode'].upper() == 'CARTESIAN':
                    Sky.All = np.transpose(np.reshape(Sky.All, (Sky.All.shape[0],
                                                   int(simInfo.Parameters['Mapping']['ct_naxis2']),
                                                   int(simInfo.Parameters['Mapping']['ct_naxis1']))), (0,2,1))
                    Sky.Hits = np.reshape(Sky.Hits, (int(simInfo.Parameters['Mapping']['ct_naxis2']),
                                                     int(simInfo.Parameters['Mapping']['ct_naxis1']))).T


                FileTools.WriteH5Py(writeAllFile.format(simInfo.Parameters['Outputs']['outputdir'], simInfo.Parameters['Inputs']['sim_ID'],
                                                        simInfo.Parameters['Inputs']['sim_IDtod'],r ), {'MAPS': Sky.All,
                                                                                                        'WEIGHTS': Maps.w_all,
                                                                                                        'HITS': Sky.Hits})
                Maps.sw_all[...] = 0.
                Maps.w_all[...] = 0.
                Maps.hits_all[...] = 0.
                Maps.ZeroLocalMaps()

            ############# END OF REALISATIONS LOOP
        if simInfo.Parameters['Inputs']['sim_Fnoise']: # Needed for 1/f noise
            FNoiseObject.FreeRandom()

        #####################################################                    
        # HERE WE WRITE ALL THE NEW MAPS OUT TO FILE #
        #####################################################
    comm.Barrier()
    sys.exit()


###################^^^^^^####################
# *** TOD GENERATION PIPELINE ENDS HERE *** #
##########^^^^^^^^########^^^^^^^^###########



if __name__ == "__main__":
    
    paramFile = sys.argv[1]
    if len(sys.argv) > 2:
        defaultFile = sys.argv[2]
    else:
        defaultFile = None

    main(paramFile, defaultFile)
