#

import numpy as np
from distutils.core import setup
from distutils.extension import Extension

extent = []

# Compile the map binning routine
extent += [Extension('pipeline.MapMaking.Naive.cbinning', 
                    ['pipeline/MapMaking/Naive/cbinning.c'], 
                    include_dirs=[np.get_include()])]

# Compile the GSL routines
#libs = ['gsl', 'gslcblas']
#extent += [Extension('pipeline.GSL.PyGSL_RNG', #
#                    ['pipeline/GSL/PyGSL_RNG.c'], 
#                    libraries=libs)]

# Compile Satellite modules
sourcefiles = ['pipeline/Observatory/RFI/SatPos.c']
extent += [Extension('pipeline.Observatory.RFI.SatPos', 
                     sourcefiles, 
                     include_dirs=[np.get_include()])]


# Setup the package structure of the pipeline
packages=['pipeline', 
          'pipeline.GSL',
          'pipeline.Control',
          'pipeline.MapMaking','pipeline.MapMaking.Naive', 
          'pipeline.Component_Separation', 'pipeline.Component_Separation.PCA', 'pipeline.Component_Separation.GNILC', 
          'pipeline.Tools', 'pipeline.Analysis',
          'pipeline.Observatory','pipeline.Observatory.Receivers',
          'pipeline.Observatory.Sky','pipeline.Observatory.Sky.COModel1','pipeline.Observatory.Sky.HIModel1',
          'pipeline.Observatory.Telescope','pipeline.Observatory.Telescope.Pointing','pipeline.Observatory.RFI',
          'pipeline.Observatory.Beam']

data = {'pipeline':['Parameters.ini']}

setup(name='Intensity Mapping Pipeline',
      version='1.0',
      author='Stuart Harper',
      author_email='stuart.harper@manchester.ac.uk',
      ext_modules = extent,
      packages=packages,
      package_data=data
)
