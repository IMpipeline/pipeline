import os
from setuptools import setup, Extension, find_packages
from distutils import sysconfig
import re
import numpy as np
import warnings


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

if re.search('gcc', sysconfig.get_config_var('CC')) is None:
	args = []
else:
	args = ['-fopenmp']

try:
	from Cython.Distutils import build_ext
	HAVE_CYTHON = True

except ImportError as e:
	warnings.warn("Cython not installed.")
	from distutils.command import build_ext
	HAVE_CYTHON = False
def cython_file(filename):
	filename = filename + ('.pyx' if HAVE_CYTHON else '.c')
	return filename

# Cubic spline extension
cs_ext = Extension('gen_input.cora.util.cubicspline', [ cython_file('gen_input/cora/util/cubicspline') ], 
		include_dirs=[ np.get_include() ], 
		extra_compile_args=args, 
		extra_link_args=args)

# Spherical bessel extension (not built by default)
sb_ext = Extension('gen_input.cora.util._sphbessel_c', [cython_file('gen_input/cora/util/_sphbessel_c')],
		include_dirs=[np.get_include()],
		libraries=['gsl', 'gslcblas'])

# Tri-linear map extension
tm_ext = Extension('gen_input.cora.util.trilinearmap', [cython_file('gen_input/cora/util/trilinearmap')],
		include_dirs=[np.get_include()])

setup(
    name = "IM pipeline",
    version = "0.0.1",
    author = " ",
    author_email = "",
    description = (""),
    packages=['gen_scan', 'gen_systematics', 'gen_noise', 'gen_tod2map','gen_tod','comp_separation','gen_input'],
    ext_modules = [ cs_ext, tm_ext],
    requires = ['numpy', 'scipy', 'healpy', 'pyfits'],
    package_data = {'gen_input.cora.signal' : ['data/ps_z1.5.dat', 'data/corr_z1.5.dat'], 'gen_input.cora.foreground' : ['data/skydata.npz', 'data/combinedps.dat']},
    cmdclass = {'build_ext': build_ext}
)
