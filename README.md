# INSTALLATION

## External Libaries

1) The pipeline relies upon several external libaries. The two that are REQUIRED are: HEALPIX and MPI4PY.
To install MPI4PY you will first need to install openmpi from: https://www.open-mpi.org/software/ompi/v3.0/ .
It is likely you will need to install OpenMPI even if you are using a JBCA machine as all local copies of OpenMPI are very old.
Installation is easy though!
```

> wget openmpi-X.X.X.tar.gz
> tar zxvf openmpi-X.X.X.tar.gz
> cd openmpi-X.X.X.tar.gz
> ./configure --prefix=/path/to/your/libs/lib # You cannot use the default without SU!
> make
> make install

```
Remember to add /path/to/your/libs/lib to your LD_LIBRARY_PATH and /path/to/your/libs/bin to your PATH.

2) The other libaries you may need are FFTw and GSL depending on how much speed up you need in noise generation.

3) If you intend to write lots of time-ordered data files then you will likely also want to install an MPI ready version of HDF5 and link it with your local openmpi library. See FAQ.

4) Ensure that you are running the latest version of Python 3.

## PYTHON

1) All required Python Modules can be installed using the requirements.txt:

If you have SU privilages use (e.g. your own laptop):
```

> sudo pip install -r requirements.txt --user

```
else just:
```

> pip install -r requirements.txt --user

```

2) Now you need to compile and install the pipeline package using

```

> python setup.py build
> python setup.py install

```

The pipeline should now be installed like any other package. For example if you want to access the HDF5 tools call

```

> from pipeline.Tools import FileTools
> data = FileTools.ReadH5Py('XXX.hdf5')

```

# Running the code

1) Make a new directory somewhere on your system and copy run.py and Parameters.ini from the repository there.

2) From the Configs directory in the repository copy a Telescopes.dat and a Receivers.dat from the examples.

3) If you call 
```

> python run.py Parameters.ini

```
This should populate your working directory with several new directories but not produce any data. For a basic sky map output with noise set the following in Parameters.ini:
```

sim_TOD : True
sim_Receiver : True

```

4) MPI jobs can be called using

```

> mpirun -n X python run.py Parameters.ini

```

5) The Parameters.ini file that you are using does not need to be complete, and you could set a default parameter file and combine it with parameters files concerned with only specific jobs e.g.
```

> mpirun -n X python run.py MySimulations.ini Defaults.ini

```



## FAQ

1) I set _write\_TOD_ to be True and the run script crashes.

The likely explanation for this is that you have not linked you MPI libraries with you HDF5 libraries used by the H5Py Python modules. In order to do this you will need to perform the following steps:
- Remove H5Py and MPI4Py from your Python installation (using pip, but be mindful that all the H5Py headers are removed from the path/to/python/directory/pkgs/hdf5\_X.X directory)

- Download the HDF5 source (www.hdfgroup.org) and follow instructions for parallel hdf5 on the h5py website (http://docs.h5py.org/en/latest/mpi.html)

- Once the HDF5 source is compiled you must then compile the H5Py Cython libraries as described on (http://docs.h5py.org/en/latest/mpi.html)

- NOTE: If you are running Linux and H5PY is failing to compile using your MPI library try making the following change to the src/h5p.pyx file:

```
 if MPI:
	-L30: from mpi4py.mpi_c cimport MPI_Comm, MPI_Info, MPI_Comm_dup, MPI_Info_dup, \
        -L31:  MPI_Comm_free, MPI_Info_free
        +L30: from mpi4py.libmpi cimport MPI_Comm, MPI_Info, MPI_Comm_dup, MPI_Info_dup, MPI_Comm_free, MPI_Info_free

```

#########
# NOTES #
#########

1) Maps of the sky for each component, including 1/f, RFI, etc... are saved to SKY_MODELS directory in your simulation directory.

2) Feel free to modify the run.py to include component separation modules.