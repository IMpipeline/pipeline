from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext

sourcefiles = ["PyGSL_RNG.pyx"]
libs = ['gsl', 'gslcblas']
ext_modules = [Extension("PyGSL_RNG",
                         sourcefiles,
                         libraries=libs)]

setup(
    name = 'GSL RNG Wrapper',
    cmdclass = {'build_ext': build_ext},
    ext_modules = ext_modules)

