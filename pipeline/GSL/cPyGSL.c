#include <stdio.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include <time.h>

int _GenerateGaussian(int N, double data[], gsl_rng * r);
int _GenerateUniform(int N, double data[], gsl_rng * r);
gsl_rng* _setupRNG(unsigned long seed);
void _freeRNG(gsl_rng * r);

int _GenerateGaussian(int N, double data[], gsl_rng * r){
  /*
    Generate:
    Returns N random gaussian variables using the GSL_RNG_TYPE random number generator.
    Note - taus2 seems to be fastest, mt19937 is also good (this is what numpy uses?)

    Arguments
    N - Length of data array
    data - the data array 
    r - the gsl_rng structure thing
    
  */
  int i;
  double sigma = 1;

  for (i = 0; i < N; i++)
    {
      data[i] = gsl_ran_gaussian_ziggurat(r, sigma);
    }

  return N;
}

int _GenerateUniform(int N, double data[], gsl_rng * r){
  /*
    Generate:
    Returns N random uniform variables using the GSL_RNG_TYPE random number generator.
    Note - taus2 seems to be fastest, mt19937 is also good (this is what numpy uses?)

    Arguments
    N - Length of data array
    data - the data array 
    r - the gsl_rng structure thing
    
  */
  int i;

  for (i = 0; i < N; i++)
    {
      data[i] = gsl_rng_uniform(r);
    }

  

  return N;
}


gsl_rng * _setupRNG(unsigned long seed){
  /*
    SetupRNG:
    This sets the seed of the random number generator using GSL_RNG_SEED varibale
   */
  const gsl_rng_type * T;
  gsl_rng * r;

  gsl_rng_env_setup();
  
  T = gsl_rng_default;
  r = gsl_rng_alloc(T);

  gsl_rng_set(r, seed);

  return r;
}

void _freeRNG(gsl_rng * r){
  /*
    freeRNG
    This clears up the gsl_rng from memory
   */
    gsl_rng_free(r);
}
