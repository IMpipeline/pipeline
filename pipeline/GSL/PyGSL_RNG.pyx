cdef extern from "gsl/gsl_rng.h":
     ctypedef struct gsl_rng_type:
          const char *name;
          unsigned long int max;
          unsigned long int min;
          size_t size;
          void (*set) (void *state, unsigned long int seed);
          unsigned long int (*get) (void *state);
          double (*get_double) (void *state);

     ctypedef struct gsl_rng:
          const gsl_rng_type *type;
          void *state;

cdef extern from "cPyGSL.c":
     int _GenerateGaussian(int N,  double* data, gsl_rng* r)
     int _GenerateUniform(int N,  double* data, gsl_rng* r)
     gsl_rng* _setupRNG(unsigned long seed)
     void _freeRNG(gsl_rng* r)

     
import numpy as np
cimport numpy as np

DTYPE = np.float
ctypedef np.float_t DTYPE_t

ITYPE = np.int
ctypedef np.int_t ITYPE_t

cdef class GSLRandom:
    cdef gsl_rng *r
    def __cinit__(self, mySeed):
        self.r = _setupRNG(int(mySeed))

    def _setSeed(self, mySeed):
        self.r = _setupRNG(int(mySeed))


    def GenerateGaussian(self, np.ndarray[np.double_t,ndim=3] x):#DTYPE_t[:] x):

        cdef int kstore = x.shape[0]
        cdef int nsamples = x.shape[1]
        cdef int nfreqs = x.shape[2]
        cdef np.ndarray[np.double_t, ndim=3, mode="c"] A_c
        
        A_c = np.ascontiguousarray(x, dtype=np.double)
        
        _GenerateGaussian(nsamples*nfreqs*kstore, &A_c[0,0,0], self.r)

    def GenerateUniform(self, np.ndarray[np.double_t,ndim=3] x):#DTYPE_t[:] x):

        cdef int kstore = x.shape[0]
        cdef int nsamples = x.shape[1]
        cdef int nfreqs = x.shape[2]
        cdef np.ndarray[np.double_t, ndim=3, mode="c"] A_c
        
        A_c = np.ascontiguousarray(x, dtype=np.double)
        
        _GenerateUniform(nsamples*nfreqs*kstore, &A_c[0,0,0], self.r)

    def GenerateUniformPhases(self, np.ndarray[np.double_t,ndim=3] x):#DTYPE_t[:] x):

        cdef int kstore = x.shape[0]
        cdef int nsamples = x.shape[1]
        cdef int nfreqs = x.shape[2]
        cdef np.ndarray[np.double_t, ndim=3, mode="c"] A_c
        
        A_c = np.ascontiguousarray(x, dtype=np.double)
        
        _GenerateUniform(nsamples*nfreqs*kstore, &A_c[0,0,0], self.r)

    def free(self):
        _freeRNG(self.r)
