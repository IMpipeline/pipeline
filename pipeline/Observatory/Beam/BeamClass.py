import numpy as np
from scipy.special import j1
import healpy as hp
import os
from pipeline.Tools import FileTools
from scipy.interpolate import griddata, interp2d, Rbf, interp1d

import scipy.interpolate as spint
import scipy.spatial.qhull as qhull

def interp_weights(xy, uv,d=2):
    tri = qhull.Delaunay(xy)
    simplex = tri.find_simplex(uv)
    vertices = np.take(tri.simplices, simplex, axis=0)
    temp = np.take(tri.transform, simplex, axis=0)
    delta = uv - temp[:, d]
    bary = np.einsum('njk,nk->nj', temp[:, :d, :], delta)
    return vertices, np.hstack((bary, 1 - bary.sum(axis=1, keepdims=True)))

def interpolate(values, vtx, wts):
    return np.einsum('nj,nj->n', np.take(values, vtx), wts)

def beamFilter(theta, theta_c):
    
    window = (np.cos(2.*np.pi * (theta - theta_c)/(np.pi/180.)/2. ) + 1.)/2.
    
    low = theta < ( theta_c - np.pi/180.)
    high= theta > (theta_c)
    
    window[low] = 0
    window[high] = 1.
    
    return window


class Beam:

    def __init__(self, model, fwhm, f0, maxFreq, minFreq, nchannels, nside, BeamTransform=True, beamfile=None, theta_c=0):
        '''
        Store important information such as FWHM
        Pregenerate Harmonic/Fourier beam models

        model - string
        fwhm  - of beam, in degrees
        f0    - beam model central frequency, MHz
        maxFreq - Maximum frequency of receiver, MHz
        minFreq - Minimum frequency of receiver, MHz
        nchannels - No. channels in receiver
        nside   - nside of sky maps # Needs updating to allow for Cartesian gridding. Two setup functions?

        '''

        nside = int(nside) #int(simInfo.Parameters['Mapping']['hp_nside'])
        self.model = model #  simInfo.Parameters['Beam']['model']
        self.fwhm = fwhm # simInfo.Parameters['Beam']['fwhm']
        self.f0   = f0 # simInfo.Parameters['Beam']['f0'] # MHz
        self.chanEdges = np.linspace(minFreq, maxFreq, nchannels + 1, endpoint=True)
        self.chanMids  = (self.chanEdges[1:] + self.chanEdges[:-1])/2. # MHz            

        self.theta_c = theta_c

        self.container = None

        if self.model.lower() == 'airy':
            self.k = 2.058/self.fwhm * 180. / self.f0*self.chanMids
            self.mainbeamratio = 1.053
            self.AiryDiskCreate()
            self.Gain = self.AiryDisk
        elif self.model.lower() == 'model':
            self.beamfile = beamfile
            self.ModelCreate()
            self.Gain = self.Model
            self.mainbeamratio = 1.
        else:
            self.model = 'gaussian'
            self.k = self.fwhm*self.f0/self.chanMids*np.pi/180.
            self.mainbeamratio = 1.
            self.Gain = self.Gaussian
            
        if BeamTransform:
            HPBeamFile = 'SKY_MODEL/HPBeamModel_{}_Nside{}_FWHM{}_Nchan{}.hdf5'.format(self.model.lower(), nside, self.fwhm, nchannels)
            if os.path.isfile(HPBeamFile):
                self.Bl = FileTools.ReadH5Py(HPBeamFile)['Bl']
            else:
                self.GenerateHealpixBeamModel(nside)
                #FileTools.WriteH5Py(HPBeamFile,{'Bl': self.Bl})
        self.I = None

    def SetupI(self, thetasize):
        '''
        Pre-load arrays used for
        '''
        self.I = np.zeros((self.chanMids.size, thetasize))
        
    def ClearI(self):
        self.I *= 0.

    def AiryDisk(self, nu, theta):
        '''
        theta in radians
        '''
        
        for i in range(nu.size):
            theta[nu.size-i-1, :] = self.fModels[i](theta[0,:])
            
        
    def AiryDiskCreate(self):
        '''
        theta in radians
        '''
        
        nsamps = 180./self.fwhm * 5.
        theta = np.linspace(0, np.pi, nsamps)

        self.I = np.outer(self.k, np.sin(theta/2.))
        
        self.I = 4. * (j1(self.I))**2/self.I**2
        
        if np.min(theta) == 0.:
            self.I[:,(theta == 0)] = 1

        I0 = 4.*np.pi/(1.197 * (self.fwhm * self.f0 / self.chanMids * np.pi/180.)**2)

        self.airy = I0[:,np.newaxis] * self.I
        self.fModels = []
        for i in range(self.chanMids.size):

            if self.theta_c > 0:
            
                self.fModels += [interp1d(theta, self.airy[i,:]*beamFilter(theta, self.theta_c),
                                          bounds_error=False, fill_value=0)]
            else:
                self.fModels += [interp1d(theta, self.airy[i,:],
                                      bounds_error=False, fill_value=0)]
        
        #return self.I

    def ModelCreate(self):
        
        d = FileTools.ReadH5Py(self.beamfile)

        self.mTheta = d['theta']*np.pi/180.
        self.mFreqs = d['frequency']


        # First regrid the beam to the simulation setup
        theta, frequency = np.meshgrid(d['theta']*np.pi/180., d['frequency'])
        self.positions = np.array([theta.ravel(), frequency.ravel()]).T

        self.newpos = np.meshgrid(d['theta']*np.pi/180., self.chanMids)

        self.fModel = griddata(self.positions, d['beam'].T.ravel(), (self.newpos[0], self.newpos[1]), 
                               method='linear',
                               fill_value=0)


        
        self.fModels = []
        for i in range(self.chanMids.size):

            self.fModels += [interp1d(d['theta']*np.pi/180., self.fModel[i,:],
                                      bounds_error=False, fill_value=0)]


    def Model(self, nu, theta):

        #from matplotlib import pyplot
        #self.newpos = np.array(np.meshgrid(theta, [1100]))

        #print(self.newpos.shape, self.positions.shape, self.fModel.shape)
        #pyplot.plot(griddata(self.positions, self.fModel, (self.newpos[0], self.newpos[1]), 
        #                     method='linear',
        #                     fill_value=0).flatten())
        

        #self.newpos = np.meshgrid(theta, nu)
        #b = griddata(self.positions, self.fModel, (self.newpos[0], self.newpos[1]), 
        #                     method='linear',
        #                     fill_value=0)
        
        #from matplotlib.colors import LogNorm
        #pyplot.imshow(b, aspect='auto', norm=LogNorm())
        #pyplot.show()

        #if isinstance(self.container, type(None)):
        #    self.container = np.zeros((nu.size, theta.size))
        for i in range(nu.size):
            theta[nu.size-i-1, :] = self.fModels[i](theta[0,:])
            #print(np.median(theta), np.median(self.container[10,:]))
        #from matplotlib import pyplot
        #pyplot.plot(theta)
        #pyplot.show()
            
        return 0 # self.container

        #return griddata(self.positions, self.fModel, (self.newpos[0], self.newpos[1]), 
        #                     method='linear',
        #                     fill_value=0)

    def Gaussian(self,nu, theta):
        '''
        theta in radians
        '''
        
        self.I = np.outer(1./self.k, theta, out=self.I)**2*8.*np.log(2)
        
        
        self.I = np.exp(-0.5 * self.I )

        return self.I
                
    def GenerateHealpixBeamModel(self, nside):
        '''
        Expects SetupI + Chosen Beam Model function to be run first
        '''
        
        pix = np.arange(12*nside**2)
        t, p = hp.pix2ang(nside, pix)

        self.SetupI(t.size)
        
        self.Bl = np.zeros((self.chanMids.size, 3*nside))

        if self.model.lower() == 'airy':
            self.AiryDisk(self.chanMids, t) # Generate N_nu Healpix maps with beam at colat=0
            for i in range(self.chanMids.size):
                #(i)
                self.Bl[i,:] = hp.anafast(self.I[i,:]) # Multiply maps using
                #self.Bl[i,:] *= 1.075/np.max(self.Bl[i,:])
                self.Bl[i,:] = np.sqrt(self.Bl[i,:])
                self.Bl[i,:] /= np.max(self.Bl[i,:])
        elif self.model.lower() == 'model':
            pass
        else:
            for i in range(self.chanMids.size):
                self.Bl[i,:] = hp.gauss_beam(self.k[i], lmax=3*nside-1) # Multiply maps using #
