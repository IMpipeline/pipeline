#call with: python setup.py build_ext --inplace

from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize
from Cython.Distutils import build_ext

setup(
    #ext_modules = cythonize("cbinning.pyx")
    name = 'phases',
    ext_modules = [Extension('phases', ['phases.pyx'])],
    cmdclass = {'build_ext': build_ext}
)

