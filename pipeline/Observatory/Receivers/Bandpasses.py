import numpy as np
import scipy.signal as signal
from scipy.interpolate import interp1d
from matplotlib import pyplot

def Cheby(order, ripple, lowFreq, highFreq, nchannels):
    
    topEnd = highFreq*4.
    chanEnds = np.linspace(lowFreq, highFreq, nchannels+1)
    
    b, a = signal.cheby1(order, ripple, [lowFreq/topEnd, highFreq/topEnd], btype='band')
    w, h = signal.freqz(b, a, worN=4000)
    w = (topEnd/np.pi)*w
    mdl = interp1d(w, np.abs(h)**2)
    bandpass = mdl(chanEnds)
    bandpass = (bandpass[1:] + bandpass[:-1])/2.
    
    return bandpass