'''

Name: Noise.py

Description: A series of functions for generati2ng different forms of
simulated receiver noise.

 - Thermal noise
 - 1/f noise
 -

'''

from mpi4py import MPI


import numpy as np 
import scipy.fftpack as sfft
import time
import os
from scipy.interpolate import interp1d

try:
    from pipeline.GSL import PyGSL_RNG
    NoGSL=False
except:
    NoGSL=True

try:
    import pyfftw
    NoPyFFTw=False
except:
    NoPyFFTw=True

# ESTIMATED CORRECTION FACTOR FOR BETA CORRELATIONS

def _Correction3(grad, nchannels, dv):
    low  = 1./dv/nchannels

    i0 = (nchannels - 1.)/dv/nchannels/2. # Integral for grad = 0

    n1 = sfft.fftfreq(nchannels, d=dv)
    dk = np.abs(n1[1] - n1[0])
    b =  (low/2.  /(np.abs(n1)) )**grad 
    C = np.sum(b[1:n1.size//2]) * dk
    A = C/i0

    nf = float(nchannels)
    return nf/(1. + A * (nf -1.))


C3 = np.vectorize(_Correction3)

from pipeline.Tools import Filtering




class FNoise:
        

    def __init__(self, ratio, dG, dGFreq, alpha, cutoff, beta, sampleRate, dv, _nFreq, _nSamples, 
                 fftMode='scipy', random='numpy', GSL_RNG_TYPE='mt19937',
                 threads=1, planner='FFTW_MEASURE', whiteNoise=False, filterScale=None):
        """

        Arguments
        ratio - If True, dG is the ratio of 1/f to white at timescale
        dG - The rms fluctuations of the 1/f noise at timescale
        dGFreq - timescale in Hz at which the 1/f noise have a power of rms
        """
        comm = MPI.COMM_WORLD
        rank = comm.rank
        size = comm.size

        self.GaussianMode = True

        self.random = random
        self.fftMode = fftMode

        if NoGSL & (self.random.lower() == 'gsl'):
            if rank == 0:
                print('GSL selected but no GSL library found.')
            self.random = 'numpy'

        if NoPyFFTw & (self.fftMode.lower() == 'fftw'):
            if rank == 0:
                print('FFTw selected but no FFTw library found.')
            self.fftMode = 'scipy'

        # Random number generator
        if self.random.lower() == 'gsl':
            os.environ['GSL_RNG_TYPE'] = GSL_RNG_TYPE#'gfsr4'#'taus2'#
            self.rng = PyGSL_RNG.GSLRandom(str(int(time.time()*1e5 + rank*size)))
        else:
            self.rng = None

        # First make sure that nSamples is a power of 2
        self.nSamples = int(2**np.ceil(np.log(_nSamples*2)/np.log(2.)))
        self.nFreq    = int(2**np.ceil(np.log(_nFreq*2)/np.log(2.)))

        self.filterScale = filterScale 
        
        # Generate containers
        if self.fftMode.lower() == 'fftw':
            self.P = pyfftw.empty_aligned((self.nFreq, self.nSamples))
            t1 = pyfftw.empty_aligned(self.P.shape, dtype='complex64')
            self.phases = pyfftw.empty_aligned(self.P.shape, dtype='complex64')
            self.rand = pyfftw.empty_aligned((1, self.P.shape[0], self.P.shape[1]*2))

            if self.nFreq > 1:
                self.ifft_object = pyfftw.FFTW(self.phases,t1, axes=(-2,-1),  direction='FFTW_BACKWARD')
            else:
                self.ifft_object = pyfftw.FFTW(self.phases,t1,  direction='FFTW_BACKWARD')
        else:
            self.P      = np.zeros((self.nFreq, self.nSamples))
            self.phases = np.zeros(self.P.shape, dtype='complex64')
            self.rand   = np.zeros((1, self.P.shape[0], self.P.shape[1]*2))
            if self.nFreq > 1:
                self.ifft_object = sfft.ifft2
            else:
                self.ifft_object = sfft.ifft

        # Generate a blank power spectrum array
        self.P[...] = 1.
        
        self.ratio = ratio
        self.dG = dG
        self.dGFreq = dGFreq

        self.rms = 1./dv # fractional rms of the white noise, to calculate knee

        self.alpha = alpha
        self.cutoff = cutoff
        self.sampleRate = sampleRate
        self.grad = (1. - beta)/beta
        self.dv = dv
        
        # Correction for 1/f noise FFT volume normalisation
        if self.nFreq > 1:
            self.fudge = C3(self.grad, self.nFreq, self.dv)
        else:
            self.fudge = 1.

        #Simulate white-noise too?
        self.whiteNoise = whiteNoise

        self.CalcSpectrum()


        # A CONTAINER FOR THE RANDOM PHASES
        self.output = np.zeros(self.P.shape, dtype='float32')

    def CalcSpectrum(self):
        """
        Calculates the power spectrum of the noise given the input parameters
        """

        # Lastly, apply a high-pass filter kernel
        if not isinstance(self.filterScale, type(None)):
            self.window = Filtering.CosineWindow(self.nSamples, self.sampleRate, 1./self.filterScale, 1./self.filterScale/2.)


        # Setup FFT grid
        # The idea here is that we want to generate
        # the FFT power that is 'colour' UNcorrected for 
        self.P *= 0.
        self.P += 1. 

        # Colour correct the frequencies
        low  = float(self.sampleRate)/self.P.shape[1]
        n0 = sfft.fftfreq(self.P.shape[1], d=1./self.sampleRate)
        n0[0] += low/2.

        low  = 1./self.dv/self.P.shape[0]
        n1 = sfft.fftfreq(self.P.shape[0], d=self.dv)
        n1[0] += low/2.


        nx, ny = np.meshgrid(n0, n1)


        # Now calculate the time based correlations
        a = (1./(np.abs(nx) + 1./self.cutoff ))**self.alpha 

        # This function defines the frequency correlations
        b =  (n1[0]  /np.abs(ny) )**self.grad 

        self.P *=  a
        self.P *=  b

        # Amplitude of 1/f component
        P0 = self.dG * self.dGFreq**self.alpha 
        if self.ratio:
            P0 *= self.rms

        
        A0 = P0 * self.fudge
        

        self.P *= A0 

        if self.whiteNoise:
             self.P += self.rms 


        self.P[0,0] = 0.


        # Lastly, apply a high-pass filter kernel
        if not isinstance(self.filterScale, type(None)):
            self.P *= self.window.window

        self.P = np.sqrt(self.P * self.P.size * self.sampleRate)

    def _GetTemporalPower(self, nu):
        low  = float(self.sampleRate)/self.P.shape[1]
        n0 = sfft.fftfreq(self.P.shape[1], d=1./self.sampleRate)
        n0[0] += low/2.

        mdl = interp1d(n0, self.P[0,:]**2/(self.P.size * self.sampleRate), bounds_error=False)

        return mdl(nu)

    def RandomDump(self):

        if self.random == 'gsl':
            if self.GaussianMode:
                self.rng.GenerateGaussian(self.rand)
            else:
                self.rng.GenerateUniform(self.rand)
        else:
            if self.GaussianMode:
                self.rand = np.random.normal(loc=0, scale=1, size=self.rand.shape)
            else:
                self.rand = np.random.uniform(0, 1, size=self.rand.shape)
           

    def Realisation(self, _nFreq, _nSamples, dknee=1):
        
        #self.rand = np.random.normal(size=(self.P.shape[0], self.P.shape[1]*2))
        #print a.shape
        #print np.sum(a), np.sum(self.phases)
        self.phases *= 0.

        self.RandomDump()
        if self.GaussianMode:
            self.phases += 1.*self.rand[0,:,self.P.shape[1]:]
            self.phases += 1j*self.rand[0,:,:self.P.shape[1]]
        else:
            self.phases +=    np.cos(2.*np.pi*self.rand[0,:,self.P.shape[1]:])
            self.phases += 1j*np.sin(2.*np.pi*self.rand[0,:,:self.P.shape[1]])
            self.phases *= np.sqrt(2)
        self.phases *= self.P 
            
        # Perform the inverse fft to get the correlated noise back
        self.output = np.real(self.ifft_object(self.phases))

        return self.output[:int(_nFreq), :int(_nSamples)]

    def FreeRandom(self):
        if self.random == 'gsl':
            self.rng.free()
        else:
            pass

    def _setSeed(self, seed):
        if self.random == 'gsl':
            self.rng._setSeed(int(seed))
        else:
            pass
