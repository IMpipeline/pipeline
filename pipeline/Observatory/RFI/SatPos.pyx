cdef extern from "math.h":
    double M_PI
    double sin(double x)
    double cos(double x)
    double tan(double x)
    double asin(double x)
    double acos(double x)
    double atan(double x)
    double atan2(double x, double y)
    double sqrt(double x)
    double pow(double x, double y)
    double floor(double x)
    double fabs(double x)
    
import numpy as np
cimport numpy as np

DTYPE = np.float
ctypedef  np.float_t DTYPE_t

ITYPE = np.int
ctypedef np.int_t ITYPE_t


cimport cython
from cpython.array cimport array, clone


cdef double mu = 3.98576e14
#cdef double Re = 6371e3
cdef double F = 298.257223563 # Earth flattening WGS-84
cdef double Re = 6378.137e3 # WGS84 Equatorial radius

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cdef double NewtonRaph(DTYPE_t M, DTYPE_t e):
    """
    Newton Raphson iterative minimisation to solve for eccentric anomaly (E) 
    given an input Mean Anomaly (M) and eccentricity (e).
    """
    
    cdef double E, E0, top, bot, 
    cdef int i, maxiter
    
    E0 = M
    maxiter = 5
    
    for i in range(maxiter):
        
        top = (M - E0 + e*sin(E0))
        bot = (1. - e*cos(E0))
        
        E = E0 + top/bot
        
        E0 = E
            
    return E

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cdef double TrueAnomaly(DTYPE_t E, DTYPE_t e):
    
    cdef double v
    
    v = 2. * atan(sqrt((1+e)/(1-e)) * tan(0.5 * E) )
    
    return v
    
@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)  
cdef double Semimajor(DTYPE_t P):
    
    cdef double T
    
    T = 24.*3600./P 
    return pow(T*T*mu/4./M_PI/M_PI, 1./3.)
    
@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cdef double jd2gst(DTYPE_t jd):
    
    cdef double jd0, jdi, T, T0, H, D ,D0, jdmin
    jdi = jd + 2400000.5
    
    T0 = (jdi - 2451545.0)/ 36525.0
    T = 67310.54841 + T0 * (876600 * 3600 + 8640184.812866 + T0 * (0.093104 - T0 * 6.2 * 10e-6))
    
    #2#451545
    #jdmin = floor(jdi)-0.5    
    #H = (jdi - jdmin)* 24.
    #D = jdi - 2451545.0
    #D0 = jdmin - 2451545.0
    #T = D/36525.
    
    #T0 = 6.697374558 + 0.06570982441908*D0  + 1.00273790935*H + 0.000026*T*T
    #T0 = T0 % 24.
    #print('JD', jdi, floor(T0), floor((T0-floor(T0))*60.) )
    #T0 *= 15.
    #T0 *= M_PI/180.
    
    #jd0 = floor(jdi - 0.5) + 0.5
    #T = (jd0 - 2451545.0) / 36525.
    #T0 = 6.697374558 + 2400.051336 * T + 0.000025862 * T*T
    #T0 = T0 % 24
    #ut = (jdi - jd0) * 24
    #T0 += ut * 1.002737909
    #T0 = T0 % 24
    return (T / 240. * M_PI/180.) % (2 * M_PI)
    
@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
cdef SatCoord(DTYPE_t[:] r, DTYPE_t a, DTYPE_t e, DTYPE_t E, DTYPE_t v, DTYPE_t w, DTYPE_t O, DTYPE_t inc):
    """
    Satellite geocentric celestial sphere coordinates from Spherical Astronomy Sec. 7.2
    """
    
    cdef double r0 , c, s, co, so, ci, si
    
    r0 = a * (1. - e * cos(E))
    c = cos(v + w)
    s = sin(v + w)
    co = cos(O)
    so = sin(O)
    ci = cos(inc)
    si = sin(inc)
    
    r[0] = r0 * (c * co - s * so * ci)
    r[1] = r0 * (c * so + s * co * ci)
    r[2] = r0 * s * si

@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
def SatPos(DTYPE_t[:] t, DTYPE_t t0, DTYPE_t P, DTYPE_t M0, DTYPE_t e, DTYPE_t inc, DTYPE_t O, DTYPE_t w, DTYPE_t lon, DTYPE_t lat, DTYPE_t alt, DTYPE_t[:,:] out):
    """
    t - Time of Observation (MJD)
    
    t0 - Time of TLE (MJD)
    P - Mean Motion (revs per day)
    M - Mean Anomaly (degrees, at t0)
    e - Eccentricity of orbit
    i - Inclination
    O - RA or Longitude of Ascending Node
    w - Argument of perigee
    
    lon - longitude of observer
    lat - latitude of observer
    alt - altitude of observer
    
    out - the array containing the returned values

    returns distance (m), ra (rad), ha (rad), dec (rad)
    """
    
    cdef array template = array('d')
    cdef DTYPE_t[:] r, robs
    #cdef DTYPE_t[:] az, el, rho
    r  = clone(template, 3, False)
    robs  = clone(template, 3, False)
    #az = clone(template, t.shape[0], False)
    #el = clone(template, t.shape[0], False)
    
    #dec = clone(template, t.shape[0], False)
    #rho = clone(template, t.shape[0], False)
    cdef double M, E, v, gst, fact, ra, dec, ha
    cdef int i
    
    fact = 1. / sqrt(1. + (1./F - 2.) * sin(lat) * sin(lat)/F) # Corrects for the flattening of the Earth.
    
    
    a = Semimajor(P)
    for i in range(t.shape[0]):
        M = (M0 + 2 * M_PI * P * (t[i] - t0)) % (2 * M_PI)
        E = NewtonRaph(M, e)
        v = TrueAnomaly(E, e)

        SatCoord(r, a, e, E, v, w, O, inc)
        
        # Need to convert observer vector into equivalent position in the celestial frame.
        gst = jd2gst(t[i])
        robs[0] = (alt + Re*fact) * cos(lon + gst) * cos(lat)
        robs[1] = (alt + Re*fact) * sin(lon + gst) * cos(lat)
        robs[2] = (alt + Re*fact) * sin(lat)
        
        # Convert to RA and Dec 
        out[0,i] = sqrt((r[0]-robs[0])**2 + (r[1]-robs[1])**2 + (r[2]-robs[2])**2)
        ra  = atan2((r[1]-robs[1]), (r[0]-robs[0]))
        if ra > 2.*M_PI:
            ra -= 2.*M_PI
        elif ra < 0:
            ra += 2.*M_PI
            
        ha = gst - lon - ra
        if ha > 2.*M_PI:
            ha -= 2.*M_PI
        elif ha < 0:
            ha += 2.*M_PI
        dec = asin((r[2]-robs[2])/out[0,i])
        
        # Convert to Az/El too?
        robs[0] = - cos(ha) * cos(dec) * sin(lat) + sin(dec) * cos(lat)
        robs[1] = - sin(ha) * cos(dec)
        robs[2] =   cos(ha) * cos(dec) * cos(lat) + sin(dec) * sin(lat)
        v = sqrt(robs[0]*robs[0] + robs[1]*robs[1]) # reusing a temp variable!
        
        out[1,i] = atan2(robs[1], robs[0])
        out[2,i] = atan2(robs[2], v)
        
    del r, robs
    #return rho, az, el
    
def SatPosCoords(DTYPE_t[:] t, DTYPE_t t0, DTYPE_t P, DTYPE_t M0, DTYPE_t e, DTYPE_t inc, DTYPE_t O, DTYPE_t w, DTYPE_t lon, DTYPE_t lat, DTYPE_t alt, DTYPE_t[:,:] out, DTYPE_t[:,:] r):
    """
    t - Time of Observation (MJD)
    
    t0 - Time of TLE (MJD)
    P - Mean Motion (revs per day)
    M - Mean Anomaly (degrees, at t0)
    e - Eccentricity of orbit
    i - Inclination
    O - RA or Longitude of Ascending Node
    w - Argument of perigee
    
    lon - longitude of observer
    lat - latitude of observer
    alt - altitude of observer
    
    out - the array containing the returned values
    r - array of position vectors

    returns distance (m), ra (rad), ha (rad), dec (rad)
    """
    
    cdef array template = array('d')
    cdef DTYPE_t[:]  robs
    #cdef DTYPE_t[:] az, el, rho
    #r  = clone(template, 3, False)
    robs  = clone(template, 3, False)
    #az = clone(template, t.shape[0], False)
    #el = clone(template, t.shape[0], False)
    
    #dec = clone(template, t.shape[0], False)
    #rho = clone(template, t.shape[0], False)
    cdef double M, E, v, gst, fact, ra, dec, ha
    cdef int i
    
    fact = 1. / sqrt(1. + (1./F - 2.) * sin(lat) * sin(lat)/F) # Corrects for the flattening of the Earth.
    
    
    a = Semimajor(P)
    for i in range(t.shape[0]):
        M = (M0 + 2 * M_PI * P * (t[i] - t0)) % (2 * M_PI)
        E = NewtonRaph(M, e)
        v = TrueAnomaly(E, e)

        SatCoord(r[:,i], a, e, E, v, w, O, inc)
        
        # Need to convert observer vector into equivalent position in the celestial frame.
        gst = jd2gst(t[i])
        robs[0] = (alt + Re*fact) * cos(lon + gst) * cos(lat)
        robs[1] = (alt + Re*fact) * sin(lon + gst) * cos(lat)
        robs[2] = (alt + Re*fact) * sin(lat)
        
        # Convert to RA and Dec 
        out[0,i] = sqrt((r[0,i]-robs[0])**2 + (r[1,i]-robs[1])**2 + (r[2,i]-robs[2])**2)
        ra  = atan2((r[1,i]-robs[1]), (r[0,i]-robs[0]))
        if ra > 2.*M_PI:
            ra -= 2.*M_PI
        elif ra < 0:
            ra += 2.*M_PI
            
        ha = gst - lon - ra
        if ha > 2.*M_PI:
            ha -= 2.*M_PI
        elif ha < 0:
            ha += 2.*M_PI
        dec = asin((r[2,i]-robs[2])/out[0,i])
        
        # Convert to Az/El too?
        robs[0] = - cos(ha) * cos(dec) * sin(lat) + sin(dec) * cos(lat)
        robs[1] = - sin(ha) * cos(dec)
        robs[2] =   cos(ha) * cos(dec) * cos(lat) + sin(dec) * sin(lat)
        v = sqrt(robs[0]*robs[0] + robs[1]*robs[1]) # reusing a temp variable!
        
        out[1,i] = atan2(robs[1], robs[0])
        out[2,i] = atan2(robs[2], v)
        
    del robs
    #return rho, az, el

    
        
@cython.cdivision(True)
@cython.boundscheck(False)
@cython.wraparound(False)
def SatDist(DTYPE_t minAngle, DTYPE_t[:,:] dtheta, ITYPE_t[:] mask,
            DTYPE_t[:] A,DTYPE_t[:] B, DTYPE_t[:] C,
            DTYPE_t[:] D):

    cdef int i, j

    cdef int nsamps = dtheta.shape[0]
    cdef int nfreqs = dtheta.shape[1]
    cdef double dist

    for i in range(nsamps):
        
        dist = acos(A[i] + B[i]*C[i])*180./M_PI
        for j in range(nfreqs):
            dtheta[i,j] = dist * D[j]

        if dist < minAngle:
            mask[i] = 0
