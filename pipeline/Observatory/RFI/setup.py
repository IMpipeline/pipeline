from distutils.core import setup
from distutils.extension import Extension
from Cython.Distutils import build_ext
from Cython.Build import cythonize
import numpy as np

sourcefiles = ['SatPos.pyx']
ext_modules = [Extension('SatPos', sourcefiles, include_dirs=[np.get_include()])]

setup ( name = 'SatModule',
    cmdclass = {'build_ext':build_ext}, ext_modules=cythonize(ext_modules))
    