# IM Pipeline GNSS satellites module. v2
# This module includes a complete power spectral model of GNSS transmissions for:
# - GPS L5 and L2 
# - GLONASS G3 and G2
# - Galileo E5

import numpy as np
from pipeline.Observatory.RFI import SatPos
import jdcal
from scipy.interpolate import interp1d
import time

# PSD MODELS - Models normalised to sum to unity.
def BPSK(f, m, n=None, f0=1.023):
    '''
    Binary Phase Switch Keying PSD (also QPSK)
    
    Has a sinc**2 PSD. This PSD is normalised to sum to unity.
    
    '''
    
    return np.sinc(f/f0/m)**2/m/f0

def BOC(f, m, n, f0=1.023):
    '''
    Binary Offset Carrier (BOC). Combines a BPSK signal with a binary carrier mode.
    The chip size of the BPSK signal (Tc) is larger and an integer value of the
    carrier signal chap size (Ts). Tc > Ts.
    
    This PSD is normalised to sum to unity.
    
    n > m
    (Tc > Ts)
    
    '''
    fs = n * f0
    fc = m * f0
    A = np.sin(np.pi*f/2./fs)**2 
    B = np.sin(np.pi*f/fc)**2
    C = np.cos(np.pi*f/2./fs)**2
    
    return fc * A * B / np.pi**2 / f**2 /C
    
def BOCc(f, m, n, f0=1.023):
    '''
    cosine Binary Offset Carrier (BOCc). Combines a BPSK signal with a binary carrier mode.
    The chip size of the BPSK signal (Tc) is larger and an integer value of the
    carrier signal chap size (Ts). Tc > Ts.
    
    This PSD is normalised to sum to unity.
    
    n > m
    (Tc > Ts)
    
    '''

    fs = n * f0
    fc = m * f0
    phi = int(2. * n/m)
    
    if np.mod(phi, 2) == 1:
        B = 4.*np.cos(np.pi*f/fc)**2
    else:
        B = 4.*np.sin(np.pi*f/fc)**2

    A = np.sin(np.pi*f/4./fs)**4 
    C = np.cos(np.pi*f/2./fs)**2
    
    return fc* A * B / np.pi**2 / f**2 / C


def altBOC(f, m, n, f0=1.023):
    '''
    Binary Offset Carrier (BOC). The altBOC name refers to use a QPSK multiplexing signal.
    The chip size of the BPSK signal (Tc) is larger and an integer value of the
    carrier signal chap size (Ts). Tc > Ts.
    
    This PSD is normalised to sum to unity.
    
    n > m
    (Tc > Ts)
    
    '''    
    
    fs = n * f0
    fc = m * f0


    phi = int(2. * n/m)
    if np.mod(phi, 2) == 0: # Phi-even spectrum
        A = np.sin(np.pi*f/fc)**2
        B = np.pi**2 * f**2 * np.cos(np.pi*f/2./fs)**2
        C = (1. - np.cos(np.pi*f/2./fs))
        return fc * A / B * C
    else: # Phi-odd spectrum
        A = np.cos(np.pi*f/fc)**2
        B = np.pi**2 * f**2 * np.cos(np.pi*f/2./fs)**2
        C = (1. - np.cos(np.pi*f/2./fs))
        return fc * A / B * C





# SATELLITE SIGNAL MODELS - Frequency structures from Hofmann-Wellenhof et al. 2008 (Book)
# Power measurements from measurements in: Steigenberger et al. 2017
# Beam powers from Steigenberger
# Power emitted for each band of GPS from: http://acc.igs.org/orbits/thrust-power.txt
rf    = {'BIIR': # GPS IIR-M Block
                 {'L1': {'p': {'eirp': 13.5 + 13.5, 'nu':1575.42, 'm':10, 'n':None, 'f0':1.023, 'mode':BPSK},
                         'ca': {'eirp': 16.5 + 13.5, 'nu':1575.42, 'm':1, 'n':None, 'f0':1.023, 'mode':BPSK}
                        },
                  'L2': {'p': {'eirp': 8.75 + 13.5, 'nu':1227.60, 'm': 10, 'n':None,'f0':1.023, 'mode':BPSK}
                        }
                 },
        'BIIRM': # GPS IIR-M Block
                 {'L1': {'p': {'eirp': 13.5 + 13.5, 'nu':1575.42, 'm':10, 'n':None, 'f0':1.023, 'mode':BPSK},
                         'ca': {'eirp': 16.5 + 13.5, 'nu':1575.42, 'm':1, 'n':None, 'f0':1.023, 'mode':BPSK},
                         'm': {'eirp': 18.2 + 13.5 , 'nu':1575.42, 'm':5,'n':10,'f0':1.023,'mode':BOC}
                        },
                  'L2': {'p': {'eirp': 10.5 + 13.5, 'nu':1227.60, 'm': 10, 'n':None,'f0':1.023, 'mode':BPSK},
                         'ca':{'eirp': 11.2 + 13.5 , 'nu':1227.60, 'm':1, 'n':None, 'f0':1.023, 'mode':BPSK},
                         'm': {'eirp': 15.2 + 13.5 , 'nu':1227.60, 'm':5,'n':10,'f0':1.023,'mode':BOC}
                        }
                 },
         'BIIF': # GPS IIF Block
                 {'L1': {'p': {'eirp':13.5 + 13.5, 'nu':1575.42, 'm':10, 'n':None, 'f0':1.023, 'mode':BPSK},
                         'ca': {'eirp':16.5 + 13.5, 'nu':1575.42, 'm':1, 'n':None, 'f0':1.023, 'mode':BPSK},
                         'm': {'eirp': 18.2 + 13.5 , 'nu':1575.42, 'm':5,'n':10,'f0':1.023,'mode':BOC}
                        },
                  'L2': {'p': {'eirp': 10.5 + 13.5, 'nu':1227.60, 'm': 10, 'n':None,'f0':1.023, 'mode':BPSK},
                         'ca':{'eirp': 11.2 + 13.5, 'nu':1227.60, 'm':1, 'n':None, 'f0':1.023, 'mode':BPSK},
                         'm': {'eirp': 15.2 + 13.5 , 'nu':1227.60, 'm':5,'n':10,'f0':1.023,'mode':BOC}
                        },
                  'L5': {'p': {'eirp': 16. + 13.5 , 'nu': 1176.45, 'm': 10, 'n':None,'f0':1.023, 'mode':BPSK}
                        }
                 },
        'Galileo': # Neglecting the E1 Signal plan (absorbed into power estimates)
                 {'E6': {'p': {'eirp': 15. + 15. , 'nu': 1278.75 , 'm': 5, 'n':10,'f0':1.023, 'mode':BOCc},
                         'ca': {'eirp': 18. + 15. , 'nu': 1278.75 , 'm': 5, 'n':None,'f0':1.023, 'mode':BPSK}
                        },
                  'E5': {'p': {'eirp': 18. + 15. , 'nu': 1191.795 , 'm': 10, 'n':15,'f0':1.023, 'mode':altBOC}
                         }
                 },
        'M': # GLONASS M satellites 
                 {'G1': {'p':  {'eirp': 13 + 13.5 , 'nu': 1602., 'm': 10, 'n':None,'f0':0.511, 'mode':BPSK},
                         'ca': {'eirp': 13 + 13.5 , 'nu': 1602., 'm': 1, 'n':None,'f0':0.511, 'mode':BPSK}
                        },
                  'G2': {'p':  {'eirp': 10 + 13.5 , 'nu': 1246., 'm': 10, 'n':None,'f0':0.511, 'mode':BPSK},
                         'ca': {'eirp': 10 + 13.5 , 'nu': 1246., 'm': 1, 'n':None,'f0':0.511, 'mode':BPSK}
                        }
                 },                 
        'K': # GLONASS K satellites 
                 {'G1': {'p': {'eirp': 13 + 13.5 , 'nu': 1602., 'm': 10, 'n':None,'f0':0.511, 'mode':BPSK},
                         'ca': {'eirp': 13 + 13.5 , 'nu': 1602., 'm': 1, 'n':None,'f0':0.511, 'mode':BPSK}
                        },
                  'G2': {'p': {'eirp': 10 + 13.5 , 'nu': 1246, 'm': 10, 'n':None,'f0':0.511, 'mode':BPSK},
                         'ca': {'eirp': 10 + 13.5 , 'nu': 1246, 'm': 1, 'n':None,'f0':0.511, 'mode':BPSK}
                        },
                  'G3': {'p': {'eirp': 14.8 + 13.5 , 'nu': 1207.14, 'm': 10, 'n':None,'f0':1.023, 'mode':BPSK}
                        }
                 }
        }
        
def line(rf, nu):
    
    # ref step = 0.05 MHz
    refnu = 0.05
    
    dnu = np.abs(nu[1] - nu[0])
    
    

    if refnu > dnu:
        refnu = dnu
    
    # How over sampled does the dnu need to be?
    step = int(dnu / refnu + 0.5)
    
    nu2 = np.linspace(nu[0], nu[-1], nu.size*step)
    power = {}
    for ksat, sat in rf.items():
        power[ksat] = np.zeros((1, nu2.size))
        for kline, line in sat.items():
            for kband, band in line.items():
                
                power[ksat][0,:] += 10**(band['eirp']/10.)*band['mode'](nu2 - band['nu'], band['m'], band['n'], band['f0'])*refnu

        # Now downsample by step and scale by chan width
        if dnu > refnu:
            power[ksat] = np.sum(np.reshape(power[ksat][0,:], (1, nu.size, step)) , axis=2)
        else:
            power[ksat] = power[ksat][0,:]

    return power

# TLE reading functions and container
class TLE(object):
    def __init__(self):
        self.mjd = None
        self.i = None
        self.e = None
        self.w = None
        self.O = None
        self.M0 = None
        self.P = None
        
def ReadTLEs(filenames):
    """
    Assumes that the TLEs are in the format given on celestrak.com
    files should be named: Constellation.dat
    """
        
    # Count lines first
    totallines = 0
    for filename in filenames:
        f = open(filename, 'r') 
        nlines = 0
        for nlines, n in enumerate(f):
            pass
        f.close()
        nlines += 1
        totallines += nlines
        
    TLEs = np.empty(totallines//3, dtype='object')

    k = 0
    for filename in filenames:
        f = open(filename, 'r') 
        for i, line in enumerate(f):
            if np.mod(i,3)==0:
                line0 = line
            elif np.mod(i,3) == 1:
                line1 = line
            else:
                line2 = line
                TLEobj = TLE()
                epoch_year = line1[18:20]
                epoch_day = float(line1[20:32])
                year = int('20'+epoch_year)
                jd, mjd = jdcal.gcal2jd(year,1,1)
                
                fname = filename.split('/')[-1]
                
                satname = line0.split(' ')
                if 'GPS' in satname[0]:
                    TLEobj.name = satname[1].split('-')[0]
                if 'COSMOS' in satname[0]:
                    if 'K' in satname[-1]:
                        TLEobj.name = 'K'
                    else:
                        TLEobj.name = 'M'
                if 'GSAT' in satname[0]:
                    TLEobj.name = 'Galileo'
                    
                TLEobj.mjd = mjd + epoch_day
            
                TLEobj.i = float(line2[8:16])  * np.pi/180.
                TLEobj.O = float(line2[17:25]) * np.pi/180.
                TLEobj.e = float(line2[26:33]) * 1e-7
                TLEobj.w = float(line2[34:42]) * np.pi/180.
                TLEobj.M0= float(line2[43:51]) * np.pi/180.
                TLEobj.P = float(line2[52:63]) 
                        
                TLEs[k] = TLEobj
                k += 1
        f.close()
    return TLEs

# Main Function:
kb = 1.38064852e-23
c  = 299792458.

class Satellites():
    
    def __init__(self, nu, nsamp, minAngle, filelist):
        '''
        Arguments
        nu - centre frequencies to calculate simulated bandpass (MHz)
        nsamp - Size of tod samples to simulate
        minAngle - minimum separation angle between beam axis and satellites
        filelist - the list of TLE files
        '''
        self.TLEs=ReadTLEs(filelist)
        #self.beamf0 = beamf0

        # NEED TO MODIFY FOR OTHER INPUT BEAM FORMATS
        #btheta, a, logG = np.loadtxt(beamfile).T
        #self.bmdl = interp1d(btheta, logG/10., bounds_error=False, fill_value=-100)
        
        self.power = line(rf, nu)
        self.nu = nu # Should be MHz
        self.dnu = np.abs(nu[1] - nu[0]) # Should be MHz
        self.minAngle = minAngle

        self.f2 = np.reshape(self.nu*1e6, (1, self.nu.size))**2

        self.dtheta = np.zeros((nu.size, nsamp))
        self.out = np.zeros((3, nsamp))

        

    def Run(self, tod, mask, az0, el0, jd, lat, lon, alt, Beam, satCount=None, satCountEl = 30, dist=None, satel=None, sataz=None):
        """
        Expects inputs in radians.
        nu - frequency in MHz
        Beam - BeamClass object

        keywords 
        satCount - if array passed returns the number of satellites above horizon
        satCountEl - min elevation used for satCount (degrees)
        """

        countSats = not isinstance(satCount, type(None))
        distSats = not isinstance(dist, type(None))
        getAz = not isinstance(sataz, type(None))
        getEl = not isinstance(satel, type(None))
        
        # TIMING CODE
        times = np.zeros((3, len(self.TLEs)))
        #
        
        self.dtheta *= 0.
        self.out *= 0.
        if countSats:
            satCount *= 0.
        if distSats:
            dist *= 0.
            #dist += 500. # Because we check against the closest sat at time T
        if getAz:
            sataz *= 0
        if getEl:
            satel *= 0
        for i, TLE in enumerate(self.TLEs):
            # TIMING CODE
            t0 = time.time()
            #

            self.dtheta *= 0.
            SatPos.SatPos(jd, 
                          TLE.mjd ,
                          TLE.P, 
                          TLE.M0, 
                          TLE.e, 
                          TLE.i, 
                          TLE.O, 
                          TLE.w, 
                          lon, lat ,alt, self.out)    
                              
            # Calculate angular separation distance 
            A = np.sin(self.out[2,:])*np.sin(el0)
            B = np.cos(self.out[2,:])*np.cos(el0)
            C = np.cos(np.mod(self.out[1,:], np.pi*2) - az0)
            
            if getAz:
                sataz[:,i] = self.out[1,:]*180./np.pi
            if getEl:
                satel[:,i] = self.out[2,:]*180./np.pi
            
            self.dtheta[0,:] += np.arccos(A + B*C)
            if countSats:
                satCount[self.out[2,:] > satCountEl*np.pi/180.] += 1

            if distSats:
                #gd = (self.dtheta[0,:]*180./np.pi < dist) 
                dist[:,i] = self.dtheta[0,:]*180./np.pi#self.dtheta[0,gd]*180./np.pi

            d2 = self.dtheta[0,:]*180./np.pi
            mask[self.dtheta[0,:] < self.minAngle*np.pi/180.] = 0.


            # TIMING CODE
            t1 = time.time()
            #

            # Interpolate beam - this is quite slow (the 10** is slow even!?)
            #dtheta[...] = 10**(self.bmdl(dtheta))
            
            Beam.Gain(self.nu, self.dtheta)
            

            # TIMING CODE
            t2 = time.time()
            #

            # Free space path loss
            L =  ((c/4./np.pi/ self.out[0:1,:].T)**2/self.f2 ).T
                        
            # Calculate the receiver antenna temperature and remove signals less than 0 EL.
            temp = self.dtheta  * (L * self.power[TLE.name].T) / kb / self.dnu/1e6
            
            #print(np.max(self.dtheta), np.median(self.dtheta), np.median(tod), np.max(tod))
            tod += temp
            

            #from matplotlib import pyplot
            #pyplot.plot( (tod[15,:] - np.mean(tod[15,:]))/np.std(tod[15,:]) )
            #pyplot.plot( (d2 - np.mean(d2))/np.std(d2) )
            #pyplot.show()

            #a = (tod[15,:] - np.mean(tod[15,:]))/np.std(tod[15,:])
            #b = (d2 - np.mean(d2))/np.std(d2) 
            #pyplot.plot(b, a, '.')
            #pyplot.show()


            # TIMING CODE
            t3 = time.time()
            times[0,i] = t1-t0
            times[1,i] = t2-t1
            times[2,i] = t3-t2
            #

        #from matplotlib import pyplot
        #pyplot.plot(tod[10,:])
        #pyplot.plot(temp[10,:])
        #pyplot.show()

        #print('TIMINGS:', np.mean(times,axis=1), np.std(times,axis=1))
            
            
    def RunGetPos(self, tod, mask, az0, el0, jd, lat, lon, alt):
        """
        Expects inputs in radians.
        nu - frequency in MHz
        beamf0 - Central frequency of beam model (MHz)
        """
    
        f2 = np.reshape(self.nu*1e6, (1, self.nu.size))**2 
        azel = np.zeros((len(self.TLEs), 2, tod.shape[0]))
        for i, TLE in enumerate(self.TLEs):
            
            # Returns distance, az, el
            out = np.array(SatPos.SatPos(jd, 
                                         TLE.mjd ,
                                         TLE.P, 
                                         TLE.M0, 
                                         TLE.e, 
                                         TLE.i, 
                                         TLE.O, 
                                         TLE.w, 
                                         lon, lat ,alt))
            bd = (out[2,:] < 0)
            #gd = (out[2,:] > 0)
    
            azel[i,0,:] = out[1,:]
            azel[i,1,:] = out[2,:]
    
    
            A = np.sin(out[2,:])*np.sin(el0)
            B = np.cos(out[2,:])*np.cos(el0)
            C = np.cos(np.mod(out[1,:], np.pi*2) - az0)
            
            
            dtheta = (np.arccos(A + B*C)*180./np.pi)
            mask[dtheta < self.minAngle] = 0.
            dtheta = np.reshape(dtheta, (dtheta.size, 1))
            dtheta = dtheta*self.nu/self.beamf0 
            Gtele = 10**(self.bmdl(dtheta)/10.)
    
            #dr = dtheta - mindist
            #rgd = (dr < 0)    
            #mindist[rgd & gd] = dtheta[rgd & gd]


            # Free space path loss
            L =  (c/4./np.pi/ out[0:1,:].T)**2/f2 
                
            # Calculate the receiver antenna temperature and remove signals less than 0 EL.
            temp = Gtele * (L * self.power[TLE.name].T) / kb
            temp[bd, :] = 0.
            tod += temp
            
        return azel
