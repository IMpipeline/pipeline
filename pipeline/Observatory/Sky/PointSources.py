import numpy as np
from matplotlib import pyplot
import healpy as hp
import h5py

import glob
import os
import subprocess
from scipy import integrate as si
from scipy.optimize import curve_fit

def Gaussian2D(A, fwhm, x, xc, y, yc):
    """
    """
    sig = fwhm/2.355

    xi = (x - xc)
    xi[xi < -np.pi] += 2*np.pi
    xi[xi >  np.pi] -= 2.*np.pi
    yi = (y - yc)

    sigx = sig/np.cos((np.pi/2.-y))
    
    r = (xi/sigx)**2 + (yi/sig)**2

    return A*np.exp(-r/2.)

#Start of lucas' code.

#import pdb

##### Physical constants

k_b = 1.38064852e-23   # Boltzmann constant
c = 299792458.0   # speed of light
jy_to_si = 1.e-26   # jansky to SI

##### Minimum flux (in Jy)

flux_min = 1.0e-6

##### Omega sky - full-sky
    
#omega_sky = 4.0 * np.pi
omega_sky = 1.0

################################################################################
################################################################################

##### Functions

### Source count models

# Battye et al. 2012
def source_count_battye(flux):

    """Source count for point sources at 1.4 GHz - Battye et al 2012.
       Inputs: flux (in Jy)
       Output: dn / ds in number of sources per unit flux  
    """

    # Polynomial constants

    S_0 = 1.0
    N_0 = 1.0

    a_0 = 2.593
    a_1 = 9.333e-2
    a_2 = -4.839e-4
    a_3 = 2.488e-1
    a_4 = 8.995e-2
    a_5 = 8.506e-3

    # Nomalized flux

    ss = flux / S_0

    # Differential source counts 

    if flux <= 1.0e0: 
        dn_ds = N_0 * flux**(-2.5) * 10**(a_0 + a_1 * np.log10(ss) + a_2 * (np.log10(ss))**2 + a_3 * (np.log10(ss))**3 + a_4 * (np.log10(ss))**4 + a_5 * (np.log10(ss))**5)

    # Interpolate for high fluxes (>1.0e0 Jy)

    if flux > 1.0e0:
        par = np.array([2.59300238, -2.40632446])
        dn_ds = 10.**(par[0] + par[1] * np.log10(flux))

    # Output

    return omega_sky * dn_ds

###############################################################################
###############################################################################

### Background temperature

def temp_background_fixed_freq(flux_max, source):

    """Background temperature (mK) at a fixed frequency (1.4 GHz).
       Inputs: flux_max: maximum flux (in Jy)
               source: model to use for the point sources - models implemented: battye
       Output: background temperature at a fixed frequency (1.4 GHz) in mK         
    """

    fixed_freq = 1.4e3   # MHz
    conversion_factor = 2 * k_b * ((fixed_freq * 1.0e6) / c)**2   # flux to temperature

    if flux_max <= 1.0e-7:
        sys.exit("Maximum flux has to be larger or equal to 1.0e-7 Jy.")

    if source == "battye":        
        if flux_max  <= 1.0e-3:
            N = 1.0e5    
            s_integrand = np.linspace(flux_min, flux_max, N)
            s_function = np.zeros(N)
            for i in range(0, int(N)):
                s_function[i] = s_integrand[i] * source_count_battye(s_integrand[i])
            temp_ps = (1.0 / conversion_factor) * jy_to_si * (si.simps(s_function, s_integrand))
        elif 1.0e-3 < flux_max <= 1.0e1:
            N = 1.0e5
            s_integrand_1 = np.linspace(flux_min, 1.0e-3, N)
            s_integrand_2 = np.linspace(1.0e-3, flux_max, N)
            s_function_1 = np.zeros(N)
            s_function_2 = np.zeros(N)
            for i in range(0, int(N)):
                s_function_1[i] = s_integrand_1[i] * source_count_battye(s_integrand_1[i])
                s_function_2[i] = s_integrand_2[i] * source_count_battye(s_integrand_2[i])
            temp_ps = (1.0 / conversion_factor)  * jy_to_si * (si.simps(s_function_1, s_integrand_1) + si.simps(s_function_2, s_integrand_2))
        elif 1.0e1 < flux_max <= 1.0e3:
            N = 1.0e5
            s_integrand_1 = np.linspace(flux_min, 1.0e-3, N, endpoint=True)
            s_integrand_2 = np.linspace(1.0e-3, 1.0e1, N, endpoint=True)
            s_integrand_3 = np.linspace(1.0e1, flux_max, N, endpoint=True)
            s_function_1 = np.zeros(N)
            s_function_2 = np.zeros(N)
            s_function_3 = np.zeros(N)
            for i in range(0, int(N)):
                s_function_1[i] = s_integrand_1[i] * source_count_battye(s_integrand_1[i])
                s_function_2[i]= s_integrand_2[i] * source_count_battye(s_integrand_2[i])
                s_function_3[i]= s_integrand_3[i] * source_count_battye(s_integrand_3[i])
            temp_ps = (1.0 / conversion_factor) * jy_to_si * (si.simps(s_function_1, s_integrand_1) + si.simps(s_function_2, s_integrand_2) + si.simps(s_function_3, s_integrand_3))
        elif flux_max > 1.0e3:
            N = 1.0e5
            s_integrand_1 = np.linspace(flux_min, 1.0e-3, N, endpoint=True)
            s_integrand_2 = np.linspace(1.0e-3, 1.0e1, N, endpoint=True)
            s_integrand_3 = np.linspace(1.0e1, 1.0e3, N, endpoint=True)
            s_function_1 = np.zeros(N)
            s_function_2 = np.zeros(N)
            s_function_3 = np.zeros(N)
            for i in range(0, int(N)):
                s_function_1[i] = s_integrand_1[i] * source_count_battye(s_integrand_1[i])
                s_function_2[i] = s_integrand_2[i] * source_count_battye(s_integrand_2[i])
                s_function_3[i] = s_integrand_3[i] * source_count_battye(s_integrand_3[i])
            temp_ps = (1.0 / conversion_factor) * jy_to_si * (si.simps(s_function_1, s_integrand_1) + si.simps(s_function_2, s_integrand_2) + si.simps(s_function_3, s_integrand_3))

    return 1000.0 * temp_ps

################################################################################
################################################################################

##### Poisson distribution

### Poisson at low flux

def cl_poisson_fixed_freq(flux_max, source):

    """Poisson distribution of the sources (mK^2) at a fixed frequency (1.4 GHz).
       Inputs: flux_max: maximum flux (in Jy)
               source: model to use for the point sources - models implemented: battye
       Output: Poisson contribution to the angular power spectrum of point sources at a fixed frequency (1.4 GHz) - (mK^2)     
    """
    
    fixed_freq = 1.4e3   # MHz
    conversion_factor = 2 * k_b * ((fixed_freq * 1.0e6)/ c)**2   # flux to temperature

    if flux_max <= 1.0e-7:
        sys.exit("Maximum flux has to be larger or equal to 1.0e-7 Jy.")

    if source == "battye":        
        if flux_max  <= 1.0e-3:
            N = 1.0e5    
            s_integrand = np.linspace(flux_min, flux_max, N)
            s_function = np.zeros(N)
            for i in range(0, int(N)):
                s_function[i] = s_integrand[i]**2 * source_count_battye(s_integrand[i])
            aps_poisson = (1.0 / conversion_factor)**2 * jy_to_si**2 * (si.simps(s_function, s_integrand))
        elif 1.0e-3 < flux_max <= 1.0e1:
            N = 1.0e5
            s_integrand_1 = np.linspace(flux_min, 1.0e-3, N)
            s_integrand_2 = np.linspace(1.0e-3, flux_max, N)
            s_function_1 = np.zeros(N)
            s_function_2 = np.zeros(N)
            for i in range(0, int(N)):
                s_function_1[i] = s_integrand_1[i]**2 * source_count_battye(s_integrand_1[i])
                s_function_2[i] = s_integrand_2[i]**2 * source_count_battye(s_integrand_2[i])
            aps_poisson = (1.0 / conversion_factor)**2 * jy_to_si**2 * (si.simps(s_function_1, s_integrand_1) + si.simps(s_function_2, s_integrand_2))
        elif 1.0e1 < flux_max <= 1.0e3:
            N = 1.0e5
            s_integrand_1 = np.linspace(flux_min, 1.0e-3, N, endpoint=True)
            s_integrand_2 = np.linspace(1.0e-3, 1.0e1, N, endpoint=True)
            s_integrand_3 = np.linspace(1.0e1, flux_max, N, endpoint=True)
            s_function_1 = np.zeros(N)
            s_function_2 = np.zeros(N)
            s_function_3 = np.zeros(N)
            for i in range(0, int(N)):
                s_function_1[i] = s_integrand_1[i]**2 * source_count_battye(s_integrand_1[i])
                s_function_2[i]= s_integrand_2[i]**2 * source_count_battye(s_integrand_2[i])
                s_function_3[i]= s_integrand_3[i]**2 * source_count_battye(s_integrand_3[i])
            aps_poisson = (1.0 / conversion_factor)**2 * jy_to_si**2 * (si.simps(s_function_1, s_integrand_1) + si.simps(s_function_2, s_integrand_2) + si.simps(s_function_3, s_integrand_3))
        elif flux_max > 1.0e3:
            N = 1.0e5
            s_integrand_1 = np.linspace(flux_min, 1.0e-3, N, endpoint=True)
            s_integrand_2 = np.linspace(1.0e-3, 1.0e1, N, endpoint=True)
            s_integrand_3 = np.linspace(1.0e1, 1.0e3, N, endpoint=True)
            s_function_1 = np.zeros(N)
            s_function_2 = np.zeros(N)
            s_function_3 = np.zeros(N)
            for i in range(0, int(N)):
                s_function_1[i] = s_integrand_1[i]**2 * source_count_battye(s_integrand_1[i])
                s_function_2[i] = s_integrand_2[i]**2 * source_count_battye(s_integrand_2[i])
                s_function_3[i] = s_integrand_3[i]**2 * source_count_battye(s_integrand_3[i])
            aps_poisson = (1.0 / conversion_factor)**2 * jy_to_si**2 * (si.simps(s_function_1, s_integrand_1) + si.simps(s_function_2, s_integrand_2) + si.simps(s_function_3, s_integrand_3))

    return 1000.0**2 * aps_poisson

### Poisson at high flux (flux_min has to be the same as flux_max of poisson_fixed_freq) - (flux_max ~ 10^3 Jy)

def map_poisson_fixed_freq_high_flow(flux_min_hf, flux_max_hf, nside, source, fwhm):

    """Poisson map for high fluxes (mK) at a fixed frequency (1.4 GHz).
       Inputs: flux_min_hf = minimum flux (in Jy)
               flux_max: maximum flux (in Jy)
               nside = nside map
               source: model to use for the point sources - models implemented: battye
       Output: map for high fluxes of the point sources that respects a Poisson distribution at a fixed frequency (1.4 GHz) - (mK)
    """

    fixed_freq = 1.4e3   # MHz
    conversion_factor = 2 * k_b * ((fixed_freq * 1.0e6)/ c)**2   # flux to temperature
    omega_pix = (4.0 * np.pi) / (12.0 * nside**2)
    omega_beam= 1.13*fwhm**2

    decades = np.floor(np.log10(flux_max_hf)) - np.floor(np.log10(flux_min_hf))

    dn_ds = np.zeros(10)

    map = np.zeros(hp.nside2npix(nside))
    pix = np.arange(12*nside**2)
    dec, ra = hp.pix2ang(nside, pix)

    c_decs = list()
    c_ras  = list()
    fluxes = list()

    if source == "battye":
        for i in range(0, int(decades)):
            s = np.linspace(flux_min_hf * 10**i, flux_min_hf * 10**(i + 1), 10)
            for j in range(0, 9):
                s_delta = np.linspace(s[j], s[j + 1], 10, endpoint=False)   
                for k in range(0, 10):
                    dn_ds[k] = source_count_battye(s_delta[k])
                delta_n = np.ceil(si.simps(dn_ds, s_delta))
                for l in range(0, int(delta_n)):
                    stemp = np.random.uniform(low=s[j], high=s[j + 1])
                    temp = (1.0 / conversion_factor) * jy_to_si * (1.0 / omega_beam) * stemp
                    map_index = np.random.randint(low=0, high=(hp.nside2npix(nside) - 1))

                    c_dec, c_ra = hp.pix2ang(nside, map_index)

                    c_ras  += [c_ra*180./np.pi]
                    c_decs += [(np.pi/2. - c_dec)*180./np.pi]
                    fluxes += [stemp*1.]

                    map[:] += Gaussian2D(temp, fwhm, ra, c_ra,  dec, c_dec)

    return map, np.array(c_ras), np.array(c_decs), np.array(fluxes)

################################################################################
################################################################################

### Clustering distribution

def cl_cluster_fixed_freq(ell, t_ps, source):

    """Clustering distribution of the sources (mK)^2 at a fixed frequency (1.4 GHz).
       Inputs: ell: multipole
               t_ps: background temperature (mK)
               source: model to use for the point sources - models implemented: battye
       Output: clustering contribution to the angular power spectrum of point sources at a fixed frequency (1.4 GHz) - (mK^2) 
    """

    if ell == 0:
        ell = 1.0e-3

    if source == "battye":        
        w_l = 1.8e-4 * ell**(-1.2)
        
    cl_cluster = w_l * t_ps**2

    return cl_cluster


# End of Lucas' code
#################################

def PSModel1(chanEdges, fwhm, nside, flux_max_poisson, flux_max_general, poisson=True, cluster=True):
    """
    """

    # Define some constants
    nside    = int(nside)
    lmax     = 3 * nside - 1
    chanMids = (chanEdges[1:] + chanEdges[:-1])/2. * 1e-9
    zeroFreq = 1.4 # GHz, frequency of the sources.
    alpha    = -2.7  # Spectral index of the sources
    source = "battye"
    
    # Loop through frequency slices, generating point sources.
    maps = np.zeros((chanMids.size, 12*nside**2))
    cl = np.zeros(lmax + 1)

    #
    if poisson:
        map_p, ras, decs, fluxes = map_poisson_fixed_freq_high_flow(flux_max_poisson, flux_max_general, nside, source, fwhm) 
    else:
        map_p = np.zeros(12*nside**2)

    for i in range(chanMids.size):
        cl[:] = 0.
        # For the poisson distributed point sources:
        if poisson:
            cl[:] += cl_poisson_fixed_freq(flux_max_poisson, source) * (chanMids[i]/zeroFreq)**alpha            

        # For the cluster distributed point sources: 
        if cluster:
            t_ps = temp_background_fixed_freq(flux_max_general, source) * (chanMids[i]/zeroFreq)**alpha
            for l in range(lmax+1):
                cl[l] += cl_cluster_fixed_freq(l, t_ps, source)

        
        maps[i,:] = hp.smoothing((hp.synfast(cl, nside) + t_ps)*1e-3, fwhm=fwhm) + map_p * (chanMids[i]/zeroFreq)**alpha# Output in K


    return maps, (ras, decs, fluxes)

PSModels = {'Model1': PSModel1}

def GeneratePointSources(model, sim_dir, fMax, fMin, fChannels, fwhm, nside, flux_max_poisson, flux_max_general, poisson_dist, cluster_dist, write=True):
    """
    Generate the free-free sky model.

    Model - fMax - fMin - fChannels - CoordSystem
    """

    chanEdges = np.linspace(fMin, fMax, fChannels + 1)

    maps, ps = PSModels[model](chanEdges, fwhm, nside, flux_max_poisson, flux_max_general, poisson=poisson_dist, cluster=cluster_dist)

    if write:
        hp.write_map('{}/inputs/PointSources/PointSources_{}.fits'.format(sim_dir, model), maps)
        f = h5py.File('{}/inputs/PointSources/SourceLocations_{}.hdf5'.format(sim_dir, model))
        dset1 = f.create_dataset('RA', ps[0].shape, dtype=ps[0].dtype)
        dset1[...] = ps[0][...]
        dset2 = f.create_dataset('DEC', ps[1].shape, dtype=ps[1].dtype)
        dset2[...] = ps[1][...]
        dset2 = f.create_dataset('FLUX', ps[2].shape, dtype=ps[2].dtype)
        dset2[...] = ps[2][...]
        f.close()
    else:
        return maps
