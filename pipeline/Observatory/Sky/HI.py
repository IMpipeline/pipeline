import numpy as np
from matplotlib import pyplot
import healpy as hp
import h5py

import glob
import os
import subprocess

from pipeline.Tools import FileTools

from pipeline.Observatory.Sky import HIModel1, Sky


HIModels = {'Model1': HIModel1.hi_ps_maps}

def GenerateHI(simInfo, model, datadir, datafiles, fMax, fMin, fChannels, fwhm, coordSystem, nside, write=True, writeFile='default.fits'):
    
    """
    Generate the free-free sky model.

    Model - fMax - fMin - fChannels - CoordSystem
    """

    chanEdges = np.linspace(fMin, fMax, fChannels + 1)
    chanMids  = (chanEdges[1:] + chanEdges[:-1])/2.
 
    widthfreq = (fMax - fMin)/fChannels
    if not isinstance(datafiles, list):
        datafiles = [datafiles]
    filenames = ['{}/{}'.format(datadir, filename) for filename in datafiles]
    
    powerspectrum = np.loadtxt(filenames[0] )

    l, cl = HIModels[model](fMin*1e-6, widthfreq*1e-6, int(fChannels), int(nside), powerspectrum) 

    #d= FileTools.ReadH5Py('HI_Cl_950-1410_20MHz.fits')
    #l = d['l']
    #cl = d['cl']
    FileTools.WriteH5Py('{}/HI_Cl_Model_{:.0f}-{:.0f}MHz_{:d}Channels_.hdf5'.format(writeFile.split('/')[1],
                                                                              fMin*1e-6,
                                                                              fMax*1e-6,
                                                                              fChannels), 
                        {'l': np.array(l), 'cl': np.array(cl)})

    
    maps = np.zeros((int(fChannels), int(12*nside**2) ))
    for i in range(fChannels):
        maps[i,:] = hp.sphtfunc.synfast(cl[i,:], int(nside), verbose=False)
    

    maps = Sky.SmoothMaps(maps, simInfo, chanMids, chanEdges)


    if write:
        hp.write_map(writeFile, maps)
    else:
        return maps
