#==================================================================
#This programme is to get the AME map at a certain frequency from 
#a given dust map, e.g., Planck tau_353 optical depth map

#This programme is adapted from spdscale.py to make it as a function
# in order to fit for the pipeline

#T.CHEN 14052017
#==================================================================

#----------------Import relevant libraries-----------------

import numpy as np
import healpy as hp
import matplotlib.pyplot as plt
from astropy.io import fits as  pyfits
from scipy import interpolate

from pipeline.Observatory.Sky import Sky
from scipy.ndimage.filters import gaussian_filter

from pipeline.Tools import FileTools

def GenerateAME(simInfo, writeFile, write=True):  
    """
    Generate the AME sky model.
    Based on PLANCK353 or IRIS100 map.
    
    The default spdust_file is spdust2_cnm.dat from SPDUST code assuming cold neutrial matter environment

    """
#-------Set input parameters--------------
    model = simInfo.Parameters['AME']['model'].upper()
    indir = simInfo.Parameters['AME']['ancil_dir']
    infile = '{}/{}'.format(indir, simInfo.Parameters['AME']['ancil_files'])
    spdust_file = '{}/{}'.format(indir, simInfo.Parameters['AME']['spdust_model'])
    mode = simInfo.Parameters['Mapping']['mode'].upper()
    coord = simInfo.Parameters['Mapping']['coordinate_system'].upper()

    chanEdges = np.linspace(simInfo.minFreq, simInfo.maxFreq, simInfo.nchannels + 1, endpoint=True)
    chanMids  = (chanEdges[1:] + chanEdges[:-1])/2.


    if model == 'PLANCK353':
        ame_ratio = 8.7 #K
        freq_in = 22.8 #GHz
    elif model == 'IRIS100':
        ame_ratio = 11.9e-6 #K
        freq_in = 33#GHz
    else:
        print('# INVALID AME MODEL INPUT {}, PLEASE CHOOSE PLANCK353 OR IRIS100'.format(simInfo.Parameters['AME']['model'].upper()))
        raise ValueError
    
#-----------------Get Scale at different frequencies------------------
    freq, flux = np.loadtxt(spdust_file, comments = ';', usecols = (0, 1), unpack = True) #Read in spdust file 
    intp = interpolate.interp1d(freq*1e9, flux, fill_value=0, bounds_error=False) #get the interpolation function of freq and flux from the spdust file 
    flux_in = intp(freq_in*1e9) #get the flux in the spdust file at the input ame map freq   
    flux_out = intp(chanMids) #get the flux in the spdust file at the wanted ame map freq
    scale = flux_out/flux_in #scale factor to go to ame map at wanted freq

#-----------------Scale maps at each freq channel----------------------------
    nside = int(simInfo.Parameters['Mapping']['hp_nside'])

    if mode == 'HEALPIX':
        inmap =  Sky.SetupAncilMaps(infile, nside, coord)   

        spdmap = inmap[0]*ame_ratio #convert to the ame temperature map in K
        maps = np.zeros((scale.size, spdmap.size))

        for i in range(scale.size):
            maps[i, :] = spdmap*scale[i] #get AME map at each freq channel
    elif simInfo.Parameters['Mapping']['mode'].upper() == 'CARTESIAN':    
        naxis = [simInfo.Parameters['Mapping']['ct_naxis1'], simInfo.Parameters['Mapping']['ct_naxis2']]
        cdelt = [simInfo.Parameters['Mapping']['ct_cdelt1'], simInfo.Parameters['Mapping']['ct_cdelt2']]
        crval = [simInfo.Parameters['Mapping']['ct_crval1'], simInfo.Parameters['Mapping']['ct_crval2']]

        inmap = Sky.SetupAncilMaps(infile, nside, coord, mode = mode, naxis=naxis, cdelt= cdelt, crval=crval)   
        spdmap = inmap[0]*ame_ratio #convert to the ame temperature map in K
        maps = np.zeros((scale.size, int(naxis[0])*int(naxis[0])) )
        b = np.reshape(spdmap, (int(naxis[0]), int(naxis[0])))
        for i in range(scale.size):
            # Smooth the maps
            pass # No cartesian model?
    else:
        print('# INVALID MAPPING MODE INPUT {}, PLEASE CHOOSE CARTESIAN OR HEALPIX'.format(simInfo.Parameters['Mapping']['mode'].upper()))
        raise ValueError

    maps = Sky.SmoothMaps(maps, simInfo, chanMids, chanEdges)

    if write:
        hp.write_map(writeFile, maps)
    else:
        return maps


