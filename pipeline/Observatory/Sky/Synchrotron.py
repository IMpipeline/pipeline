
import numpy as np
from matplotlib import pyplot
import healpy as hp

import glob
import os
import subprocess
from pipeline.Tools.HPTools import HealpixRotate
from pipeline.Observatory.Telescope import Coordinates
from scipy.interpolate import griddata

import scipy.interpolate as spint
import scipy.spatial.qhull as qhull
import itertools    

from pipeline.Observatory.Sky import Sky
from scipy.ndimage.filters import gaussian_filter

from pipeline.Tools import FileTools

def ComapM31(AncilMaps, chanEdges):
    
    m = AncilMaps[0] # modified 545 GHz map
    chanMids = (chanEdges[:-1] + chanEdges[1:])/2.
    nu_28 = 28.4e9
    return m[np.newaxis,:]* (chanMids[:,np.newaxis]/nu_28)**-2.1


def SyncModel1(AncilMaps, chanEdges):
    """

    Model one generates a synchrotron model assuming a constant
    spectral index for every pixel.

    Arguements: 
    datadir     - Directory for the required input maps
    chanEdges   - The edges of each frequency channel
    coordSystem - Ouput sky coordinate system

    The default coordainte system of these maps are Galactic.

    """
    #Now create the frequency maps
    chanMids = (chanEdges[:-1] + chanEdges[1:])/2.
    haslam_nu = 408e6 #Hz

    haslam = AncilMaps[0] - 2.73 # remove CMB temperature from the map background
    platania = AncilMaps[1]

    haslam   = np.reshape(haslam  , (1, haslam.size))
    platania = np.reshape(platania, (1, platania.size))
    chanMids = np.reshape(chanMids, (1, chanMids.size))
    
    maps = ((chanMids.T/haslam_nu)**platania) * haslam
    
    return maps
    
def SinglePowerLaw(filenames, chanEdges, fwhm, coordSystem, nside, index, curvature):
    """

    Model one generates a synchrotron model assuming a constant
    spectral index for every pixel.

    Arguements: 
    datadir     - Directory for the required input maps
    chanEdges   - The edges of each frequency channel
    coordSystem - Ouput sky coordinate system
    index       - Main power law index at 1GHz
    curvature   - Curvature index 

    The default coordainte system of these maps are Galactic.

    """

    #use haslam dsds map to get amplitude
    haslam = hp.read_map('{}'.format(filenames[0]))
    haslam_c = 'G'
    haslam_nu = 408e6 #Hz
    haslam_res = 56./60.*np.pi/180.
    haslam[np.isnan(haslam) | np.isinf(haslam)] = hp.UNSEEN

    # 

    
    haslam = hp.ud_grade(haslam, nside)
    if coordSystem == 'Celestial':
        haslam   = HealpixRotate(haslam, coords=['C', 'G'])

    #Now create the frequency maps
    chanMids = (chanEdges[:-1] + chanEdges[1:])/2.
    
    haslam  = np.reshape(haslam  , (1, haslam.size))
    chanMids= np.reshape(chanMids, (1, chanMids.size))
    
    maps = haslam.T.dot( (chanMids/haslam_nu)**(index + curvature*np.log(chanMids/haslam_nu)) )
    
    
    return maps.T
    


SyncModels = {'Model1': SyncModel1, 'SinglePowerLaw': SinglePowerLaw,'COMAP_M31': ComapM31}

def GenerateSynchrotron(simInfo, write=True, writeFile='default.fits'):

    """
    Generate the synchrotron sky model.

    Model - fMax - fMin - fChannels - CoordSystem
    """
    model     = simInfo.Parameters['Synchrotron']['model']
    datadir   = simInfo.Parameters['Synchrotron']['ancil_dir']
    datafiles = simInfo.Parameters['Synchrotron']['ancil_files']
    coordSystem = simInfo.Parameters['Mapping']['coordinate_system']
    chanEdges = np.linspace(simInfo.minFreq, simInfo.maxFreq, simInfo.nchannels + 1, endpoint=True)
    chanMids  = (chanEdges[1:] + chanEdges[:-1])/2.


    if not isinstance(datafiles, list):
        datafiles = [datafiles]
    filenames = ['{}/{}'.format(datadir, filename) for filename in datafiles]


    
    nside = int(simInfo.Parameters['Mapping']['hp_nside'])

    if simInfo.Parameters['Mapping']['mode'].upper() == 'HEALPIX':
        ancilMaps = Sky.SetupAncilMaps(filenames, nside, coordSystem)
    elif simInfo.Parameters['Mapping']['mode'].upper() == 'CARTESIAN':
        naxis = [simInfo.Parameters['Mapping']['ct_naxis1'], simInfo.Parameters['Mapping']['ct_naxis2']]
        cdelt = [simInfo.Parameters['Mapping']['ct_cdelt1'], simInfo.Parameters['Mapping']['ct_cdelt2']]
        crval = [simInfo.Parameters['Mapping']['ct_crval1'], simInfo.Parameters['Mapping']['ct_crval2']]

        ancilMaps = Sky.SetupAncilMaps(filenames, nside, simInfo.Parameters['Mapping']['coordinate_system'],
                                       mode = simInfo.Parameters['Mapping']['mode'].upper(), naxis=naxis, cdelt= cdelt, crval=crval)
    else:
        print('# INVALID MAPPING MODE INPUT {}, PLEASE CHOOSE CARTESIAN OR HEALPIX'.format(simInfo.Parameters['Mapping']['mode'].upper()))
        raise ValueError

    maps = SyncModels[model](ancilMaps, chanEdges)

    maps = Sky.SmoothMaps(maps, simInfo, chanMids, chanEdges)
    if write:
        if simInfo.Parameters['Mapping']['mode'].upper() == 'HEALPIX':
            hp.write_map(writeFile, maps)
        else:
            FileTools.WriteH5Py(writeFile, {'maps':maps})
    else:
        return maps


