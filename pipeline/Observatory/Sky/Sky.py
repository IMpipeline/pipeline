"""
"""

import numpy as np
from matplotlib import pyplot
import healpy as hp

import glob
import os
import subprocess

from pipeline.Tools.HPTools import HealpixRotate

from pipeline.Tools import CartPix
from scipy.interpolate import griddata
from scipy.ndimage.filters import gaussian_filter

def SetupAncilMaps(filenames, nside, coordSystem, mode='HEALPIX', naxis=[], cdelt=[], crval=[]):
    # Convert maps to correct coordinate system and pixel system
    ancilMaps = []

    if isinstance(filenames, str):
        filenames = [filenames]

    for f in filenames:
        # Get the map and remove any bad data
        m = hp.read_map('{}'.format(f))
        m[np.isnan(m) | np.isinf(m)] = hp.UNSEEN
        m = hp.ud_grade(m, nside)

        # Rotate to correct coord system
        #if coordSystem.upper() == 'CELESTIAL':
        #    m = HealpixRotate(m, coords=['C', 'G'])

        # NOW CONVERT TO CARTESIAN IF REQUIRED
        if mode.upper() == 'CARTESIAN':
            xspan = naxis[0] * cdelt[0]
            yspan = naxis[1] * cdelt[1]

            xpix, ypix = np.meshgrid(np.arange(naxis[0]), np.arange(naxis[1]))
            xpix = xpix.flatten()
            ypix = ypix.flatten()
            ctheta, cphi = CartPix.pix2ang(naxis, cdelt, crval, xpix, ypix)

            # Perform the interpolation
            m = hp.get_interp_val(m,cphi, ctheta, lonlat=True)


        # Add map to list of ancilMaps
        ancilMaps += [m]

    return ancilMaps


def SmoothMaps(maps, simInfo, chanMids, chanEdges):
    '''
    Smooths maps with correct beam model. Checks for Healpix or Cartesian gridding.
    '''

    fwhm = simInfo.Parameters['Beam']['fwhm']
    if simInfo.Parameters['Mapping']['mode'].upper() == 'HEALPIX':

        nside = int(simInfo.Parameters['Mapping']['hp_nside'])

        # Smooth the maps
        for i in range(maps.shape[0]):
            alm = hp.map2alm(maps[i,:])
            alm = hp.almxfl(alm, simInfo.Beam.Bl[i,:])
            maps[i,:] = hp.alm2map(alm, nside) * simInfo.Beam.mainbeamratio
            #maps[i,:] = hp.smoothing(maps[i,:], fwhm=fwhm*(chanMids[-1]/chanMids[i]*np.pi/180.))
   
    if simInfo.Parameters['Mapping']['mode'].upper() == 'CARTESIAN':
        naxis = [simInfo.Parameters['Mapping']['ct_naxis1'], simInfo.Parameters['Mapping']['ct_naxis2']]
        cdelt = [simInfo.Parameters['Mapping']['ct_cdelt1'], simInfo.Parameters['Mapping']['ct_cdelt2']]
        crval = [simInfo.Parameters['Mapping']['ct_crval1'], simInfo.Parameters['Mapping']['ct_crval2']]
        
        # Smooth the maps
        b = np.transpose(np.reshape(maps, (maps.shape[0], int(naxis[1]), int(naxis[0]))), (0, 2,1))

        for i in range(maps.shape[0]):
            # Smoothing distance in pixel coordinates
            sigma = [fwhm*chanMids[-1]/chanMids[i]/cdelt[0]/2.355,
                     fwhm*chanMids[-1]/chanMids[i]/cdelt[1]/2.355]
            # Smooting reflects at edges.
            b[i,:,:] =  gaussian_filter(b[i,:,:], sigma )
            maps[i,...] = b[i,...].T.flatten()

    return maps
