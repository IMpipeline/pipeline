import numpy as np
from matplotlib import pyplot
import scipy.fftpack as fft

class Atmosphere:
    
    def __init__(self, meanTau, sampleRate, chanWidth, nchannels, maxBlockSize):
        
        self.meanTau = meanTau
        self.sampleRate = sampleRate
        self.chanWidth = chanWidth
        self.nchannels = nchannels
        self.maxBlockSize = maxBlockSize
        self.PowerSpectrum(maxBlockSize)
        
    def PowerSpectrum(self, dataSize):
        time = dataSize/self.sampleRate
        knee = 1./(3600.) # every how it might move by 10%
        daycut = 1./(3600.*24.) # and has zero fluctuations beyond 1 day
        
        freqs = fft.fftfreq(dataSize, d=1./self.sampleRate)
        freqs[0] = freqs[1]
        P = (knee/freqs)**2 / freqs[1]
        P[0] = 0
        P[np.abs(freqs) < daycut] = 0
        
        self.Pk = np.sqrt(P)
        #return np.sqrt(P)
        
    def GenerateAtmosphere(self,elevation):
        
        self.PowerSpectrum(elevation.size)
        # assume a flat frequency spectrum for now
        tau = self.meanTau/np.sin(elevation*np.pi/180.)
        tau[np.isfinite(tau)==False] = np.nanmin(tau)
        Tatm = 270. # K
        
        # Add a 5% scatter
        tauVar = 0.05
        
        # Power spectrum
        
        taunoise = np.random.normal(scale=tau*tauVar)
        taunoise = np.real(fft.ifft(fft.fft(taunoise)*self.Pk)) + tau
        
        Asky = np.exp(-taunoise)
        Tsky = Tatm * (1. - Asky)
        
        return Tsky, Asky
        
if __name__ == "__main__":
    
    meanTau = 0.075
    sampleRate, chanWidth, nchannels, maxBlockSize = 20., 1e9, 8, 2**16
    atmos = Atmosphere(meanTau, sampleRate, chanWidth, nchannels, maxBlockSize)
    
    elevation = np.arange(maxBlockSize)/maxBlockSize * 30 + np.ones(maxBlockSize)*50.
    T, A  = atmos.GenerateAtmosphere(elevation)
    
    time = np.arange(maxBlockSize)/sampleRate /3600.
    pyplot.plot(time,T)
    pyplot.plot(time,A)
    pyplot.show()
        
    pyplot.plot(elevation,T)
    pyplot.show()
        
    
        