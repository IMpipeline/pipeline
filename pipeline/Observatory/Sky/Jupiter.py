import numpy as np
import healpy as hp
from pipeline.Tools import CartPix, FileTools

# This script describes a very simple model for Jupiter.
# The model assumes that Jupiter is fixed at 15 1 23.3876, -16 15 44.5474 ( its position August 2018),
# It is at a fixed distance of 5.7 AU
#
def RotatePhi(skyVec, objRa):
    outVec = skyVec*0.
    # Rotate first RA
    outVec[:,0] =  skyVec[:,0]*np.cos(objRa*np.pi/180.) + skyVec[:,1]*np.sin(objRa*np.pi/180.) 
    outVec[:,1] = -skyVec[:,0]*np.sin(objRa*np.pi/180.) + skyVec[:,1]*np.cos(objRa*np.pi/180.) 
    outVec[:,2] =  skyVec[:,2]
    return outVec

def RotateTheta(skyVec, objDec):
    outVec = skyVec*0.
    # Rotate first Dec
    outVec[:,0] =  skyVec[:,0]*np.cos(objDec*np.pi/180.) + skyVec[:,2]*np.sin(objDec*np.pi/180.) 
    outVec[:,1] =  skyVec[:,1]
    outVec[:,2] = -skyVec[:,0]*np.sin(objDec*np.pi/180.) + skyVec[:,2]*np.cos(objDec*np.pi/180.) 
    return outVec


def RotateR(skyVec, objPang):
    outVec = skyVec*0.
    # Rotate first pang
    outVec[:,0] =  skyVec[:,0]
    outVec[:,1] =  skyVec[:,1]*np.cos(objPang*np.pi/180.) + skyVec[:,2]*np.sin(objPang*np.pi/180.) 
    outVec[:,2] = -skyVec[:,1]*np.sin(objPang*np.pi/180.) + skyVec[:,2]*np.cos(objPang*np.pi/180.) 
    return outVec

def Rotate(ra, dec, r0, d0, p0):
    """
    Rotate coordinates to be relative to some ra/dec and sky rotation pang
    
    All inputs in degrees

    """
    skyVec = hp.ang2vec((90.-dec)*np.pi/180., ra*np.pi/180.)

    outVec = RotatePhi(skyVec, r0)
    outVec = RotateTheta(outVec, d0)
    outVec = RotateR(outVec, p0)

    _dec, _ra = hp.vec2ang(outVec)
    _dec = (np.pi/2. - _dec)*180./np.pi
    _ra = _ra * 180./np.pi

    _ra[_ra > 180] -= 360.
    return _ra, _dec


def Gauss2D(npix, chanMids, fwhm, r):
    
    m = np.zeros((chanMids.size, npix))
    for i in range(chanMids.size):
        sigma = fwhm*chanMids[-1]/chanMids[i] / 2.355 #/cdelt[0]/2.355
        
        m[i,:] = np.exp(-0.5*r**2/sigma**2)/1.13/(sigma*np.pi/180.*2.355)**2
    return m
    
def Fixed(simInfo, chanMids):
    
    fwhm = simInfo.Parameters['Beam']['fwhm']
    jupra, jupdec = (15. + 1./60. + 23.3876/60.**2)*15., -(16. + 15./60. + 44.5474/60.**2)
    # Needs to know the mapping infomation
    if simInfo.Parameters['Mapping']['mode'].upper() == 'HEALPIX':
        nside = int(simInfo.Parameters['Mapping']['hp_nside'])
        theta, phi = hp.pix2ang(nside, np.arange(12*nside**2, dtype=int))
        phi *= 180./np.pi
        theta = (np.pi/2. - theta)*180./np.pi
        
        phi, theta = Rotate(phi, theta, jupra, jupdec, 0)
        
        r = np.sqrt(phi**2 + theta**2)
        m = Gauss2D(12*nside**2, chanMids, fwhm, r)
        
        
    elif simInfo.Parameters['Mapping']['mode'].upper() == 'CARTESIAN':
        naxis = [simInfo.Parameters['Mapping']['ct_naxis1'], simInfo.Parameters['Mapping']['ct_naxis2']]
        cdelt = [simInfo.Parameters['Mapping']['ct_cdelt1'], simInfo.Parameters['Mapping']['ct_cdelt2']]
        crval = [simInfo.Parameters['Mapping']['ct_crval1'], simInfo.Parameters['Mapping']['ct_crval2']]
        
        ypix, xpix= np.meshgrid(np.arange(naxis[0]), np.arange(naxis[1]))
        theta, phi = CartPix.pix2ang(naxis, cdelt, crval,  xpix.flatten(), ypix.flatten())  
        phi, theta = Rotate(phi, theta, jupra, jupdec, 0)
        
        npix = int(naxis[0]*naxis[1])
        r = np.sqrt(phi**2 + theta**2)
        m = Gauss2D(npix, chanMids, fwhm, r)
        
    else:
        print('# INVALID MAPPING MODE INPUT {}, PLEASE CHOOSE CARTESIAN OR HEALPIX'.format(simInfo.Parameters['Mapping']['mode'].upper()))
        raise ValueError

    Tj = 10**(0.149*np.log10(chanMids*1e-9/22.8) + 2.15) # Brightness of Jupiter
    Tj *= 2.481e-8 * (5.2/5.708)**2
        
    return m*Tj[:, np.newaxis]

JupModels = {'Fixed': Fixed}

def GenerateJupiter(simInfo, write=True, writeFile='default.fits'):

    """
    Generate the synchrotron sky model.

    Model - fMax - fMin - fChannels - CoordSystem
    """
    model     = simInfo.Parameters['Jupiter']['model']
    coordSystem = simInfo.Parameters['Mapping']['coordinate_system']
    chanEdges = np.linspace(simInfo.minFreq, simInfo.maxFreq, simInfo.nchannels + 1, endpoint=True)
    chanMids  = (chanEdges[1:] + chanEdges[:-1])/2.



    

    if simInfo.Parameters['Mapping']['mode'].upper() == 'HEALPIX':
        nside = int(simInfo.Parameters['Mapping']['hp_nside'])
    elif simInfo.Parameters['Mapping']['mode'].upper() == 'CARTESIAN':
        naxis = [simInfo.Parameters['Mapping']['ct_naxis1'], simInfo.Parameters['Mapping']['ct_naxis2']]
        cdelt = [simInfo.Parameters['Mapping']['ct_cdelt1'], simInfo.Parameters['Mapping']['ct_cdelt2']]
        crval = [simInfo.Parameters['Mapping']['ct_crval1'], simInfo.Parameters['Mapping']['ct_crval2']]
    else:
        print('# INVALID MAPPING MODE INPUT {}, PLEASE CHOOSE CARTESIAN OR HEALPIX'.format(simInfo.Parameters['Mapping']['mode'].upper()))
        raise ValueError

    maps = JupModels[model](simInfo, chanMids)

    # maps = Sky.SmoothMaps(maps, simInfo, chanMids, chanEdges)
    if write:
        if simInfo.Parameters['Mapping']['mode'].upper() == 'HEALPIX':
            hp.write_map(writeFile, maps)
        else:
            FileTools.WriteH5Py(writeFile, {'maps':maps})
    else:
        return maps
