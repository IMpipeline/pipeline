import numpy as np
from matplotlib import pyplot
import healpy as hp
import h5py

import glob
import os
import subprocess

from pipeline.Observatory.Sky import COModel1


COModels = {'Model1': COModel1.co_ps_maps}

def GenerateCO(model, sim_dir, datadir, fMax, fMin, fChannels, fwhm, nside, write=True):
    """
    Generate the free-free sky model.

    Model - fMax - fMin - fChannels - CoordSystem
    """

    
    widthfreq = (fMax - fMin)/fChannels
    metadir = '{}/CO/'.format(datadir)
    maps, l, cl = COModels[model](fMin*1e-9, widthfreq*1e-9, int(fChannels), int(nside), fwhm, metadir=metadir) 

    if write:
        hp.write_map('{}/inputs/CO/CO_{}.fits'.format(sim_dir, model), maps)
    else:
        return maps
