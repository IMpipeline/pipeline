import numpy as np
from astropy.io import fits as pyfits
import healpy as hp
from matplotlib import pyplot
import subprocess

# Import integration routines
from scipy import integrate as si
# Import special functions
from scipy import special as ss
# Import curve fitting
from scipy.optimize import curve_fit

#import pdb

################################################################################
################################################################################

##### COSMOLOGY

### Universe components - current time - flat Universe 

omega_b = 0.0486
omega_c = 0.2589
omega_d = 0.6925

omega_m = omega_b + omega_c

omega_k = 1.0 - omega_d - omega_m

### Dark energy parameters

w_0 = -1.0
w_a =  0.0

### Hubble parameters - km/s/Mpc

h = 0.6774
H_0 = 100.0 * h

### HI parameter - constant

omega_hi = 6.2 * 10**(-4)
omega_hi_h = omega_hi * h

bias = 1.0

### CAMB parameters

# ns = 0.9667
# As = 2.1e-9
# tau = 0.066

################################################################################
################################################################################

##### Redshift Functions

### Hubble parameter: E(z) (dimensionless)

def hubble(z): 
    """
    Returns Hubble Parameter E(z) [dimensionless]

    Arguments:
    z - redshift

    """
    
    E = (omega_m * (1.0 + z)**3 + omega_k * (1.0 + z)**2 + omega_d * (1.0 + z)**(3.0 * (1.0 + w_0 + w_a)) * np.exp(-3.0 * w_a * z / (1 + z)))**0.5

    return E

### 21 cm background temperature - mK

def temp_hi(z):
    
    T_hi = 44.0 * 10**(-3) * (omega_hi_h / (2.45 * 10**(-4))) * ((1.0 + z)**2 / hubble(z))

    return T_hi

### Comoving distance = dimensionless

def comoving_dist(z):
    
    fz = lambda x: 1.0 / (omega_m * (1.0 + x)**3 + omega_k * (1.0 + x)**2 + omega_d * (1.0 + x)**(3 * (1.0 + w_0 + w_a)) * np.exp(-3 * w_a * x / (1 + x)))**0.5

#    comoving_dist = (c / H_0) * si.romberg(fz, 0, z)

    comoving_dist = si.romberg(fz, 0, z)

    return comoving_dist

### Growth factor for a spatially flat cosmology with dust and a cosmological constant (w const or w = w_0 + (z / 1 + z) * w_a)

def deriv(D, z):

# Derivative of the growth factor needed to solve the differential equation for the growth factor

#    x_z = lambda z: ((omega_m / (1.0 - omega_m)) * ((1.0 + z)**(-3.0 * (w_0 + w_a)) * np.exp(3.0 * w_a * (z / (1.0 + z)))))
#    omega_z = lambda z: (w_0 + (z / (1.0 + z)) * w_a)

    derivative = np.array([D[1], 1.5 * (( (omega_m / (1.0 - omega_m)) * ((1.0 + z)**(-3.0 * (w_0 + w_a)) * np.exp(3.0 * w_a * (z / (1.0 + z)))))/ (1.0 + ((omega_m / (1.0 - omega_m)) * ((1.0 + z)**(-3.0 * (w_0 + w_a)) * np.exp(3.0 * w_a * (z / (1.0 + z))))))) * (1.0 / (1.0 + z)**2) * D[0] - 0.5 * (1.0 / (1.0 + z)) * (1.0 + 3.0 * ((w_0 + (z / (1.0 + z)) * w_a) / (1 + ((omega_m / (1.0 - omega_m)) * ((1.0 + z)**(-3.0 * (w_0 + w_a)) * np.exp(3.0 * w_a * (z / (1.0 + z)))))))) * D[1]])

    return derivative

def growth_factor(z_input):

    if z_input <= 1.0:
        N = 10
    else:
        N = 20

    if z_input != 0.0:
        redshift = np.linspace(0.0, z_input, N)
        growth_initial = np.array([0.76037996, -0.37177863])   # Correct for LambdaCDM
        growth_factor = si.odeint(deriv, growth_initial, redshift)

        growth_factor_0 = growth_factor[0, 0]
        growth_factor_z = growth_factor[N - 1, 0]

        growth_factor_norm = growth_factor_z / growth_factor_0

    else:
        growth_factor_norm = 1.0

    return growth_factor_norm

################################################################################
################################################################################

##### Power Spectrum

### Power spectrum (calculated with CAMB) - Output in h/Mpc

def func_1(x, a, b, c):

    # interpolation function: quadratic

    return a + b*x + c*x**2

def power_spectrum(k_input, k_calc, pk_calc):

    index = np.where(k_calc <= k_input)
    if np.array(index).size >= 1:
        index = (index[-1])[-1]
    else:
        index = 0
    
    nk = k_calc.size

    if index >= nk - 1:
        p_out = (k_input/k_calc[nk - 1])**(-3) * pk_calc[nk - 1]
    elif index == 0:
        p_out = (k_input/k_calc[0]) * pk_calc[0]
    else:
        k_data = np.zeros(3)
        pk_data = np.zeros(3)
        k_data[:] = k_calc[index-1:index+2]
        pk_data[:] = pk_calc[index-1:index+2]
        par, pcov = curve_fit(func_1, k_data, pk_data)
        p_out = par[0] + par[1]*k_input + par[2]*k_input**2

    return p_out

################################################################################
################################################################################

##### Angular Power Spectrum

### Cl using Limber approximation        
    
def cl_limber(l, z_min, z_max, h_input, cd_input, t_input, gf_input, k_calc, pk_calc):

    if l == 0:
        l = 1.0e-3
    
    windows = 1.0 / (z_max - z_min)   # Windows function (should not be hard coded, but for now we are only using a uniform window in redshift)

    N = h_input.size # samples - odd
    z = np.linspace(z_min, z_max, N, endpoint=True)

    integrand_array = np.zeros(N)

    for i in range (0, N):
#        integrand_array[i] = h_input[i] * t_input[i]**2 * gf_input[i]**2 * (1.0 / (cd_input[i])**2) * power_spectrum((l + 0.5) / cd_input[i], k_calc, pk_calc)
        integrand_array[i] = h_input[i] * t_input[i]**2 * gf_input[i]**2 * (1.0 / (cd_input[i])**2) * power_spectrum(((l + 0.5) / cd_input[i]) / 3000.0, k_calc, pk_calc)

#    cl_limber = ((H_0 * bias**2) / c) * windows**2 * si.simps(integrand_array, z)
    cl_limber = (bias**2 * windows**2 * si.simps(integrand_array, z)) / 3000.**3

    return cl_limber

################################################################################
################################################################################

##### Main function: calculates the HI map for each frequency channel (no correlation for now) - mK

###
# Inputs: freq_min -- minimum frequency in MHz 
#         freq_width -- width of the frequency channels in MHz
#         nchannels -- number of channels
#         nside (lmax = 3 * nside - 1) -- nside of the Healpix maps
#         suffix -- string for the file suffix -- string with the suffix of the output fits file
#
# Output: fits file named 'map_hi_' + suffix + '.fits' with an array a[npixel, nchannels], where npixel is the number of pixels of the maps. The maps are given in mK.
###

# The Cosmology should be an input!
# CAMB should be called.

def hi_ps_maps(freq_min, freq_width, nchannels, nside, powerspec, suffix=None):
    """
    Return set of HI maps equal to nchannels, with 12*nside**2 pixels.

    Arguments
    freq_min   - Minimum frequency (MHz)
    freq_width - Width of each bandpass (MHz)
    nchannels  - Number of bandpasses in bandwidth
    nside      - Resolution parameter for output HEALPix maps
    powerspec  - Array containing k-values and P_k values of powerspectrum. Column 1 is k, column 2 is P_k.
    """

    k_calc = powerspec[:,0]
    pk_calc = powerspec[:,1]


    lineFrequency = 1420.4 # (MHz)

    # Defining the experiment

    freq_half = freq_width / 2.0
    channels = np.zeros(nchannels) 
    ind = np.arange(nchannels)
    channels = freq_min + ind * freq_width

    lmax = 3 * nside - 1
    ell = np.arange(lmax + 1)
    c_ell = np.zeros((nchannels, lmax + 1))

    nk = k_calc.size

    # Hubble parameter, comoving distance, HI temp, growth factor   

    samples = 11 # samples - odd 

    h_input  = np.zeros(samples)
    cd_input = np.zeros(samples)
    t_input  = np.zeros(samples)
    gf_input = np.zeros(samples)

    maps = np.zeros((12*nside**2, nchannels))

    for i in range (0, nchannels):
        print('CREATING CHANNEL {}'.format(i))


        z_min = (lineFrequency / (channels[i] + freq_half)) - 1.0
        z_max = (lineFrequency / (channels[i] - freq_half)) - 1.0
        z = np.linspace(z_min, z_max, samples, endpoint=True)    

        
        for j in range (0, samples):
             h_input[j] = hubble(z[j])
             cd_input[j] = comoving_dist(z[j])
             t_input[j] = temp_hi(z[j])
             gf_input[j] = growth_factor(z[j]) 


        # Calculating the C_ell
       
        for l in range (0, lmax + 1):
            c_ell[i,l] = cl_limber(l, z_min, z_max, h_input, cd_input, t_input, gf_input, k_calc, pk_calc)

        #l_hi =np.arange(lmax+1)
        #map = hp.sphtfunc.synfast(c_ell[i,:]*hp.gauss_beam(fwhm=fwhm, lmax=(lmax)), nside, verbose=False)        
        #maps[:, i] = map/1000. #Units of K
    return np.arange(lmax+1), c_ell/1e6

   # if not isinstance(suffix, type(None)):
  #      file_map = 'map_hi_' + suffix + '.fits'
 #       pyfits.writeto(file_map, maps, clobber=True)
#    else:


        
if __name__ == "__main__":

    # Calculating the power spectrum - should really call CAMB to calculate the power spectrum for a particular Cosmology

    file_pk = 'HI_Powerspec.dat'

    pk_calc = np.loadtxt(file_pk)

    nside = 256
    maps = hi_ps_maps(960, 20, 1, nside, pk_calc)
