import numpy as np
from scipy import integrate as si
from scipy.optimize import curve_fit
import healpy as hp
from astropy.io import fits as pyfits

import matplotlib.pyplot as plt

################################################################################################################################################################

g_const = 4.302 * 1.e-9   # Mpc * M_o^{-1} * (km / s)^2
c = 3.0 * 10**5   # (km / s)

##### COSMOLOGY

### Universe components - current time - flat Universe 

omega_b = 0.0486
omega_c = 0.2589
omega_d = 0.6925

omega_m = omega_b + omega_c

omega_k = 1.0 - omega_d - omega_m

### Dark energy parameters

w_0 = -1.0

### Hubble parameters - km/s/Mpc

h = 0.6774
H_0 = 100.0 * h

### CO parameters

amplitude = 1.0
f_duty = 0.1
freq_line_co = 115. # GHz
m_min = 1.e9
m_max = 1.e15

### CAMB parameters

# ns = 0.9667
# As = 2.1e-9
# tau = 0.066

################################################################################################################################################################

### Power Spectrum

def func_1(x, a, b, c):

    # interpolation function: quadratic

    return a + b*x + c*x**2

def power_spectrum(k_input, k_calc, pk_calc):

    index = np.where(k_calc <= k_input)
    if np.array(index).size >= 1:
        index = (index[-1])[-1]
    else:
        index = 0
    
    nk = k_calc.size

    if index >= nk - 1:
        p_out = (k_input/k_calc[nk - 1])**(-3) * pk_calc[nk - 1]
    elif index == 0:
        p_out = (k_input/k_calc[0]) * pk_calc[0]
    else:
        k_data = np.zeros(3)
        pk_data = np.zeros(3)
        k_data[:] = k_calc[index-1:index+2]
        pk_data[:] = pk_calc[index-1:index+2]
        par, pcov = curve_fit(func_1, k_data, pk_data)
        p_out = par[0] + par[1]*k_input + par[2]*k_input**2

    return p_out

### Growth factor -- Flat DE + DM Universe

def growth_factor(z_input):

    if z_input != 0.0:
        x = ((1.0 / omega_m) - 1.0) / (1.0 + z_input)**3
        x_0 = ((1.0 / omega_m) - 1.0) / (1.0 + 0)**3

        num = 1.0 + 1.175 * x + 0.3064 * x**2 + 0.005355 * x**3
        den = 1.0 + 1.857 * x + 1.021 * x**2 + 0.1530 * x**3
        num_0 = 1.0 + 1.175 * x_0 + 0.3064 * x_0**2 + 0.005355 * x_0**3
        den_0 = 1.0 + 1.857 * x_0 + 1.021 * x_0**2 + 0.1530 * x_0**3
        
        D = (1.0 + x)**0.5 / (1.0 + z_input) * (num / den)
        D_0 = (1.0 + x_0)**0.5 / (1.0 + 0) * (num_0 / den_0)

        D_norm = D / D_0
    else:
        D_norm = 1.0

    return D_norm

### Sigma_m -- simple windows function (Top hat)

def sigma_m(mass, k_calc, pk_calc):

    rho_c = (3. * 1.e4 * h**2) / (8. * np.pi * g_const)   #  M_o * Mpc^{-3}
    rho_m = rho_c * omega_m
    k_c = ((6. * np.pi**2 * rho_m) / mass)**(1./3.)   # Mpc^{-1}

    k_ind = np.where(k_calc <= k_c)
    nk = (k_ind[0]).size
    k_input = np.zeros(nk)
    k_input[:] = k_calc[k_ind]
    
    ps = np.zeros(nk)

    for i in range(0, nk):
        ps[i] = power_spectrum(k_input[i], k_calc, pk_calc)

    integrand_array = np.zeros(nk)
    integrand_array = ps * k_input**2

    sigma_squared = (1. / (2. * np.pi**2)) * si.simps(integrand_array, k_input)

    return np.sqrt(sigma_squared)

### Halo mass number density

def dn_dlnMh(z_input, dn_dlnmh_array):
      
    z_min = 2.3
    z_max = 8.0
    zeta = np.linspace(z_min, z_max, 500)
    
    z_inds = np.where(z_input <= zeta)
    z_ind = (z_inds[0])[0]

    return dn_dlnmh_array[z_ind, :]

### Background functions

def hubble(z_input): 
    
    E = (omega_m * (1.0 + z_input)**3 + omega_k * (1.0 + z_input)**2 + omega_d * (1.0 + z_input)**(3.0 * (1.0 + w_0)))**0.5

    return E

def hubble_time(z_input):

    t_h = (1. / (H_0 * hubble(z_input)))

    return (3.0857e19 / (365. * 24. * 60.**2)) * t_h

def universe_age(z_input):

    fz = lambda x: 1.0 / ((1 + x) * (omega_m * (1.0 + x)**3 + omega_k * (1.0 + x)**2 + omega_d * (1.0 + x)**(3 * (1.0 + w_0)))**0.5)

    t_age = (13.82 * 1.e9) - 3.0857e19 * (1. / H_0) * si.romberg(fz, 0, z_input) * (1.0 / (365. * 24. * 60.**2))

    return t_age

def comoving_dist(z_input):
    
    fz = lambda x: 1.0 / (omega_m * (1.0 + x)**3 + omega_k * (1.0 + x)**2 + omega_d * (1.0 + x)**(3 * (1.0 + w_0)))**0.5

    comoving_dist = (c / H_0) * si.romberg(fz, 0, z_input)

    return comoving_dist

### CO backdround temperature -- Lidz et al. 2011

def temp_co(z_input, mh_array, dn_dlnmh_array):
    
    rho_c = (3. * 1.e4 * h**2) / (8. * np.pi * g_const)   #  M_o * Mpc^{-3}
    rho_m = rho_c * omega_m

    m_array = mh_array * (omega_m / h)
    m_ind_min = (np.where(m_array >= m_min)[0])[0]
    m_ind_max = (np.where(m_array <= m_max)[0])[-1]
    m_array = m_array[m_ind_min:m_ind_max + 1]

    dn_dlnm_array = dn_dlnMh(z_input, dn_dlnmh_array) * h**3
    dn_dlnm_array = dn_dlnm_array[m_ind_min:m_ind_max + 1]
    
#    temp_co = 0.60 * (amplitude / 2.e-6) * (hubble_time(3) / hubble_time(z_input)) * (hubble(3) / hubble(z_input)) * ((1 + z_input) / 4)**2 * (si.simps(dn_dM_M_array, mass_array) / 7.05e9)

    temp_co = 0.65 * amplitude * (1.0 / 0.3) * (si.simps(dn_dlnm_array, m_array) / rho_m) * (universe_age(2) / universe_age(z_input)) * (hubble(2) / hubble(z_input)) * ((1 + z_input) / 3.)**2

    return temp_co

### CO bias -- Breysse et al 2014

def bias(z_input, m_array, mh_array, dn_dlnmh_array, sigma_array, k_calc, pk_calc):

    delta_c = 1.69

    m_array_ind = mh_array * (omega_m / h)
    m_ind_min = (np.where(m_array_ind >= m_min)[0])[0]
    m_ind_max = (np.where(m_array_ind <= m_max)[0])[-1]
    dn_dlnm_array = dn_dlnMh(z_input, dn_dlnmh_array) * h**3
    dn_dlnm_array = dn_dlnm_array[m_ind_min:m_ind_max + 1]

    bias_array = 1.0 + (((delta_c / (sigma_array * growth_factor(z_input))) - 1.0) / delta_c)
        
    bias_integrand = dn_dlnm_array * bias_array

    return si.simps(bias_integrand, m_array) / si.simps(dn_dlnm_array, m_array)

### CO shot noise ps -- Breysse et al 2014

def ps_shot(z_input, mh_array, dn_dlnmh_array):
    
#    f_duty = (1.0e8 / universe_age(z_input))

    m_array = mh_array * (omega_m / h)
    m_ind_min = (np.where(m_array >= m_min)[0])[0]
    m_ind_max = (np.where(m_array <= m_max)[0])[-1]
    m_array = m_array[m_ind_min:m_ind_max + 1]

    dn_dlnm_array = dn_dlnMh(z_input, dn_dlnmh_array) * h**3
    dn_dlnm_array = dn_dlnm_array[m_ind_min:m_ind_max + 1]

    dn_dM_M_squared_array = m_array * dn_dlnm_array

    return (1. / f_duty) * (si.simps(dn_dM_M_squared_array, m_array) / si.simps(dn_dlnm_array, m_array)**2)

### CO total ps (only the clustering part) -- Breysse et al 2014

def ps_co(z_input, k_input, m_array, mh_array, dn_dlnmh_array, sigma_array, k_calc, pk_calc):

#    return temp_co(z_input, mh_array, dn_dlnmh_array)**2 * (bias(z_input, m_array, mh_array, dn_dlnmh_array, sigma_array, k_calc, pk_calc)**2 * growth_factor(z_input)**2 * power_spectrum(k_input, k_calc, pk_calc) + ps_shot(z_input, mh_array, dn_dlnmh_array))

    return temp_co(z_input, mh_array, dn_dlnmh_array)**2 * (bias(z_input, m_array, mh_array, dn_dlnmh_array, sigma_array, k_calc, pk_calc)**2 * growth_factor(z_input)**2 * power_spectrum(k_input, k_calc, pk_calc))

### Ang power spectrum -- Limber approx -- Breysse et al 2014

def cl_limber(l, z_min, z_max, h_input, cd_input, m_array, mh_array, dn_dlnmh_array, sigma_array, k_calc, pk_calc):

    if l == 0:
        l = 1.0e-3
    
    windows = 1.0 / (z_max - z_min)   # Windows function (should not be hard coded, but for now we are only using a uniform window in redshift)

    N = h_input.size # samples - odd
    z = np.linspace(z_min, z_max, N, endpoint=True)

    integrand_array = np.zeros(N)

    for i in range (0, N):
        integrand_array[i] = h_input[i] * (1.0 / (cd_input[i])**2) * ps_co(z[i], (l + 0.5) / cd_input[i], m_array, mh_array, dn_dlnmh_array, sigma_array, k_calc, pk_calc)
        
    cl_limber = (H_0 / c) * windows**2 * si.simps(integrand_array, z)

    return cl_limber

##### Main function: calculates CO maps for each frequency channel (no correlation for now) - microK  

### WARNING: This code only works for COMAP freqs: 26 to 34 GHz                                
##### Main function: calculates CO maps for each frequency channel (no correlation for now) - microK                                           

###                                                                             
# Inputs: freq_min -- minimum frequency in GHz                                  
#         freq_width -- width of the frequency channels in GHz                 
#         nchannels -- number of channels                                       
#         nside (lmax = 3 * nside - 1) -- nside of the Healpix maps             
#         suffix -- string for the file suffix -- string with the suffix of the output fits file                                              
#                                                                               
# Output: fits file named 'map_hi_' + suffix + '.fits' with an array a[npixel, nchannels], where npixel is the number of pixels of the maps. The maps are given in microK.                                                                          
###                                                                             

# The Cosmology should be an input!                                            
# CAMB should be called.

def co_ps_maps(freq_min, freq_width, nchannels, nside, fwhm, suffix=None, metadir=''):

#, suffix):

    # Defining the experiment                                                                                                                 
    freq_half = freq_width / 2.0
    channels = np.zeros(nchannels)
    ind = np.arange(nchannels)
    channels = freq_min + ind * freq_width

    lmax = 3 * nside - 1
    ell = np.arange(lmax + 1)
    c_ell = np.zeros(lmax + 1)

    # Calculating the power spectrum - should really call CAMB to calculate the power spectrum for a particular Cosmology

    file_pk = '{}/{}'.format(metadir,'CO_Powerspec.dat')
    powerspec = np.loadtxt(file_pk)
    k_calc = powerspec[:,0] #np.loadtxt(file_k)
    k_calc = k_calc / h
    pk_calc = powerspec[:,1]#np.loadtxt(file_pk)
    pk_calc = h**3 * pk_calc

    # Calculating the halo mass function - I am using a halo mass function that comes with CosmoSIS -- should really call CosmoSIS to calculate the halo mass for a particular Cosmology

    file_mh = '{}/{}'.format(metadir,"mf_tinker/mass_function/m_h.txt")
    mh_array = np.loadtxt(file_mh)
    file_dn = '{}/{}'.format(metadir,"mf_tinker/mass_function/dndlnmh.txt")
    dn_dlnmh_array = np.loadtxt(file_dn)

    m_array = mh_array * (omega_m / h)
    m_ind_min = (np.where(m_array >= m_min)[0])[0]
    m_ind_max = (np.where(m_array <= m_max)[0])[-1]
    m_array = m_array[m_ind_min:m_ind_max + 1]

    sigma_array = np.zeros(m_array.size)

    for i in range(0, m_array.size):
        sigma_array[i] = sigma_m(m_array[i], k_calc, pk_calc)

    # Hubble parameter and comoving distance

    samples = 11 # samples - odd 
    h_input = np.zeros(samples)
    cd_input = np.zeros(samples)

    maps = np.zeros((nchannels, hp.nside2npix(nside)))
    for i in range (0, nchannels):
        z_min = (freq_line_co / (channels[i] + freq_half)) - 1.0
        z_max = (freq_line_co / (channels[i] - freq_half)) - 1.0
        z = np.linspace(z_min, z_max, samples, endpoint=True)    
        
        for j in range (0, samples):
            h_input[j] = hubble(z[j])
            cd_input[j] = comoving_dist(z[j])

        for l in range (0, lmax + 1):
            c_ell[l] = cl_limber(l, z_min, z_max, h_input, cd_input, m_array, mh_array, dn_dlnmh_array, sigma_array, k_calc, pk_calc)

        map = hp.sphtfunc.synfast(c_ell[:]*hp.gauss_beam(fwhm=fwhm, lmax=(lmax)), nside, verbose=False)        
        maps[i, :] = map[:]/1e6 # Convert to K

    #file = 'maps_co' + suffix + '.fits'
    #pyfits.writeto(file, maps, clobber=True)
    if not isinstance(suffix, type(None)):
        file_map = 'map_co_' + suffix + '.fits'
        pyfits.writeto(file_map, maps, clobber=True)
    else:
        return maps, np.arange(lmax+1), c_ell


if __name__ == "__main__":

    # Calculating the power spectrum - should really call CAMB to calculate the power spectrum for a particular Cosmology


    nside = 16
    metadir = '/home/sharper/IM_Dev/IM_Sim_Ancil/CO'
    maps, l, cl = co_ps_maps(26, 0.05, 2, nside, np.pi/180., metadir=metadir)

    
    #maps[:,0] = np.random.normal(size=12*nside**2, scale=10)
    pk = hp.anafast(maps[0,:]) * 12*nside**2/4./np.pi
    from matplotlib import pyplot
    hp.mollview(maps[0,:])
    pyplot.show()
