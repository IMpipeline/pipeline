import numpy as np
from matplotlib import pyplot
import healpy as hp

import glob
import os
import subprocess

from pipeline.Observatory.Sky import Sky
from scipy.ndimage.filters import gaussian_filter

from pipeline.Tools import FileTools

def FreeModel1(filenames, Te,  chanEdges, fwhm, coordSystem, nside):
    """

    This map is in the Galactic coordinate system.

    """

    # Use h-alpha map to estimate free-free amplitude 
    halpha = hp.read_map('{}'.format(filenames[0]))
    halpha_c = 'G'
    halpha[np.isnan(halpha) | np.isinf(halpha)] = hp.UNSEEN
    
    halpha = hp.ud_grade(halpha, nside)

    if coordSystem == 'Celestial':
        halpha = HealpixRotate(halpha, coords=['C', 'G'])


    T4 = Te*1e-4 
    chanMids = (chanEdges[1:] + chanEdges[:-1])/2.
    nu = chanMids*1e-9 # GHz

    
    # R/K factor from Dicki
    a_factor = 0.366*nu**0.1 * Te**-0.15 * (np.log(4.995*1e-2/nu) + 1.5*np.log(Te))
    factor   = 8.396e3 * a_factor * nu**-2.1 * T4**0.667 * 10**(0.029/T4) * 1.08 * 1e-6

    factor = np.reshape(factor, (1, factor.size))
    halpha = np.reshape(halpha, (1, halpha.size))

    pyplot.plot(chanMids, factor[0,:])
    pyplot.show()
    maps   = factor.T*halpha


    return maps

def CommanderFFwithHalpha(AncilMaps,  chanEdges):
    """

    This map is in the Galactic coordinate system.

    """

    halpha = AncilMaps[0]
    EM = AncilMaps[2]
    Te = AncilMaps[1]

    npix = Te.size

    chanMids = (chanEdges[1:] + chanEdges[:-1])/2.
    nu = chanMids*1e-9 # GHz

    maps = np.zeros((2, nu.size, Te.size))
    # R/K factor from Dicki
    Te   = np.reshape(Te , (1, Te.size))
    EM   = np.reshape(EM , (1, EM.size))
    nu   = np.reshape(nu, (1, nu.size))


    # TeM = 7000.
    a_factor = 0.366*nu.T**0.1 * Te**-0.15 * (np.log(4.995*1e-2/nu.T) + 1.5*np.log(Te))
    factor   = 8.396e3 * a_factor * nu.T**-2.1 * (Te*1e-4)**0.667 * 10**(0.029/(Te*1e-4)) * 1.08 * 1e-6

    halpha = np.reshape(halpha, (1, halpha.size))

    maps[0,...]   = factor* halpha

    # Now for the calculation for optical depth from Tools for Radio ....
    tc = 8.235e-2 * nu.T**-2.1 * Te**-1.35 * EM
    maps[1,...]  = Te * (1. - np.exp(-tc))

    a = maps[1,:,:]

    return np.max(maps[:,:,:],axis=0)

FreeModels = {'Model1': FreeModel1, 'CommFF1':CommanderFFwithHalpha}

#def GenerateFreeFree(model, datadir, datafiles, Te, fMax, fMin, fChannels, fwhm, 
#                     coordSystem, 
#                     mode, # Pixel projection 
#                     npix, # Number of pixels
#                     opticsID=0, backendID=0, write=True, writeFile='default.fits'):

def GenerateFreeFree(simInfo, write=True, writeFile='default.fits'):

    """
    Generate the free-free sky model.

    Model - fMax - fMin - fChannels - CoordSystem
    """
    
    model     = simInfo.Parameters['FreeFree']['model']
    datadir   = simInfo.Parameters['FreeFree']['ancil_dir']
    datafiles = simInfo.Parameters['FreeFree']['ancil_files']
    Te        = simInfo.Parameters['FreeFree']['electron_temp']
    coordSystem = simInfo.Parameters['Mapping']['coordinate_system']

    chanEdges = np.linspace(simInfo.minFreq, simInfo.maxFreq, simInfo.nchannels + 1, endpoint=True)
    chanMids  = (chanEdges[1:] + chanEdges[:-1])/2.

    if not isinstance(datafiles, list):
        datafiles = [datafiles]
    filenames = ['{}/{}'.format(datadir, filename) for filename in datafiles]

    
    nside = int(simInfo.Parameters['Mapping']['hp_nside'])

    if simInfo.Parameters['Mapping']['mode'].upper() == 'HEALPIX':
        ancilMaps = Sky.SetupAncilMaps(filenames, nside, coordSystem)
    elif simInfo.Parameters['Mapping']['mode'].upper() == 'CARTESIAN':
        naxis = [simInfo.Parameters['Mapping']['ct_naxis1'], simInfo.Parameters['Mapping']['ct_naxis2']]
        cdelt = [simInfo.Parameters['Mapping']['ct_cdelt1'], simInfo.Parameters['Mapping']['ct_cdelt2']]
        crval = [simInfo.Parameters['Mapping']['ct_crval1'], simInfo.Parameters['Mapping']['ct_crval2']]

        ancilMaps = Sky.SetupAncilMaps(filenames, nside, simInfo.Parameters['Mapping']['coordinate_system'],
                                       mode = simInfo.Parameters['Mapping']['mode'].upper(), naxis=naxis, cdelt= cdelt, crval=crval)
    else:
        print('# INVALID MAPPING MODE INPUT {}, PLEASE CHOOSE CARTESIAN OR HEALPIX'.format(simInfo.Parameters['Mapping']['mode'].upper()))
        raise ValueError

    maps = FreeModels[model](ancilMaps, chanEdges)

    maps = Sky.SmoothMaps(maps, simInfo, chanMids, chanEdges)

    if write:
        if simInfo.Parameters['Mapping']['mode'].upper() == 'HEALPIX':
            hp.write_map(writeFile, maps)
        else:
            FileTools.WriteH5Py(writeFile, {'maps':maps})
    else:
        return maps



