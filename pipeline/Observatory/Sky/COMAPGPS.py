import numpy as np
from matplotlib import pyplot
import healpy as hp

import glob
import os
import subprocess
from pipeline.Tools.HPTools import HealpixRotate
from pipeline.Observatory.Telescope import Coordinates
from scipy.interpolate import griddata

import scipy.interpolate as spint
import scipy.spatial.qhull as qhull
import itertools    

from pipeline.Observatory.Sky import Sky
from scipy.ndimage.filters import gaussian_filter

from pipeline.Tools import FileTools

from scipy.interpolate import  RectBivariateSpline as RBS
from scipy.interpolate import interp1d
import h5py 
from pipeline.Tools import CartPix

def GenerateCOMAPGPS(simInfo, write=True, writeFile='default.fits'):

    """
    Generate the synchrotron sky model.

    Model - fMax - fMin - fChannels - CoordSystem
    """
    model     = simInfo.Parameters['COMAPGPS']['model']
    datadir   = simInfo.Parameters['COMAPGPS']['ancil_dir']
    datafiles = simInfo.Parameters['COMAPGPS']['ancil_files']
    coordSystem = simInfo.Parameters['Mapping']['coordinate_system']
    chanEdges = np.linspace(simInfo.minFreq, simInfo.maxFreq, simInfo.nchannels + 1, endpoint=True)
    chanMids  = (chanEdges[1:] + chanEdges[:-1])/2.


    if not isinstance(datafiles, list):
        datafiles = [datafiles]
    filenames = ['{}/{}'.format(datadir, filename) for filename in datafiles]
    
    naxis = np.array([simInfo.Parameters['Mapping']['ct_naxis1'], simInfo.Parameters['Mapping']['ct_naxis2']]).astype(int)
    cdelt = np.array([simInfo.Parameters['Mapping']['ct_cdelt1'], simInfo.Parameters['Mapping']['ct_cdelt2']]).astype(float)
    crval = np.array([simInfo.Parameters['Mapping']['ct_crval1'], simInfo.Parameters['Mapping']['ct_crval2']]).astype(float)
    maps  = np.zeros((simInfo.nchannels, naxis[0]* naxis[1]))

    ancilFiles = h5py.File(datadir +'/'+ datafiles[0])
    keys = ['VGPS', 'Tau353']
    ancilMaps = [ancilFiles[key][...] for key in keys]
    naxisMap = ancilFiles['naxis'][...]
    cdeltMap = ancilFiles['cdelt'][...]
    crvalMap = ancilFiles['crval'][...]

    # SPDUST Model
    spdust     = np.loadtxt(datadir +'/'+ datafiles[1], comments=';')
    normFreq   = ancilFiles['normFreq'][0]
    normIndex  = np.argmin((spdust[:,0]-normFreq)**2)
    spdust[:,1] /= spdust[normIndex,1]
    spdustModel = interp1d(spdust[:,0], spdust[:,1])

    # Create data cube
    data = ancilMaps[0][np.newaxis,:,:] * (1.420e9/chanMids[:,np.newaxis,np.newaxis])**2.3
    data += ancilMaps[1][np.newaxis,:,:] * (spdustModel(chanMids*1e-9)* (normFreq*1e9/chanMids)**2)[:,np.newaxis,np.newaxis] 
    data[np.isnan(data)] = 0
    pyplot.imshow(data[0,:,:])
    pyplot.show()
    # need to get coordinates of map in ra/dec not gl/gb
    _, glMap = CartPix.pix2ang(naxisMap, [-cdeltMap[0], cdeltMap[1]], crvalMap,np.arange(naxisMap[0]) , np.zeros(naxisMap[0]))
    gbMap, _ = CartPix.pix2ang(naxisMap, [-cdeltMap[0], cdeltMap[1]], crvalMap,np.zeros(naxisMap[1]) , np.arange(naxisMap[1]))
    glMap = glMap

    #data = data[:,sortGb,:]
    #data = data[:,:,sortGl]


    # regrid to data
    ypix, xpix = np.meshgrid(np.arange(naxis[0]), np.arange(naxis[1]))

    decData, raData = CartPix.pix2ang(naxis, [cdelt[0],cdelt[1]], crval, xpix, ypix)
    rot = hp.Rotator(coord=['C','G'])
    gbData, glData = rot((90-decData.flatten())*np.pi/180., raData.flatten()*np.pi/180.)
    gbData, glData = (np.pi/2. - gbData)*180./np.pi, glData*180./np.pi
    gbData, glData = np.reshape(gbData, decData.shape), np.reshape(glData, raData.shape)


    for i in range(simInfo.nchannels):
        modelMap = RBS(glMap, gbMap, data[i][:,:].T)
        maps[i,:] = modelMap.ev(glData[::-1,:].flatten(), gbData[::-1,:].flatten())
    

    maps = Sky.SmoothMaps(maps, simInfo, chanMids, chanEdges)

    print(naxis, cdelt, crval)
    wcs = CartPix.Info2WCS(naxis, [cdelt[0], cdelt[1]], crval)

    pyplot.subplot(projection=wcs)
    pyplot.imshow(np.log10((np.reshape(maps[0,:], (naxis[1], naxis[0])).T)), aspect='auto', origin='lower')
    pyplot.xlabel('Right Ascension')
    pyplot.ylabel('Declination')
    overlay = pyplot.gca().get_coords_overlay('galactic')
    overlay.grid(color='white', ls='dotted')
    overlay[0].set_axislabel('Galactic Longitude')
    overlay[1].set_axislabel('Galactic Latitude')
    #pyplot.savefig('RotatedMap.png',bbox_inches='tight')
    pyplot.show()

    if write:
        if simInfo.Parameters['Mapping']['mode'].upper() == 'HEALPIX':
            hp.write_map(writeFile, maps)
        else:
            FileTools.WriteH5Py(writeFile, {'maps':maps})
    else:
        return maps


