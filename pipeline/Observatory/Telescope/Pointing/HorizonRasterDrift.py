import numpy as np
from pipeline.Tools import General
from scipy import signal

from pipeline.Observatory.Telescope import Coordinates


def HorizonRasterDrift(simInfo, startTime, endTime,  lon, lat, mode, obsLength,xcen, ycen, dslew, ijd): 
    """
    Horizon raster calculates assuming slewing in azimuth at a fixed elevation
     
    Notes:
    Assumes no acceleration
    
    Arguments
    simInfo   - Contains all the simulation input parameters
    startTime - Relative start time of the observation in seconds
    endTime   - Relative end time of the observation in seconds
    mode      - String describing the name of the observing mode
    obsLength   - Length of the observation
    xcen        - Central azimuth
    ycen        - Central elevation
    dslew       - Distance azimuth slew in degrees
    
    Returns
    az - the azimuth of the telescope boresight in degrees
    el - the elevation of the telescope boresight in degrees
    jd - the julian date of the observation 
    
    """
    
    nSamples = General.SafeInt( (endTime - startTime))*int(simInfo.sampleRate)  # You want this to avoid floating point rounding errors
        
    # How many slews in the time available?
    tSlew = dslew/simInfo.Parameters['Telescope']['speed']*2 # x2 for forward and backward slew    
    t = np.linspace(startTime, endTime, nSamples)
    az = signal.sawtooth(2.*np.pi*t/tSlew, 0.5)*dslew/2. + xcen
    el = np.ones(nSamples)*ycen
    jd = t/24./3600. + ijd


    return az, el, jd

def SkyBox(xcen, ycen, delta, lon, lat, jd):
    ires = 100
    dra = np.concatenate(( np.linspace(-delta/2., delta/2., ires),
                          np.linspace(-delta/2., delta/2., ires),
                          np.ones(ires)*-delta/2., 
                          np.ones(ires)* delta/2.))
    
    ddec = np.concatenate((np.ones(ires)*-delta/2., 
                          np.ones(ires)* delta/2.,
                          np.linspace(-delta/2., delta/2., ires),
                          np.linspace(-delta/2., delta/2., ires)))
    
    dec = ddec + ycen
    ra  = xcen + dra/np.cos(dec*np.pi/180.)
    azbox, elbox = Coordinates._equ2hor(ra, dec, np.ones(ra.size)*jd, lat, lon)

    return azbox, elbox

def WriteHorizonRasterDrift(Parameters, az, el, jd, lon, lat):
    """
    Determines the best start julian date, given a source at az, el at time jd.

    Arguments:
    Parameters - Dictionary containing input simulation parameters
    az - azimuth of source
    el - elevation of source 
    jd - julian date of source

    Returns
    line - Dictionary of {'keyword': values, ...} needed for the inputs to HorizonRaster

    """
    
    # Estimate the box that we want to observe
    delta = Parameters['Observations']['dslew']
    ycen =  Parameters['Observations']['deccen']
    xcen =  Parameters['Observations']['racen']

    # Estimate observing time
    skyRotSpeed= 360./24./3600.*np.cos(Parameters['Observations']['deccen']*np.pi/180.)
    obsLength = np.floor(delta/skyRotSpeed)

    # Convert to az and el and calculate slew length in azimuth
    # First calculate sky box at the start of the observation
    az1, el1 = SkyBox(xcen, ycen, delta, lon, lat, jd-obsLength/2./24./3600.)
    az2, el2 = SkyBox(xcen, ycen, delta, lon, lat, jd+obsLength/2./24./3600.)
 
    # Find the maximum span of azimuth between the two times.
    dslew = np.max([az1,az2]) - np.min([az1,az2])
        

    outline = [['mode', Parameters['Observations']['mode']],
               ['observation_length', obsLength],
               ['maxaz', az],
               ['ycen', el],
               ['dslew', dslew],
               ['ijd', jd - obsLength/2./24./3600.]]
    

    return outline
