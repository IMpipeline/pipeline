from __future__ import absolute_import, division, print_function, unicode_literals
import numpy as np
from pipeline.Tools import General
from pipeline.Observatory.Telescope import Coordinates, Observations

def _lissajous(t, dx, w, a, offset=0.):
    p = np.pi/2.
    
    #if np.mod(a, 2) == 0:
    #    _b = a
    #    _a = a + 1
    #else:   
    _b = a + 1
    _a = a
    
    x = dx[0]*np.sin(_a*w*(t + offset) + p )
    y = dx[1]*np.sin(_b*w*(t + offset))

    return x, y


def LissajousAzEl(simInfo, startTime, endTime, lon, lat, mode, obsLength, rcen, dcen, rlen, dlen, ijd, a): 
    """
    Lissajous scan strategy assuming sky tracking.
    
    Notes:
    Assumes that the angular rotation rate of the lissajous pattern 
    is the quadratic sum of the maximum az and el scan rates (amax & emax).
    This assumption is fairly robust except for special circumstances (e.g. near poles).
    No accelerations are modelled.
    
    Arguments
    simInfo   - Contains all the simulation input parameters
    startTime - Relative start time of the observation in seconds
    endTime   - Relative end time of the observation in seconds
    mode      - String describing the name of the observing mode
    obsLength   - Length of the observation
    rcen       - Central RA coordinate (degrees)
    dcen       - Central dec coordinate (degrees)
    rlen       - Width of field in RA (degrees) 
    dlen       - Width of field in Declination (degrees)
    amax       - Maximum azimuthal slew rate (degrees/s)
    emax       - Maximum elevation slew rate (degrees/s)
    lon        - Longitude of the observer (+W) (degrees)
    lat        - Latitude of observer (+N) (degrees)
    ijd        - Julian date start time.
    a          - Lissajous constant, approximately the number density of scans across field
    
    Returns
    az - the azimuth of the telescope boresight in degrees
    el - the elevation of the telescope boresight in degrees
    jd - the julian date of the observation 
    
    """

    nSamples = General.SafeInt( (endTime - startTime))*int(simInfo.sampleRate)  # You want this to avoid floating point rounding errors

    # Calculate lissajous constants
    if np.mod(a, 2) == 0:
        _b = a
        _a = a - 1
    else:   
        _b = a - 1
        _a = a

        

    # Timing arrays
    t  = np.linspace(startTime, endTime, nSamples) 
    jd = t/24./3600. + ijd 


    # Calculate az and el positions of the telescope
    ras, decs = Coordinates._hor2equ(rcen*np.ones(nSamples), dcen*np.ones(nSamples), jd, lat, lon)

    

    # Calculate time dependent offsets in sky position
    w = simInfo.Parameters['Observations']['wspeed']*np.pi/180.
    a = simInfo.Parameters['Observations']['aLissajous']
    
    dAz, dEl = _lissajous(t, [rlen, dlen],w, a)#, [rlen/np.cos(el*np.pi/180.), dlen], [_a, _b])
    #vaz = np.max(np.abs(np.gradient(az+dAz)*simInfo.sampleRate))
    #w = simInfo.Parameters['Telescope']['speed']/vaz
    #dAz, dEl = _lissajous(t*w, [rlen/np.cos(el*np.pi/180.), dlen], [_a, _b])
    #el = dcen*np.ones(nSamples) #+ dEl
    #az = rcen*np.ones(nSamples) #+ dAz/np.cos(el*np.pi/180.)
    
    el = dcen + dEl
    az = rcen + dAz/np.cos(el*np.pi/180.)

    return az, el, jd


def OldWriteLissajousAzEl(Parameters, ijd, lon, lat):
    """
    Determines the best start julian date, given a source at az, el at time jd.

    Arguments:
    Parameters - Dictionary containing input simulation parameters
    az - azimuth of source
    el - elevation of source 
    jd - julian date of source

    Returns
    line - Dictionary of {'keyword': values, ...} needed for the inputs to HorizonRaster

    """
        
    ra = Parameters['Observations']['racen']
    dec = Parameters['Observations']['deccen']
    emin = Parameters['Observations']['elmin']
    emax = Parameters['Observations']['elmax']
    pmin = Parameters['Observations']['pmin']

    az, el, jd = Observations.BestAzElJD(ijd, ra, dec, emax, emin, pmin, lat, lon)

    obsLength = Parameters['Observations']['obsLen']

    outline = [['mode', Parameters['Observations']['mode']],
               ['observation_length', Parameters['Observations']['obsLen']],
               ['xcen',  Parameters['Observations']['racen']],
               ['ycen',  Parameters['Observations']['deccen']],
               ['rlen', Parameters['Observations']['radist']],
               ['dlen', Parameters['Observations']['decdist']],
               ['ijd', jd],
               ['a', Parameters['Observations']['a']]]
    

    return jd, obsLength, outline

def WriteLissajousAzEl(Parameters, meanLon, meanLat):
    """
    Determines the best start julian date, given a source at az, el at time jd.

    Arguments:
    Parameters - Dictionary containing input simulation parameters
    az - azimuth of source
    el - elevation of source 
    jd - julian date of source

    Returns
    line - Dictionary of {'keyword': values, ...} needed for the inputs to HorizonRaster

    """
    mode   = Parameters['Observations']['mode']
    obsLen = Parameters['Observations']['obsLen']


    ra = Parameters['Observations']['racen']
    dec = Parameters['Observations']['deccen']
    
    raz= Parameters['Observations']['raz']
    rel= Parameters['Observations']['rel']
    a  = Parameters['Observations']['a']
    
    fieldWidth = raz*2
    ijd    = Parameters['Observations']['ijd']
    ejd    = Parameters['Observations']['ejd']

    emin = Parameters['Observations']['elmin']
    emax = Parameters['Observations']['elmax']
    hamin = Parameters['Observations']['hamin']/15.
        
        
    # Derive sky track (HA vs. elevation) for 1 day
    nsamps = 1000
    jd = np.linspace(0,1, nsamps) + ijd
    lst = Coordinates._jd2lst(meanLon, jd)
    ha = Coordinates._ra2ha(ra, lst)
    az, el = Coordinates._equ2hor(ra*np.ones(nsamps), dec*np.ones(nsamps), jd, meanLat, meanLon)
    pang = Coordinates._pang(el, dec*np.ones(nsamps), meanLat)
    # Which regions are above the minimum elevation?
    goodEl = (el > emin) & (el < emax) & (ha < 12) & (ha > hamin)

    # Range of hours
    maxHA = np.max(ha[goodEl])
    haRange = np.max(ha[goodEl])-np.min(ha[goodEl])
    siderealRate = 360./24.
    driftTime = fieldWidth/siderealRate/np.cos(dec*np.pi/180.) # in hours
    nScans = int(haRange/driftTime)

    # How many days?
    nDays = ejd - ijd
    if np.mod(nDays, 1) != 0:
        nDays = int(nDays)+1 # Always round up 
    else:
        nDays = int(nDays) # ...unless there is exactly an integer number of days
        
    fraction = 0.05
    f = open(Parameters['Observations']['configurationFile'],'w')
        
        
    outline = [['mode', Parameters['Observations']['mode']],
               ['observation_length', Parameters['Observations']['obsLen']],
               ['xcen',  Parameters['Observations']['racen']],
               ['ycen',  Parameters['Observations']['deccen']],
               ['rlen', Parameters['Observations']['radist']],
               ['dlen', Parameters['Observations']['decdist']],
               ['ijd', jd],
               ['a', Parameters['Observations']['a']]]

    lineone = ['mode', 'observation_length', 'xcen', 'ycen', 'raz', 'rel', 'ijd' ,'a']
    for i in range(nDays):
        starts = np.mod(driftTime*(np.arange(nScans)+1) + i*fraction*driftTime , haRange) + np.min(ha[goodEl])
        starts = np.concatenate((starts, 24-starts))
        jdStarts = jd[np.argmin((ha[:,np.newaxis]-starts[np.newaxis,:])**2,axis=0)]
        jsort = np.argsort(jdStarts)
        jdStarts = np.sort(jdStarts)
        
        # check difference in start times is reasonable
        driftTimeJD=driftTime/24.
        for j in range(jdStarts.size-1):
            diff = jdStarts[j+1] - jdStarts[j]
            if diff < driftTimeJD:
                jdStarts[j+1:] += driftTimeJD - diff
                         
        if i == 0:
            kstring = [lineone[j]+' ' for j in range(len(lineone))] + ['\n']
            f.write(''.join(kstring))
                    
        for j in range(jdStarts.size):
            xcen,ycen = Coordinates._equ2hor(ra*np.ones(1), dec*np.ones(1), jdStarts[j:j+1], meanLat, meanLon)
            scanLength = fieldWidth/np.cos(ycen[0]*np.pi/180.)
            lineout = [int(driftTime*3600.), xcen[0], ycen[0], raz, rel, jdStarts[j]- driftTime/2./24., a]
            vstring = [mode+' '] + [str(lineout[j])+' ' for j in  range(len(lineout))] + ['\n']
            f.write(''.join(vstring))
    
