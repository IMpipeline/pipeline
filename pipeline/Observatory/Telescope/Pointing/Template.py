from __future__ import absolute_import, division, print_function, unicode_literals
import numpy as np
from pipeline.Tools import General


def Template(simInfo, startTime, endTime, mode, obsLength, lon, lat, a, b, c): 
    """
    Template example for building a scanning strategy module
    
    Notes:
    All functions must accept a simInfo object, startTime, and endTime of the observation in seconds, 
    the mode string of the observation, and then the other parameters that observing mode needs.
    
    Don't forget to add your function to __init__.py as from Template import *
    
    Arguments
    simInfo   - Contains all the simulation input parameters
    startTime - Relative start time of the observation in seconds
    endTime   - Relative end time of the observation in seconds
    mode      - String describing the name of the observing mode
    obsLength   - Length of the observation
    a         - Some parameter a
    b         - Some parameter b
    c         - Some parameter c
    
    Returns
    az - the azimuth of the telescope boresight in degrees
    el - the elevation of the telescope boresight in degrees
    jd - the julian date of the observation 
    
    """
    
    nSamples = General.SafeInt( (endTime - startTime))*int(simInfo.sampleRate)  # You want this to avoid floating point rounding errors

        
    
    ####
    # PERFORM Calculations for the scan strategy
    ####
    
    az = np.zeros(nSamples)
    el = np.zeros(nSamples)
    jd = np.zeros(nSamples)
    
    return az, el, jd

def WriteTemplate(Parameters, az, el, jd):
    """
    Determines the best start julian date, given a source at az, el at time jd.

    Notes:
    This function is called in SimInfoClass if no observation.dat is specified and one 
    needs to be generated. Every pointing operation needs use this function to define
    its input parameters.

    Arguments:
    Parameters - Dictionary containing input simulation parameters
    az - azimuth of source
    el - elevation of source 
    jd - julian date of source

    Returns
    line - Dictionary of {'keyword': values, ...} 

    """
    
    # Minimum Parameter dictionary
    # Order 
    outline = [['mode', Parameters['Observations']['mode'] ],
               ['observation_length', Parameters['Observations']['obsLen']]]

    return outline
