from __future__ import absolute_import, division, print_function, unicode_literals
import numpy as np
from pipeline.Tools import General
from pipeline.Observatory.Telescope import Coordinates, Observations

def _lissajous(t, dx, da, offset=0.):
    p = np.pi/2.
    
    x = dx[0]*np.sin(da[0]*(t + offset) + p )
    y = dx[1]*np.sin(da[1]*(t + offset))
    
    return x, y

def CalcLissajous(t, jd, w, _a, _b, rlen, dlen, fwhm, rcen, dcen, lat, lon):
    # Calculate time dependent offsets in sky position
    dra, ddec = _lissajous(t*w, [rlen, dlen], [_a, _b])
    
    # Calculate sky positions
    dec = dcen+ddec + np.random.normal(scale=fwhm/2.)
    ra = rcen+dra/np.cos(dec*np.pi/180.) + np.random.normal(scale=fwhm/2.)
    
    # Calculate az and el positions of the telescope
    az, el = Coordinates._equ2hor(ra, dec, jd, lat, lon)

    return az, el


def Lissajous(simInfo, startTime, endTime, lon, lat, mode, obsLength, rcen, dcen, rlen, dlen, ijd, a): 
    """
    Lissajous scan strategy assuming sky tracking.
    
    Notes:
    Assumes that the angular rotation rate of the lissajous pattern 
    is the quadratic sum of the maximum az and el scan rates (amax & emax).
    This assumption is fairly robust except for special circumstances (e.g. near poles).
    No accelerations are modelled.
    
    Arguments
    simInfo   - Contains all the simulation input parameters
    startTime - Relative start time of the observation in seconds
    endTime   - Relative end time of the observation in seconds
    mode      - String describing the name of the observing mode
    obsLength   - Length of the observation
    rcen       - Central RA coordinate (degrees)
    dcen       - Central dec coordinate (degrees)
    rlen       - Width of field in RA (degrees) 
    dlen       - Width of field in Declination (degrees)
    amax       - Maximum azimuthal slew rate (degrees/s)
    emax       - Maximum elevation slew rate (degrees/s)
    lon        - Longitude of the observer (+W) (degrees)
    lat        - Latitude of observer (+N) (degrees)
    ijd        - Julian date start time.
    a          - Lissajous constant, approximately the number density of scans across field
    
    Returns
    az - the azimuth of the telescope boresight in degrees
    el - the elevation of the telescope boresight in degrees
    jd - the julian date of the observation 
    
    """
    
    nSamples = General.SafeInt( (endTime - startTime))*int(simInfo.sampleRate)  # You want this to avoid floating point rounding errors

        

    # Timing arrays
    t  = np.linspace(startTime, endTime, nSamples) + startTime
    jd = t/24./3600. + ijd 
    
    fwhm = simInfo.Parameters['Telescope']['fwhm']

    # Calculate lissajous constants
    if np.mod(a, 2) == 0:
        _b = a
        _a = a - 1
    else:   
        _b = a - 1
        _a = a

    # Calculate angular speed
    w = 1.
    az, el = CalcLissajous(t, jd, w, _a, _b, rlen, dlen, fwhm, rcen, dcen, lat, lon)
    vaz = np.max(np.abs(np.gradient(az)*simInfo.sampleRate))
    w = simInfo.Parameters['Telescope']['speed']/vaz/2.
    az, el = CalcLissajous(t, jd, w, _a, _b, rlen, dlen, fwhm, rcen, dcen, lat, lon)
    

    return az, el, jd

def WriteLissajous(Parameters, ijd, lon, lat):
    """
    Determines the best start julian date, given a source at az, el at time jd.

    Arguments:
    Parameters - Dictionary containing input simulation parameters
    az - azimuth of source
    el - elevation of source 
    jd - julian date of source

    Returns
    line - Dictionary of {'keyword': values, ...} needed for the inputs to HorizonRaster

    """
        
    ra = Parameters['Observations']['racen']
    dec = Parameters['Observations']['deccen']
    emin = Parameters['Observations']['elmin']
    emax = Parameters['Observations']['elmax']
    pmin = Parameters['Observations']['pmin']

    az, el, jd = Observations.BestAzElJD(ijd, ra, dec, emax, emin, pmin, lat, lon)

    obsLength = Parameters['Observations']['obsLen']

    outline = [['mode', Parameters['Observations']['mode']],
               ['observation_length', Parameters['Observations']['obsLen']],
               ['xcen',  Parameters['Observations']['racen']],
               ['ycen',  Parameters['Observations']['deccen']],
               ['rlen', Parameters['Observations']['radist']],
               ['dlen', Parameters['Observations']['decdist']],
               ['ijd', jd],
               ['a', Parameters['Observations']['a']]]
    

    return jd, obsLength, outline
