import numpy as np
from pipeline.Tools import General
from scipy import signal

from pipeline.Observatory.Telescope import Coordinates, Observations

def CalcCircular(t, jd, w, rmax, rmin, fwhm, xcen, ycen,delta, lon, lat):
    period = 1./w
    rrange = rmax-rmin
    tSlew = rrange/delta * period
    
    rv = (-signal.sawtooth(2.*np.pi*t/tSlew / 2., 0.5)+ 1.)/2.*rrange
    rv += rmin
    
    # phase
    phase = np.sin(2*np.pi*t/tSlew/2.)
    phase[phase > 0] = 1.
    phase[phase <= 0] = -1.
        
    dra  = rv*np.cos(2.*np.pi*t*w) 
    ddec = rv*np.sin(2.*np.pi*t*w) *phase

    # Calculate sky positions
    dec = ycen+ddec + np.random.normal(scale=fwhm/2.)
    ra = xcen+dra/np.cos(dec*np.pi/180.) + np.random.normal(scale=fwhm/2.)
    
    # Calculate az and el positions of the telescope
    az, el = Coordinates._equ2hor(ra, dec, jd, lat, lon)

    return az, el

def Circular(simInfo, startTime, endTime,  lon, lat, mode, obsLength, xcen, ycen, rmax, rmin, delta, ijd): 
    """
    Template example for building a scanning strategy module
    
    Notes:
    All functions must accept a simInfo object, startTime, and endTime of the observation in seconds, 
    the mode string of the observation, and then the other parameters that observing mode needs.
    
    Don't forget to add your function to __init__.py as from Template import *
    
    Arguments
    simInfo   - Contains all the simulation input parameters
    startTime - Relative start time of the observation in seconds
    endTime   - Relative end time of the observation in seconds
    mode      - String describing the name of the observing mode
    obsLength   - Length of the observation
    a         - Some parameter a
    b         - Some parameter b
    c         - Some parameter c
    
    Returns
    az - the azimuth of the telescope boresight in degrees
    el - the elevation of the telescope boresight in degrees
    jd - the julian date of the observation 
    
    """
    
    nSamples = General.SafeInt( (endTime - startTime))*int(simInfo.sampleRate)  # You want this to avoid floating point rounding errors


    t = np.linspace(startTime, endTime, nSamples)
    jd = t/24./3600. + ijd
    fwhm = simInfo.Parameters['Telescope']['fwhm']

    w = 1.
    az, el = CalcCircular(t, jd, w, rmax, rmin, fwhm, xcen, ycen,delta, lon, lat)
    vaz = np.max(np.abs(np.gradient(az)*simInfo.sampleRate))
    w = simInfo.Parameters['Telescope']['speed']/vaz
    az, el = CalcCircular(t, jd, w, rmax, rmin, fwhm, xcen, ycen,delta, lon, lat)
 
    return az, el, jd
    
    
def WriteCircular(Parameters, ijd, lon, lat):
    """
    Determines the best start julian date, given a source at az, el at time jd.

    Arguments:
    Parameters - Dictionary containing input simulation parameters
    az - azimuth of source
    el - elevation of source 
    jd - julian date of source

    Returns
    line - Dictionary of {'keyword': values, ...} needed for the inputs to HorizonRaster

    """
    ra = Parameters['Observations']['racen']
    dec = Parameters['Observations']['deccen']
    emin = Parameters['Observations']['elmin']
    emax = Parameters['Observations']['elmax']
    pmin = Parameters['Observations']['pmin']

    az, el, jd = Observations.BestAzElJD(ijd, ra, dec, emax, emin, pmin, lat, lon)

    obsLength = Parameters['Observations']['obsLen']

    outline = [['mode', Parameters['Observations']['mode']],
               ['observation_length', Parameters['Observations']['obsLen']],
               ['xcen',  Parameters['Observations']['racen']],
               ['ycen',  Parameters['Observations']['deccen']],
               ['rmax', Parameters['Observations']['rmax']],
               ['rmin', Parameters['Observations']['rmin']],
               ['delta', Parameters['Observations']['delta']],        
               ['ijd', jd]]
    

    return jd, obsLength, outline

