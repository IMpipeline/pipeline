from __future__ import absolute_import, division, print_function, unicode_literals
import numpy as np
from pipeline.Tools import General



def RASkyRaster(simInfo, startTime, endTime, mode, obsLength, xcen, ycen, dslew, dstep, ijd): 
    """
    Generates a sky raster, slewing back and forth in ra, stepping in declination for 
    a length of time obsLength.
    
    Notes:
    
    Arguments
    simInfo   - Contains all the simulation input parameters
    startTime - Relative start time of the observation in seconds
    endTime   - Relative end time of the observation in seconds
    mode      - String describing the name of the observing mode
    obsLength   - Length of the observation
    xcen        - Central ra
    ycen        - Central dec
    dslew       - Distance ra to slew in degrees
    dstep       - Distance dec to step in degrees

    Returns
    az - the azimuth of the telescope boresight in degrees
    el - the elevation of the telescope boresight in degrees
    jd - the julian date of the observation 
    
    """
    
    nSamples = General.SafeInt( (endTime - startTime))*int(simInfo.sampleRate)  # You want this to avoid floating point rounding errors

    # How many slews in the time available?
    tSlew = dslew/simInfo.Parameters['Telescope']['speed'] # x2 for forward and backward slew
    nSampsPerSlew = General.SafeInt(tSlew)*int(simInfo.sampleRate) 

    # How far will you get in dec?
    nSteps = obsLength/tSlew
    dDist = nSteps * dstep # Distance in declination


    t = np.linspace(startTime, endTime, nSamples)
    dra  = signal.sawtooth(2.*np.pi*t/tSlew/2., 0.5)*dslew/2. 

    dec = np.arange(int(nSteps+1))*dStep - dDist/2.
    dec = np.tile(dec, nSampsPerSlew) + ycen

    dec = dec[:nSamples]
    
    dec = ycen+ddec
    ra  = xcen + dra/np.cos(dec*np.pi/180.)


    jd = t/24./3600. + ijd
        
    az, el = Coordinates._hor2equ(ra,
                                  dec,
                                  jd,
                                  simInfo.Parameters['Telescope']['latitude'],
                                  simInfo.Parameters['Telescope']['longitude'])

    ####
    # PERFORM Calculations for the scan strategy
    ####
    
    return az, el, jd


def DecSkyRaster(simInfo, startTime, endTime, mode, obsLength, xcen, ycen, dslew, dstep, ijd): 
    """
    Generates a sky raster, slewing back and forth in ra, stepping in declination for 
    a length of time obsLength.
    
    Notes:
    
    Arguments
    simInfo   - Contains all the simulation input parameters
    startTime - Relative start time of the observation in seconds
    endTime   - Relative end time of the observation in seconds
    mode      - String describing the name of the observing mode
    obsLength   - Length of the observation
    xcen        - Central ra
    ycen        - Central dec
    dslew       - Distance dec to slew in degrees
    dstep       - Distance ra to step in degrees

    Returns
    az - the azimuth of the telescope boresight in degrees
    el - the elevation of the telescope boresight in degrees
    jd - the julian date of the observation 
    
    """
    
    nSamples = General.SafeInt( (endTime - startTime))*int(simInfo.sampleRate)  # You want this to avoid floating point rounding errors

    # How many slews in the time available?
    tSlew = dslew/simInfo.Parameters['Telescope']['speed'] # x2 for forward and backward slew
    nSampsPerSlew = General.SafeInt(tSlew)*int(simInfo.sampleRate) 

    # How far will you get in dec?
    nSteps = obsLength/tSlew
    dDist = nSteps * dstep # Distance in declination


    t = np.linspace(startTime, endTime, nSamples)
    dec  = signal.sawtooth(2.*np.pi*t/tSlew/2., 0.5)*dslew/2. + xcen

    dra = np.arange(int(nSteps+1))*dStep - dDist/2.
    dra = np.tile(dec, nSampsPerSlew) 

    dra = dra[:nSamples]
    ra  = xcen + dra/np.cos(dec*np.pi/180.)

    jd = t/24./3600. + ijd
        
    az, el = Coordinates._hor2equ(ra,
                                  dec,
                                  jd,
                                  simInfo.Parameters['Telescope']['latitude'],
                                  simInfo.Parameters['Telescope']['longitude'])

    ####
    # PERFORM Calculations for the scan strategy
    ####
    
    return az, el, jd
    
def WriteSkyRaster(Parameters, az, el, jd, lon, lat):
    """
    Determines the best start julian date, given a source at az, el at time jd.

    Arguments:
    Parameters - Dictionary containing input simulation parameters
    az - azimuth of source
    el - elevation of source 
    jd - julian date of source

    Returns
    line - Dictionary of {'keyword': values, ...} needed for the inputs to HorizonRaster

    """

    # How many steps to make a square?
    dslew = simInfo.Parameters['Observation']['dslew']
    dstep = simInfo.Parameters['Observation']['dstep']
    nsteps = int(dslew/dstep)
    tSlew = dslew/simInfo.Parameters['Telescope']['speed'] # x2 for forward and backward slew
    
    obsLength = np.floor(tSlew * nsteps)
    
    outline = [['mode', Parameters['Observations']['mode']],
               ['observation_length', obsLength],
               ['xcen', Parameters['Observations']['racen']],
               ['ycen', Parameters['Observations']['deccen']],
               ['dslew', dslew],
               ['dstep', dstep],
               ['ijd', jd]]
    

    return outline

