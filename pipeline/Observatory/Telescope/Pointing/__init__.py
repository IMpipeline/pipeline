from pipeline.Observatory.Telescope.Pointing.HorizonRaster import *
from pipeline.Observatory.Telescope.Pointing.Lissajous import *
from pipeline.Observatory.Telescope.Pointing.SkyRaster import *
from pipeline.Observatory.Telescope.Pointing.Drift import *
from pipeline.Observatory.Telescope.Pointing.Continuous import *
from pipeline.Observatory.Telescope.Pointing.Circular import *
from pipeline.Observatory.Telescope.Pointing.LissajousAzEl import *
from pipeline.Observatory.Telescope.Pointing.CircularAzEl import *

ObservationWriteFunctions = {'horizonraster': WriteHorizonRaster,
                             'lissajous': WriteLissajous,
                             'lissajousazel': WriteLissajousAzEl,
                             'circular': WriteCircular,
                             'circularazel': WriteCircularAzEl,
                             'continuous': WriteContinuous,
                             'drift':WriteDrift}
                            # 'raskyraster': WriteSkyRaster,
                            # 'raskyraster': WriteSkyRaster,
                            # 'drift': WriteDrift,
                            # 'continuous': WriteContinuous,
                            # 'circular': WriteCircular}
                             
