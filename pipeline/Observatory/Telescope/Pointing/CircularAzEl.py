import numpy as np
from pipeline.Tools import General
from scipy import signal

from pipeline.Observatory.Telescope import Coordinates, Observations


def CircularAzEl(simInfo, startTime, endTime,  lon, lat, mode, obsLength, xcen, ycen, r, ijd): 
    """
    Template example for building a scanning strategy module
    
    Notes:
    All functions must accept a simInfo object, startTime, and endTime of the observation in seconds, 
    the mode string of the observation, and then the other parameters that observing mode needs.
    
    Don't forget to add your function to __init__.py as from Template import *
    
    Arguments
    simInfo   - Contains all the simulation input parameters
    startTime - Relative start time of the observation in seconds
    endTime   - Relative end time of the observation in seconds
    mode      - String describing the name of the observing mode
    obsLength   - Length of the observation
    a         - Some parameter a
    b         - Some parameter b
    c         - Some parameter c
    
    Returns
    az - the azimuth of the telescope boresight in degrees
    el - the elevation of the telescope boresight in degrees
    jd - the julian date of the observation 
    
    """
    
    nSamples = General.SafeInt( (endTime - startTime))*int(simInfo.sampleRate)  # You want this to avoid floating point rounding errors


    t = np.linspace(startTime, endTime, nSamples)
    jd = t/24./3600. + ijd
    #fwhm = simInfo.Parameters['Beam']['fwhm']
    
    #w = 
    #dV_az = np.cos(2.*np.pi*t*w)*simInfo.Parameters['Telescope']['speed']
    #dV_el = np.sin(2.*np.pi*t*w)*simInfo.Parameters['Telescope']['speed']

    w = simInfo.Parameters['Observations']['wspeed']*np.pi/180. #/4./np.pi /r 
    dAz = np.cos(2.*np.pi*t*w) * r
    dEl = np.sin(2.*np.pi*t*w) * r

    el = dEl + ycen
    dec = simInfo.Parameters['Observations']['deccen']*np.pi/180.
    az = dAz/np.cos(el*np.pi/180.) + xcen
 
    return az, el, jd
    
def SkyBox(xcen, ycen, delta, lon, lat, jd):
    ires = 100
    dra = np.concatenate(( np.linspace(-delta/2., delta/2., ires),
                          np.linspace(-delta/2., delta/2., ires),
                          np.ones(ires)*-delta/2., 
                          np.ones(ires)* delta/2.))
    
    ddec = np.concatenate((np.ones(ires)*-delta/2., 
                          np.ones(ires)* delta/2.,
                          np.linspace(-delta/2., delta/2., ires),
                          np.linspace(-delta/2., delta/2., ires)))
    
    dec = ddec + ycen
    ra  = xcen + dra/np.cos(dec*np.pi/180.)
    azbox, elbox = Coordinates._equ2hor(ra, dec, np.ones(ra.size)*jd, lat, lon)

    return azbox, elbox
    
def OldWriteCircularAzEl(Parameters, ijd, lon, lat):
    """
    Determines the best start julian date, given a source at az, el at time jd.

    Arguments:
    Parameters - Dictionary containing input simulation parameters
    az - azimuth of source
    el - elevation of source 
    jd - julian date of source

    Returns
    line - Dictionary of {'keyword': values, ...} needed for the inputs to HorizonRaster

    """
    ra = Parameters['Observations']['racen']
    dec = Parameters['Observations']['deccen']
    emin = Parameters['Observations']['elmin']
    emax = Parameters['Observations']['elmax']
    pmin = Parameters['Observations']['pmin']


    # Estimate the box that we want to observe
    delta = Parameters['Observations']['rmax']*2. 
    ycen =  Parameters['Observations']['deccen']
    xcen =  Parameters['Observations']['racen']

    # Estimate observing time
    skyRotSpeed= 360./24./3600.*np.cos(Parameters['Observations']['deccen']*np.pi/180.)
    obsLength = np.floor(delta/skyRotSpeed)

    az, el, jd = Observations.BestAzElJD(ijd+obsLength/2./24./3600., ra, dec, emax, emin, pmin, lat, lon)

    # Convert to az and el and calculate slew length in azimuth
    # First calculate sky box at the start of the observation
    az1, el1 = SkyBox(xcen, ycen, delta, lon, lat, jd-obsLength/2./24./3600.)
    az2, el2 = SkyBox(xcen, ycen, delta, lon, lat, jd+obsLength/2./24./3600.)

    # Find the maximum span of azimuth between the two times.
    dslew = np.max([az1,az2]) - np.min([az1,az2])
        

    outline = [['mode', Parameters['Observations']['mode']],
               ['observation_length', obsLength],
               ['xcen', az],
               ['ycen', el],
               ['r', Parameters['Observations']['rmax']],
               ['ijd', jd-obsLength/2./24./3600.]]    

    return jd, obsLength, outline
    
def WriteCircularAzEl(Parameters, meanLon, meanLat):
    """
    Determines the best start julian date, given a source at az, el at time jd.

    Arguments:
    Parameters - Dictionary containing input simulation parameters
    az - azimuth of source
    el - elevation of source 
    jd - julian date of source

    Returns
    line - Dictionary of {'keyword': values, ...} needed for the inputs to HorizonRaster

    """
    mode   = Parameters['Observations']['mode']
    obsLen = Parameters['Observations']['obsLen']


    ra = Parameters['Observations']['racen']
    dec = Parameters['Observations']['deccen']
    rmax= Parameters['Observations']['rmax']
    fieldWidth = rmax*2
    ijd    = Parameters['Observations']['ijd']
    ejd    = Parameters['Observations']['ejd']

    emin = Parameters['Observations']['elmin']
    emax = Parameters['Observations']['elmax']
    hamin = Parameters['Observations']['hamin']/15.
        
        
    # Derive sky track (HA vs. elevation) for 1 day
    nsamps = 1000
    jd = np.linspace(0,1, nsamps) + ijd
    lst = Coordinates._jd2lst(meanLon, jd)
    ha = Coordinates._ra2ha(ra, lst)
    az, el = Coordinates._equ2hor(ra*np.ones(nsamps), dec*np.ones(nsamps), jd, meanLat, meanLon)
    pang = Coordinates._pang(el, dec*np.ones(nsamps), meanLat)
    # Which regions are above the minimum elevation?
    goodEl = (el > emin) & (el < emax) & (ha < 12) & (ha > hamin)

    # Range of hours
    maxHA = np.max(ha[goodEl])
    haRange = np.max(ha[goodEl])-np.min(ha[goodEl])
    siderealRate = 360./24.
    driftTime = fieldWidth/siderealRate/np.cos(dec*np.pi/180.) # in hours
    nScans = int(haRange/driftTime)

    # How many days?
    nDays = ejd - ijd
    if np.mod(nDays, 1) != 0:
        nDays = int(nDays)+1 # Always round up 
    else:
        nDays = int(nDays) # ...unless there is exactly an integer number of days
        
    fraction = 0.05
    f = open(Parameters['Observations']['configurationFile'],'w')
        
    lineone = ['mode', 'observation_length', 'xcen', 'ycen', 'r', 'ijd']
    for i in range(nDays):
        starts = np.mod(driftTime*(np.arange(nScans)+1) + i*fraction*driftTime , haRange) + np.min(ha[goodEl])
        starts = np.concatenate((starts, 24-starts))
        jdStarts = jd[np.argmin((ha[:,np.newaxis]-starts[np.newaxis,:])**2,axis=0)]
        jsort = np.argsort(jdStarts)
        jdStarts = np.sort(jdStarts)
        # check difference in start times is reasonable
        driftTimeJD=driftTime/24.
        for j in range(jdStarts.size-1):
            diff = jdStarts[j+1] - jdStarts[j]
            if diff < driftTimeJD:
                jdStarts[j+1:] += driftTimeJD - diff
                         
        if i == 0:
            kstring = [lineone[j]+' ' for j in range(len(lineone))] + ['\n']
            f.write(''.join(kstring))
                    
        for j in range(jdStarts.size):
            xcen,ycen = Coordinates._equ2hor(ra*np.ones(1), dec*np.ones(1), jdStarts[j:j+1], meanLat, meanLon)
            scanLength = fieldWidth/np.cos(ycen[0]*np.pi/180.)
            lineout = [int(driftTime*3600.), xcen[0], ycen[0], rmax, jdStarts[j]- driftTime/2./24.]
            vstring = [mode+' '] + [str(lineout[j])+' ' for j in  range(len(lineout))] + ['\n']
            f.write(''.join(vstring))
    
