from __future__ import absolute_import, division, print_function, unicode_literals
import numpy as np
from pipeline.Tools import General
from scipy import signal
from scipy.interpolate import interp1d

from pipeline.Observatory.Telescope import Coordinates, Observations

def _RasterObsTime(_dist,_speed,_accel, sampleRate):
    accelTime = _speed/_accel  #How long to accel (initial speed is zero)
    accelDist = _accel*accelTime**2/2. #Distance covered during acceleration

    driftDist = _dist - 2.* accelDist
    if driftDist > 0: #If driftDist is positive, max speed is reached
        driftTime = driftDist / _speed
        slewTime = accelTime*2. + driftTime
        drift = True
    else:
        slewTime = np.sqrt(_dist/_accel) * 2.
        drift = False

    return np.floor(slewTime*sampleRate)/sampleRate, drift

def HorizonAccelerationCurve(_dist,_speed,_accel,_sampRate):
    """
    Name: HorizonAccerlationCurve
    
    Description: Returns a relative angular position slewing template for horizon observations
    """

    accelTime = _speed/_accel  #How long to accel (initial speed is zero)
    accelDist = _accel*accelTime**2/2. #Distance covered during acceleration

    if accelDist > _dist/2.:
        driftDist = 0
    else:
        driftDist = _dist - 2.* accelDist
    driftTime = driftDist/_speed
    
    totalTime = accelTime*2 + driftTime
    aTime = np.linspace(0, accelTime, accelTime*_sampRate)
    dTime = np.linspace(0, driftTime, driftTime*_sampRate)

    speed = [_accel * aTime, _speed*np.ones(dTime.size), _speed - _accel*aTime]
    islew = np.cumsum(np.concatenate(speed))/_sampRate
    
    islew -= islew[islew.size//2]

    islew = np.concatenate((islew,islew[::-1]))
    slewTimes = np.arange(islew.size).astype('float')/_sampRate
    slewModel = interp1d(slewTimes, islew )


    return slewModel, np.max(slewTimes)

def HorizonRaster(simInfo, startTime, endTime,  lon, lat, mode, obsLength,xcen, ycen, dslew, ijd): 
    """
    Horizon raster calculates assuming slewing in azimuth at a fixed elevation
     
    Notes:
    Assumes no acceleration
    
    Arguments
    simInfo   - Contains all the simulation input parameters
    startTime - Relative start time of the observation in seconds
    endTime   - Relative end time of the observation in seconds
    mode      - String describing the name of the observing mode
    obsLength   - Length of the observation
    xcen        - Central azimuth
    ycen        - Central elevation
    dslew       - Distance azimuth slew in degrees
    
    Returns
    az - the azimuth of the telescope boresight in degrees
    el - the elevation of the telescope boresight in degrees
    jd - the julian date of the observation 
    
    """
    nSamples = General.SafeInt( (endTime - startTime))*int(simInfo.sampleRate)  # You want this to avoid floating point rounding errors
        
    speed = simInfo.Parameters['Telescope']['speed']
    accel = simInfo.Parameters['Telescope']['acceleration']
    sampleRate = int(simInfo.sampleRate)
    slewModel, slewTime = HorizonAccelerationCurve(dslew,speed,accel,sampleRate)

    t = np.linspace(startTime, endTime, nSamples)
    jd = t/24./3600. + ijd
    el = np.ones(nSamples)*ycen
    az = slewModel(np.mod(t,slewTime))  + xcen
    

    # How many slews in the time available?
    #tSlew = dslew/simInfo.Parameters['Telescope']['speed']*2 # x2 for forward and backward slew    
    #t = np.linspace(startTime, endTime, nSamples)
    #az2 = signal.sawtooth(2.*np.pi*t/tSlew, 0.5)*dslew/2.# + xcen
    #el2 = np.ones(nSamples)*ycen
    #jd = t/24./3600. + ijd

    #print slewTime/2., tSlew
    #from matplotlib import pyplot
    #pyplot.plot(az)
    #pyplot.plot(az2)
    #pyplot.show()
    
    return az, el, jd

def SkyBox(xcen, ycen, delta, lon, lat, jd):
    ires = 100
    dra = np.concatenate(( np.linspace(-delta/2., delta/2., ires),
                          np.linspace(-delta/2., delta/2., ires),
                          np.ones(ires)*-delta/2., 
                          np.ones(ires)* delta/2.))
    
    ddec = np.concatenate((np.ones(ires)*-delta/2., 
                          np.ones(ires)* delta/2.,
                          np.linspace(-delta/2., delta/2., ires),
                          np.linspace(-delta/2., delta/2., ires)))
    
    dec = ddec + ycen
    ra  = xcen + dra/np.cos(dec*np.pi/180.)
    azbox, elbox = Coordinates._equ2hor(ra, dec, np.ones(ra.size)*jd, lat, lon)
    
    return azbox, elbox

def SkyBoxRA(xcen, ycen, delta, lon, lat, jd):
    ires = 100
    dra = np.concatenate(( np.linspace(-delta/2., delta/2., ires),
                          np.linspace(-delta/2., delta/2., ires)))
    
    ddec = np.concatenate((np.ones(ires)*-delta/2., 
                          np.ones(ires)* delta/2.))
    
    dec = ddec + ycen
    ra  = xcen + dra/np.cos(dec*np.pi/180.)
    azbox, elbox = Coordinates._equ2hor(ra, dec, np.ones(ra.size)*jd, lat, lon)
    
    return azbox, elbox


def OldWriteHorizonRaster(Parameters, ijd, lon, lat):
    """
    Determines the best start julian date, given a source at az, el at time jd.

    Arguments:
    Parameters - Dictionary containing input simulation parameters
    az - azimuth of source
    el - elevation of source 
    jd - julian date of source

    Returns
    line - Dictionary of {'keyword': values, ...} needed for the inputs to HorizonRaster

    """
    ra = Parameters['Observations']['racen']
    dec = Parameters['Observations']['deccen']
    emin = Parameters['Observations']['elmin']
    emax = Parameters['Observations']['elmax']
    pmin = Parameters['Observations']['pmin']
    overhead = Parameters['Observations']['overhead']/24./3600.

    # Estimate the box that we want to observe
    delta = Parameters['Observations']['dslew']
    ycen =  Parameters['Observations']['deccen']
    xcen =  Parameters['Observations']['racen']

    # Estimate observing time
    skyRotSpeed= 360./24./3600.*np.cos(Parameters['Observations']['deccen']*np.pi/180.)
    obsLength = np.floor(delta/skyRotSpeed)

    az, el, jd = Observations.BestAzElJD(ijd+obsLength/2./24./3600., ra, dec, emax, emin, pmin, lat, lon)
    


    # Convert to az and el and calculate slew length in azimuth
    # First calculate sky box at the start of the observation
    az1, el1 = SkyBox(xcen, ycen, delta, lon, lat, jd-obsLength/2./24./3600.)
    az2, el2 = SkyBox(xcen, ycen, delta, lon, lat, jd+obsLength/2./24./3600.)
 
    # Find the maximum span of azimuth between the two times.
    dslew = np.max([az1,az2]) - np.min([az1,az2])
        

    outline = [['mode', Parameters['Observations']['mode']],
               ['observation_length', obsLength],
               ['xcen', az],
               ['ycen', el],
               ['dslew', dslew],
               ['ijd', jd-obsLength/2./24./3600.]]
    

    return jd-obsLength/2./24./3600., obsLength, outline
    
def WriteHorizonRaster(Parameters, meanLon, meanLat):
    """
    Determines the best start julian date, given a source at az, el at time jd.

    Arguments:
    Parameters - Dictionary containing input simulation parameters
    az - azimuth of source
    el - elevation of source 
    jd - julian date of source

    Returns
    line - Dictionary of {'keyword': values, ...} needed for the inputs to HorizonRaster

    """
    mode   = Parameters['Observations']['mode']
    obsLen = Parameters['Observations']['obsLen']


    ra = Parameters['Observations']['racen']
    dec = Parameters['Observations']['deccen']
    fieldWidth = Parameters['Observations']['dslew']
    fieldLength = Parameters['Observations']['ddrift'] # how many degrees in RA to drift    
    ijd    = Parameters['Observations']['ijd']
    ejd    = Parameters['Observations']['ejd']

    emin = Parameters['Observations']['elmin']
    emax = Parameters['Observations']['elmax']
    hamin = Parameters['Observations']['hamin']/15.
        
        
    # Derive sky track (HA vs. elevation) for 1 day
    def getInfo(ijd):
        nsamps = 1000
        jd = np.linspace(0,1, nsamps) + ijd
        lst = Coordinates._jd2lst(meanLon, jd)
        ha = Coordinates._ra2ha(ra, lst)
        az, el = Coordinates._equ2hor(ra*np.ones(nsamps), dec*np.ones(nsamps), jd, meanLat, meanLon)
        pang = Coordinates._pang(el, dec*np.ones(nsamps), meanLat)
        # Which regions are above the minimum elevation?
        goodEl = (el > emin) & (el < emax) & (ha < 12) & (ha > hamin)

        # Range of hours
        maxHA = np.max(ha[goodEl])
        haRange = np.max(ha[goodEl])-np.min(ha[goodEl])
        siderealRate = 360./24.
        driftTime = fieldLength/siderealRate / np.cos(dec*np.pi/180.) # in hours
        # fieldWidth/siderealRate/np.cos(dec*np.pi/180.) # in hours
        nScans = int(haRange/driftTime)
        return jd, lst, az, el, ha, pang, goodEl, maxHA, haRange, driftTime, nScans
        
    # How many days?
    nDays = ejd - ijd
    if np.mod(nDays, 1) != 0:
        nDays = int(nDays)+1 # Always round up 
    else:
        nDays = int(nDays) # ...unless there is exactly an integer number of days
        
    fraction = 0.05
    f = open(Parameters['Observations']['configurationFile'],'w')
        
    lineone = ['mode', 'observation_length', 'xcen', 'ycen', 'dslew', 'ijd']
    for i in range(nDays):
        jd, lst, az, el, ha, pang, goodEl, maxHA, haRange, driftTime, nScans = getInfo(ijd)
        starts = np.mod(driftTime*(np.arange(nScans)+1) + i*fraction*driftTime , haRange) + np.min(ha[goodEl])
        starts = np.concatenate((starts, 24-starts))
        jdStarts = jd[np.argmin((ha[:,np.newaxis]-starts[np.newaxis,:])**2,axis=0)]
        jsort = np.argsort(jdStarts)
        jdStarts = np.sort(jdStarts)
        # check difference in start times is reasonable
        driftTimeJD=driftTime/24.
        for j in range(jdStarts.size-1):
            diff = jdStarts[j+1] - jdStarts[j]
            if diff < driftTimeJD:
                jdStarts[j+1:] += driftTimeJD - diff
            
        ijd = jdStarts[-1] + driftTime   
        
        if i == 0:
            kstring = [lineone[j]+' ' for j in range(len(lineone))] + ['\n']
            f.write(''.join(kstring))
                    
        for j in range(jdStarts.size):
            if jdStarts[j] > ejd:
                break
            xcen,ycen = Coordinates._equ2hor(ra*np.ones(1), dec*np.ones(1), jdStarts[j:j+1], meanLat, meanLon)
            scanLength = fieldWidth/np.cos(ycen[0]*np.pi/180.)
            lineout = [int(driftTime*3600.), xcen[0], ycen[0], scanLength, jdStarts[j]- driftTime/2./24.]
            vstring = ['HorizonRaster '] + [str(lineout[j])+' ' for j in  range(len(lineout))] + ['\n']
            f.write(''.join(vstring))
        if jdStarts[j] > ejd:
            break

#    outline = [['mode', Parameters['Observations']['mode']],
#               ['observation_length', obsLength],
#               ['xcen', az],
#               ['ycen', el],
#               ['dslew', dslew],
#               ['ijd', jd-obsLength/2./24./3600.]]
#    

#    return jd-obsLength/2./24./3600., obsLength, outline

