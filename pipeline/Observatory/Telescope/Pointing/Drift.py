import numpy as np
from pipeline.Tools import General

def Drift(simInfo, startTime, endTime,  lon, lat, mode, obsLength,xcen, ycen, ijd): 
    """
    Template example for building a scanning strategy module
    
    Notes:
    All functions must accept a simInfo object, startTime, and endTime of the observation in seconds, 
    the mode string of the observation, and then the other parameters that observing mode needs.
    
    Don't forget to add your function to __init__.py as from Template import *
    
    Arguments
    simInfo   - Contains all the simulation input parameters
    startTime - Relative start time of the observation in seconds
    endTime   - Relative end time of the observation in seconds
    mode      - String describing the name of the observing mode
    obsLength   - Length of the observation
    xcen        - Central azimuth
    ycen        - Central elevation
    ijd         - Initial julian date
    
    Returns
    az - the azimuth of the telescope boresight in degrees
    el - the elevation of the telescope boresight in degrees
    jd - the julian date of the observation 
    
    """
    
    nSamples = General.SafeInt( (endTime - startTime)*simInfo.sampleRate)  # You want this to avoid floating point rounding errors

        
    
    ####
    # PERFORM Calculations for the scan strategy
    ####
    
    az = np.zeros(nSamples) + xcen
    el = np.zeros(nSamples) + ycen
    jd = np.linspace(startTime,endTime, nSamples)/24./3600. + ijd


    return az, el, jd


def OldWriteDrift(Parameters, ijd, lon, lat):
    """
    Determines the best start julian date, given a source at az, el at time jd.

    Arguments:
    Parameters - Dictionary containing input simulation parameters
    az - azimuth of source
    el - elevation of source 
    jd - julian date of source

    Returns
    line - Dictionary of {'keyword': values, ...} needed for the inputs to HorizonRaster

    """
        
    obslen = Parameters['Observations']['obsLen']
    outline = [['mode', Parameters['Observations']['mode']],
               ['observation_length', obslen],
               ['xcen', Parameters['Observations']['racen']],
               ['ycen', Parameters['Observations']['deccen']],
               ['ijd', ijd ]]
    

    return ijd, Parameters['Observations']['obsLen'],  outline

def WriteDrift(Parameters, meanLon, meanLat):
    """
    Determines the best start julian date, given a source at az, el at time jd.

    Arguments:
    Parameters - Dictionary containing input simulation parameters
    az - azimuth of source
    el - elevation of source 
    jd - julian date of source

    Returns
    line - Dictionary of {'keyword': values, ...} needed for the inputs to HorizonRaster

    """
    mode   = Parameters['Observations']['mode']
    obsLen = Parameters['Observations']['obsLen']
    ra = Parameters['Observations']['racen']
    dec = Parameters['Observations']['deccen']
    ijd    = Parameters['Observations']['ijd']
    ejd    = Parameters['Observations']['ejd']

    nObs = (ejd - ijd)/obsLen
    if np.mod(nObs, 1) != 0:
        nObs = int(nObs)+1 # Always round up 
    else:
        nObs = int(nObs) # ...unless there is exactly an integer number of days
            
    f = open(Parameters['Observations']['configurationFile'],'w')

    for i in range(nObs):
        iStart = ijd + i*obsLen
        if iStart > ejd:
            break
            
        if (iStart + obsLen) >= ejd:
            obsLen = (ejd - iStart)*3600.*24
        if i == 0:
            lineone = ['mode', 'observation_length', 'xcen', 'ycen', 'ijd']
            kstring = [lineone[j]+' ' for j in range(len(lineone))] + ['\n']
            f.write(''.join(kstring))
            
        lineout = [int(obsLen), ra, dec, iStart]
        vstring = ['Drift '] + [str(lineout[j])+' ' for j in  range(len(lineout))] + ['\n']
        f.write(''.join(vstring))
