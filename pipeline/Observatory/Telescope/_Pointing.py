"""

Name: Pointing.py

Description: Handles pointing calculations. Contains all the different observing mode functions... Maybe split these off for ease of reading?

"""
import numpy as np
from matplotlib import pyplot
from pipeline.Tools.Container import *
from mpi4py import MPI
import sys
import copy
from pipeline.Observatory.Telescope import Coordinates
from scipy.interpolate import interp1d


####################################
# GENERAL POINTING FUNCTIONS BELOW #
####################################

def _RasterObsTime(_dist,_speed,_accel, sampleRate):
    accelTime = _speed/_accel  #How long to accel (initial speed is zero)
    accelDist = _accel*accelTime**2/2. #Distance covered during acceleration

    driftDist = _dist - 2.* accelDist
    if driftDist > 0: #If driftDist is positive, max speed is reached
        driftTime = driftDist / _speed
        slewTime = accelTime*2. + driftTime
        drift = True
    else:
        slewTime = np.sqrt(_dist/_accel) * 2.
        drift = False

    return np.floor(slewTime*sampleRate)/sampleRate, drift

def _RasterObsTimeFull(_dist,_speed,_accel, nslews, nsteps, sampleRate):

    slewTime, drift = _RasterObsTime(_dist,_speed,_accel, sampleRate)
    #print 'RASTER OBS TIME FULL:', slewTime, nslews, nsteps
    return slewTime*nslews*nsteps


def HorizonAccelerationCurve(_dist,_speed,_accel,_sampRate):
    """
    Name: HorizonAccerlationCurve
    
    Description: Returns a relative angular position slewing template for horizon observations
    """

    accelTime = _speed/_accel  #How long to accel (initial speed is zero)
    accelDist = _accel*accelTime**2/2. #Distance covered during acceleration

    driftDist = _dist - 2.* accelDist

    slewTime, drift = _RasterObsTime(_dist, _speed, _accel, _sampRate)

    if drift: #If driftDist is positive, max speed is reached
        decelTime = slewTime - accelTime

        nSamples  = int(slewTime*_sampRate)
        #Where does Acceleration/Deceleration start in terms of samples?
        accelTimeSample = int(_sampRate * accelTime)#int(_sampRate * accelTime + 0.5) #Round to nearest integer
        decelTimeSample = nSamples - accelTimeSample#int(_sampRate * decelTime + 0.5)
        slewDeltaT = np.linspace(0, slewTime, nSamples)

        #Find the initial conditions of drift
        driftStart_x = -_dist/2. + _accel*accelTime**2/2.
        driftStart_v =  _accel*accelTime
    
        #Initial conditions of decel
        decelStart_x = driftStart_x + driftStart_v*(decelTime - accelTime)
        decelStart_v = driftStart_v

        islew = np.zeros(nSamples)
        islew[0:accelTimeSample]               = -_dist/2. + _accel*slewDeltaT[0:accelTimeSample]**2/2.
        islew[accelTimeSample:decelTimeSample] = driftStart_x + driftStart_v*(slewDeltaT[accelTimeSample:decelTimeSample]-accelTime)
        islew[decelTimeSample:]                 = decelStart_x + decelStart_v*(slewDeltaT[decelTimeSample:]-decelTime) - _accel*(slewDeltaT[decelTimeSample:]-decelTime)**2/2.


    else:
        midTime     = np.sqrt(_dist/_accel)
        slewTime    = midTime*2.
        nSamples    = slewTime*_sampRate
        slewDeltaT  = np.linspace(0,slewTime,nSamples)

        midSample    = int(midTime*_sampRate+0.5)
        decelStart_x = -_dist/2. + _accel*midTime**2/2.
        decelStart_v = _accel*midTime        
        
        islew = np.concatenate((-_dist/2. + _accel*slewDeltaT[0:midSample]**2/2.,
                                 decelStart_x + decelStart_v*(slewDeltaT[midSample:]-midTime) - _accel*(slewDeltaT[midSample:]-midTime)**2/2.))

        
    
    return islew

def HorizonRaster(sampleRate, maxspeed, acceleration, xcen, ycen, dslew, nslews, dstep, nsteps, nsamps, tstart, tend):
    """
    Name: HorizonRaster
    
    Description: Generates the RA/Dec coordinates for a horizon raster scan observation.

    """

    #Here we should try to estimate nSamples.
    slewTemplate = HorizonAccelerationCurve(dslew,
                                            maxspeed,
                                            acceleration,
                                            sampleRate)
    singleSlewSamps = slewTemplate.size
    slewTemplate = np.concatenate((slewTemplate[1:], -slewTemplate[:-1]))
    slewSamps = slewTemplate.size                                 
    slewTimes = np.arange(-slewSamps, 2*slewSamps)
    slewModel = interp1d(slewTimes, np.concatenate((slewTemplate, slewTemplate, slewTemplate)))
    
    # Generate the step template
    stepTemplate = np.linspace(-dstep*int(nsteps/2.), dstep*int(nsteps/2.), nsteps )


    # obs Time
    #nsamps = (tend - tstart)*sampleRate
    tsamps = np.arange(tstart*sampleRate, tend*sampleRate)

    

    ypos =  (tsamps/int(singleSlewSamps*nslews)).astype('i')
    ypos[ypos > nsteps-1] = nsteps-1
    xpoints = slewModel(np.mod(tsamps, slewSamps)) + xcen

    ypoints = stepTemplate[ypos] + ycen
    

    return xpoints, ypoints
    

def SkyRaster(Obs,telescope):
    """
    Name: SkyRaster
    
    Description: Generates the RA/Dec coordinates for a sky raster scan observation.

    """

    #Calculate the number of samples based on the maxSlew speed of telescope
    nSamplesPerScan = Obs['dslew']/telescope['maxSpeed'] * telescope['sampleRate']

    xpoints = np.concatenate((np.linspace(-Obs['dslew']/2.,Obs['dslew']/2.,nSamplesPerScan),np.linspace(Obs['dslew']/2.,-Obs['dslew']/2.,nSamplesPerScan) ))
    xpoints = np.tile(xpoints,Obs['nslews']*Obs['nsteps']/2.)
    if np.mod(Obs['nslews']*Obs['nsteps'],2) != 0:
        xpoints = np.concatenate((xpoints,np.linspace(-Obs['dslew']/2.,Obs['dslew']/2.,nSamplesPerScan) ))

    #Vectorised ypoint generation
    if Obs['nsteps'] > 1:
        ypoints = np.repeat(np.linspace(-Obs['nsteps']*Obs['dstep']/2.,Obs['nsteps']*Obs['dstep']/2.,Obs['nsteps']),nSamplesPerScan)
    else:
        ypoints = np.zeros(nSamplesPerScan*Obs['nslews'])

    

    if 'RA' in Obs['Mode']:
        xpoints += Obs['xcen']
        ypoints += Obs['ycen']

        #Convert to Sky-Coordinates
        jd = Obs['ijd'] + np.arange(xpoints.size)/telescope['sampleRate'] /3600./24.
        lst = Coordinates._gst2lst(Coordinates._jd2gst(jd),telescope['longitude'])
        xpoints,ypoints = Coordinates._equ2hor(xpoints,ypoints,telescope['latitude'],lst)

    else: #Declination scan mode
        xpoints += Obs['ycen']
        ypoints += Obs['xcen']

        #Convert to Sky-Coordinates
        jd = Obs['ijd'] + np.arange(xpoints.size)/telescope['sampleRate'] /3600./24.
        lst = Coordinates._gst2lst(Coordinates._jd2gst(jd),telescope['longitude'])
        xpoints,ypoints = Coordinates._equ2hor(ypoints,xpoints,telescope['latitude'],lst)

    return xpoints,ypoints,jd


def Circular(sampleRate, obsLength, xcen, ycen, radius, scanSpeed, geolon, geolat, ijd):
    """
    Circular

    Generates a circular scanning strategy centred on a given ra/dec/
    """

    nsamps = sampleRate*obsLength

    # time stamps of the TOD
    time = np.linspace(0, obsLength, nsamps)

    
    # Calculate the rotation angles
    theta = np.mod(time*scanSpeed, 360.)*np.pi/180.
    
    dec = np.ones(nsamps)*ycen
    ra  = np.ones(nsamps)*xcen

    jd = ijd + time/3600./24.
    lst= Coordinates._gst2lst(Coordinates._jd2gst(jd),geolon)

    # Track the central coordinate system
    acen, ecen = Coordinates._equ2hor(ra, dec, geolat, lst)

    # Make az/el rotate around central coordinate
    el = radius*np.cos(theta) + ecen
    az = radius*np.sin(theta)/np.cos(el*np.pi/180.) + acen

    return az, el, jd

def Drift(sampleRate, nSamples, xcen, ycen):
    """
    Name: Pointing
    
    Description: Telescope is fixed in a single horizon pointing position.

    """

    #Calculate the number of samples based on the maxSlew speed of telescope
    xpoints = np.zeros(nSamples) + xcen
    ypoints = np.zeros(nSamples) + ycen

    return xpoints, ypoints
    
def Continuous(sampleRate, speed, timeStart, timeEnd, nSamples, obsLength, xcen, ycen):
    """
    Name: Continous
    
    Description: Telescope in fixed continuous rotation

    """
    
    timeModel = np.linspace(-obsLength, 2*obsLength, nSamples*3)*sampleRate
    azModel   = np.mod(np.linspace(-obsLength*speed, 2.*obsLength*speed, 3.*nSamples), 360)
    
    
    
    #mdl = interp1d(timeModel, azModel, bounds_error=False)

    #t = np.arange(nSamples)/sampleRate*speed

    #Calculate the number of samples based on the maxSlew speed of telescope
    #inputTime = np.linspace(timeStart, timeEnd, nSamples)*sampleRate
    nStart = timeStart*sampleRate
    xpoints = np.mod(np.arange(nStart,nStart+nSamples)/sampleRate*speed, 360)#np.mod(mdl(inputTime) + xcen, 360)
    ypoints = np.zeros(nSamples) + ycen
        
    return xpoints, ypoints

    


