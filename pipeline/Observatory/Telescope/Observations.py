from __future__ import absolute_import, division, print_function, unicode_literals

import numpy as np
import jdcal
from pipeline.Observatory.Telescope import _Pointing as Pointing
from pipeline.Observatory.Telescope import Coordinates
import sys

def TransitPath(jd0, ra, dec, lat, lon):
    nres = 360*2 # accurate to a 0.5 degree
    ra = np.ones(nres)*ra
    dec= np.ones(nres)*dec
    jd= np.linspace(jd0, jd0+1, nres)

    #offset = 0.
    # JD = StartTime + N observations + offset in time for source to be above horizon
    az, el = Coordinates._equ2hor(ra,
                                  dec,
                                  jd,
                                  lat,
                                  lon)
    parangle = Coordinates._pang(el, dec, lat)

    return az, el, jd, parangle

def BestAzElJD(jd0, ra, dec, emax, emin, pmin, lat, lon):
    """
    Given a source at ra and dec, and the conditions that emax < El <
    emin and |parallactic| > pmin, and a fudicial start time of jd0,
    what is the best start time? And what is the az and el of the
    source at this time?
    """

    az, el, jd, parangle = TransitPath(jd0, ra, dec, lat, lon)

    search = ((el > emin) & (el < emax)) & (np.abs(parangle) >= pmin)
    if np.sum(search) == 0:
        print('SOURCE NEVER ABOVE HORIZON EXITING')
        sys.exit()
    jd  = jd[search]
    el  = el[search]
    az  = az[search]
    jdindex  = np.argmin(jd)
    minjd    = jd[jdindex]

    bestjd = jd[jdindex]
    bestaz = az[jdindex]
    bestel = el[jdindex]

    return bestaz, bestel, bestjd

def Time2JD(time):
    """
    Convert a time string of the format year:month:day:hr:min:ss to JD
    """
    obsTimeStamp = np.array(time.split(':')).astype('float')
    fracDay = (obsTimeStamp[3] + obsTimeStamp[4]/60. + obsTimeStamp[5]/60.**2)/24.
    jdvals  = jdcal.gcal2jd(obsTimeStamp[0],obsTimeStamp[1],obsTimeStamp[2])

    return jdvals[0]+jdvals[1]+fracDay

def ObservationLength(mode, nsteps, dslew, nslew, observation_length, speed, accel, sampleRate):
    """
    Take parameters and return a dictionary for this specific observation.
    
    Returns observation length in seconds
    """
    
    # Read in the data necesscary for raster observations
    if (mode.lower() == 'raster') | (mode.lower() == 'skyraster'):
        #Estimate the length of this observation
        return Pointing._RasterObsTimeFull(dslew, 
                                           speed, 
                                           accel,
                                           nslew,
                                           nsteps,
                                           sampleRate)

    # Data needed for a drift scan observations
    if mode.lower() == 'drift':
        return observation_length

    # Data needed for a drift scan observations
    if mode.lower() == 'circular':
        return observation_length
    
    if mode.lower() == 'continuous':
        return observation_length

    return None

def FocalPlaneOffsets(iaz, iel, focalLength, xoff, yoff, mode = 'feedoffsets'):
    """
    
    Input for az/el in radians. focalLength/xoff/yoff need to have consistent units.
    
    Need to add a check that confirms that the distance squared is smaller than the focallength
    """
    if mode.lower() == 'feedoffsets'.lower():
        if (np.abs(xoff) > 1e-3) | (np.abs(yoff) > 1e-3):
            dist = np.sqrt(xoff**2 + yoff**2)
            if dist > focalLength:
                raise('WARNING FOCAL PLANE POSITION OFFSET GREATER THAN FOCAL LENGTH - Check Receiver positions + Parameter  file')
            r  = np.arctan( dist/focalLength)
            t  = np.arctan2(yoff, xoff) # angular offset of horn
            daz = np.sin(t)*np.sin(r)/( np.cos(r)*np.cos(iel) + np.sin(iel)*np.sin(r)*np.cos(t))
        
            if np.max(np.abs(daz)) > 1e-5:
                az  = np.mod(iaz + daz, 2.*np.pi)
                el  = np.pi/2. - np.mod(np.pi/2. - np.arccos(np.sin(t)*np.sin(r)/np.sin(daz)), np.pi)
                de  = np.arccos(np.sin(t)*np.sin(r)/np.sin(daz)) - iel
            else:
                el  = iel - r
                az  = iaz
        else:
            el = iel
            az = iaz
    elif mode.lower() == 'skyoffsets':
        az = iaz + xoff*np.pi/180.
        el = iel + yoff*np.pi/180.
    else:
        el = iel
        az = iaz
        

    return az, el
    
