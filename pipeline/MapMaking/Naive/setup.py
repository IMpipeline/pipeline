#call with: python setup.py build_ext --inplace

import numpy as np
from distutils.core import setup
from distutils.extension import Extension
from Cython.Build import cythonize
from Cython.Distutils import build_ext

setup(
    name = 'cbinning',
    ext_modules = [Extension('cbinning', 
                             ['cbinning.pyx'], 
                             include_dirs=[np.get_include()])],
    cmdclass = {'build_ext': build_ext}
)

