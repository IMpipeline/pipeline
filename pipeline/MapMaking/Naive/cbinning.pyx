cdef extern from "stdlib.h":
    double drand48()
    void srand48(long int seedval)

cdef extern from "time.h":
    long int time(int)

cdef extern from "math.h":
    double M_PI
    double sin(double x)
    double cos(double x)
    double log(double x)
    double sqrt(double x)

#import pyximport
#pyximport.install(pyimport = True)

import numpy as np
cimport numpy as np

DTYPE = np.float
ctypedef np.float_t DTYPE_t

ITYPE = np.int
ctypedef np.int_t ITYPE_t



cimport cython

@cython.boundscheck(False)
@cython.wraparound(False)
def bin_pix_nw_hits(DTYPE_t[:,:] x,
                    ITYPE_t[:] pix,
                    DTYPE_t[:,:] sw,
                    DTYPE_t[:,:] hw,
                    DTYPE_t[:] hits):
    # Make sure data are floats
    #assert x.dtype     == DTYPE
    #assert sw.dtype    == DTYPE
    #assert hw.dtype    == DTYPE

    # Indexes/hits are integers
    #assert pix.dtype   == ITYPE
    #assert hits.dtype  == ITYPE

    cdef int freqs = x.shape[0]
    cdef int nsamples = x.shape[1]
    cdef int pixels = sw.shape[1]
    cdef int i, v

    for v in range(freqs):
        for i in range(nsamples):
            if (pix[i] >= 0) and (pix[i] < pixels):

                sw[v, pix[i]] += x[v, i]
                hw[v, pix[i]] += 1.
                if (v == 0):
                    hits[pix[i]] +=  1.0


    #return sw, hw, hits

@cython.boundscheck(False)
@cython.wraparound(False)
def bin_pix_mask_hits(DTYPE_t[:,:] x,
                    ITYPE_t[:] pix,
		    ITYPE_t[:] mask,
                    DTYPE_t[:,:] sw,
                    DTYPE_t[:,:] hw,
                    DTYPE_t[:] hits):
    # Make sure data are floats
    #assert x.dtype     == DTYPE
    #assert sw.dtype    == DTYPE
    #assert hw.dtype    == DTYPE

    # Indexes/hits are integers
    #assert pix.dtype   == ITYPE
    #assert hits.dtype  == ITYPE

    cdef int freqs = x.shape[0]
    cdef int nsamples = x.shape[1]
    cdef int pixels = sw.shape[1]
    cdef int i, v

    for v in range(freqs):
        for i in range(nsamples):
            if (pix[i] >= 0) and (pix[i] < pixels) and (mask[i] != 0):

                sw[v, pix[i]] += x[v, i]
                hw[v, pix[i]] += 1.
                if (v == 0):
                    hits[pix[i]] +=  1.0


    #return sw, hw, hits



def median_filter(DTYPE_t[:,:] x,
                  ITYPE_t window):

    cdef int freqs = x.shape[0]
    cdef int nsamples = x.shape[1]
    cdef int i, j, v
    
    cdef int nsteps
    cdef DTYPE_t med

    nsteps = nsamples/window

    for i in range(nsteps):
        med = np.median(x[:,window*i:window*(i+1)])
        for j in range(window*i, window*(i+1)):
            for v in range(freqs):
                med = np.median(x[v,window*i:window*(i+1)])
                x[v,j] -= med
        

    if nsteps*window < nsamples:
        for j in range(window*nsteps, nsamples):
            for v in range(freqs):
                med = np.median(x[v,window*nsteps:])
                x[v,j] -= med

def get_median_filter(DTYPE_t[:,:] x,
                      DTYPE_t[:,:] m,
                      ITYPE_t window):

    cdef int freqs = x.shape[0]
    cdef int nsamples = x.shape[1]
    cdef int i, j, v
    
    cdef int nsteps
    cdef DTYPE_t med

    nsteps = nsamples/window

    for i in range(nsteps):
        for v in range(freqs):
            m[v,window*i:window*(i+1)] = np.median(x[v,window*i:window*(i+1)])

    if nsteps*window < nsamples:
        for v in range(freqs):
            m[v,window*nsteps:] = np.median(x[v,window*nsteps:])

    #return x
