from pipeline.MapMaking.Naive import cbinning
import numpy as np
from mpi4py import MPI

class MapMaking:

    def __init__(self, nchannels, npix):
        comm = MPI.COMM_WORLD
        rank = comm.rank

        # Bin edges for making maps
        self.mapbins = np.arange(0, npix+1)

        self.npix = int(npix)
        self.nchannels = int(nchannels)

        self.sw = np.zeros((nchannels, npix))
        self.w  = np.zeros((nchannels, npix))
        self.hits  = np.zeros(npix)
        if rank == 0:
            self.sw_all = np.zeros((nchannels, npix))
            self.w_all = np.zeros((nchannels, npix))
            self.hits_all = np.zeros(npix)
        else:
            self.sw_all = None
            self.w_all = None
            self.hits_all = None

    def BinMap(self, p, d, weights=None):

        # Calculate first the signal weight map
        if isinstance(weights, type(None)):
            cbinning.bin_pix_nw_hits(d, p, self.sw, self.w, self.hits)
        else:
            cbinning.bin_pix_mask_hits(d, p, weights, self.sw, self.w, self.hits)

    def ZeroLocalMaps(self):
        self.sw[...] = 0.
        self.w[...] = 0.
        self.hits[...] = 0. 
