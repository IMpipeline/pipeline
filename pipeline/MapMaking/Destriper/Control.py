#Standard modules:
import numpy as np
try:
    from mpi4py import MPI
    f_found=True
    from ..Tools import MPI_tools
except ImportError:
    f_found=False

from pipeline.Tools import FileTools
from CGM import CGM
from MPIFuncs import MPISum2Root, MPIRoot2Nodes
import binning

#Map-making modules:
#from ..Tools.Mapping import MapsClass
#from ..Tools import nBinning as Binning
#from ..Tools import WhiteCovar

#from ..CGM.nCGM import CGM 

#Destriper modules:
from DesFuncs import bFunc,AXFunc

class Destriper:

    def __init__(self, tod, weights, pix, bl, npix, maxiter=300, Nodes=None):
        """
        Arguments
        tod -- Time-ordered single-dish radio telescope scanning data. (Array)
        weights -- Weight for each tod sample.
        pix -- Time-ordered pixel coordinate data. (Array)
        bl  -- Length of baselines used to Destripe the tod. (Integer)
        npix -- Total number of pixels for output map.

        Setup data containers
        """
        
        comm = MPI.COMM_WORLD
        size = comm.Get_size()
        rank = comm.Get_rank()


        # Make an array of prior 'long' baselines from medians
        self.nBaselines     = int(np.ceil(tod.size / float(bl)))

        # Make sure everything is the correct type!
        self.tod = tod.astype('d')
        self.weights = weights.astype('d')
        self.a0 = np.zeros(self.nBaselines).astype('d') #Variations around zero
        self.pix = pix.astype('int')
        self.bl  = int(bl)
        self.npix= int(npix)
        self.blpix = np.arange(self.tod.size, dtype='i')/self.bl # get our mapping for baselines
        self.size = tod.size
        self.maxiter = int(maxiter)

        # Setup the maps
        self.fullmap = np.zeros(npix, dtype='d')
        self.sw   = np.zeros(npix, dtype='d')
        self.w    = np.zeros(npix, dtype='d')
        self.hits = np.zeros(npix, dtype='d')

        if rank == 0:
            self.swroot  = np.zeros(npix, dtype='d')
            self.wroot  = np.zeros(npix, dtype='d')
        else:
            self.swroot = None
            self.wroot  = None

        # Setup nodes
        if isinstance(Nodes,type(None)):
            self.Nodes = np.arange(size, dtype='i')
        else:
            self.Nodes = Nodes.astype('i')
        
        # Bin edges for making maps
        #self.mapbins = np.arange(0, npix+1)
        #self.hits = np.histogram(self.pix, bins=self.mapbins)[0]
        #self.w = np.histogram(self.pix, bins=self.mapbins, weights=self.weights)[0]
        binning.weight(self.weights, self.pix, self.w)
        binning.hits(self.pix, self.hits)

        self.goodpix = (self.w != 0)

        # Baseline bin edges
        self.offsetbins = np.arange(0, self.nBaselines+1)
        self.offsetindex= np.repeat(self.offsetbins, self.bl)[:self.tod.size]
        
    def BinMap(self, d, w):
        # Calculate first the signal weight map
        self.sw *= 0.
        #self.sw[:] = np.histogram(self.pix, bins=self.mapbins, weights=d*w)[0]
        binning.histweight(d, w, self.pix, self.sw)

        # then the full sky map
        self.fullmap *= 0.
        self.fullmap[self.goodpix] = self.sw[self.goodpix]/self.w[self.goodpix]
        self.fullmap[np.isnan(self.fullmap)] = 0.

    def FtC(self, d, w):
        # Calculate first the signal weight map
        ft_bin = np.histogram(self.offsetindex, bins=self.offsetbins, weights=d*w)[0]

        return ft_bin

    def GuessA0(self):
        # Loop through median values of tod and estimate initial baseline values
        for i in range(self.nBaselines): 
            self.a0[i] = np.median(self.tod[i*self.bl:(i+1)*self.bl])





#------------------------------------------------------------------------#
#----------------------DESTRIPER FUNCTIONS-------------------------------#
#------------------------------------------------------------------------#

def Run(tod, weights, pix, bl, npix, maxiter=300, Nodes=None, substring=''):
    '''
    Return Destriped maps for a given set of TOD.

    Arguments
    tod -- Time-ordered single-dish radio telescope scanning data. (Array)
    weights -- Weight for each tod sample.
    pix -- Time-ordered pixel coordinate data. (Array)
    bl  -- Length of baselines used to Destripe the tod. (Integer)
    npix -- Total number of pixels for output map.

    Keyword Arguments
    maxiter -- Maximum CGM iterations to perform

    '''

    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()

    if isinstance(Nodes, type(None)):
        Nodes = np.arange(size, dtype='int')
    
    todmean  = MPI_tools.MPI_sum(comm,tod)/MPI_tools.MPI_len(comm,tod)
    tod -= todmean

    #Generate Maps:
    print 'PREGENERATING MAP INFORMATION'
    Data = Destriper(tod, weights, pix, bl, npix, maxiter=maxiter, Nodes=Nodes)


    print 'MAKING INITIAL GUESS'
    Data.GuessA0()

    print 'BINNING MAP'
    Data.BinMap(Data.tod, Data.weights)
    MPISum2Root(Data.sw, Data.swroot, Data.Nodes)
    MPISum2Root(Data.w , Data.wroot, Data.Nodes)


    if rank==0:
        gd = (Data.wroot != 0)
        Data.fullmap[gd] = Data.swroot[gd]/Data.wroot[gd]
        inputMap = Data.fullmap * 1.
        
    print 'STARTING CGM'
    CGM(Data, substring)
    print 'CGM COMPLETED'

    FileTools.WriteH5Py('med_offsets.hdf5', {'offsets': Data.a0[Data.blpix]})

    Data.BinMap(Data.a0[Data.blpix], Data.weights) 
    MPISum2Root(Data.sw, Data.swroot, Data.Nodes)
    MPISum2Root(Data.w , Data.wroot, Data.Nodes)
    if rank==0:
        gd = (Data.sw != 0)
        Data.fullmap[gd] = Data.swroot[gd]/Data.wroot[gd]

        return inputMap - Data.fullmap
    else:
        return None
