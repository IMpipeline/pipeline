import numpy as np
from MPIFuncs import MPISum2Root, MPIRoot2Nodes
from DesFuncs import bFunc,AXFunc
try:
    from mpi4py import MPI
    f_found=True
    from ..Tools import MPI_tools
except ImportError:
    f_found=False
from pipeline.Tools import FileTools

def CGM(Data, substring=''):
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()


    FtZd = bFunc(Data)

    FtZa = AXFunc(Data.a0, Data)

    #Ensure b and x0 are column vectors:
    r = np.reshape(FtZd - FtZa, (Data.nBaselines, 1))
    d = np.reshape(FtZd - FtZa, (Data.nBaselines, 1))


    #Initial Threshold:
    del0 = r.T.dot(r)
    if f_found:
        del0  = MPI_tools.MPI_sum(comm,del0)
    else:
        del0 = np.sum(del0)


    dnew = np.copy(del0)

    errors = np.zeros((Data.maxiter, Data.nBaselines))

    lastLim = 1e32 #Initial value
    for i in range(Data.maxiter):
        # Convergence analysis
        errors[i,:] = Data.a0
        ######################

        #Generate a new conjugate search vector Ad using d:
        FtZa = AXFunc(d[:,0], Data)

        # Calculate search vector:
        dTq = np.sum(d*FtZa)#d.T.dot(FtZa)  
        if f_found:
           dTq = MPI_tools.MPI_sum(comm,dTq)
        else:
           dTq = np.sum(dTq)

        alpha = dnew/dTq
        Data.a0 +=  alpha*d[:,0]
        

        if np.mod(i+1,50) == 0:
           FtZa = AXFunc(Data.a0, Data)
           r = np.reshape(FtZd - FtZa,(Data.nBaselines,1))
        else:
           r = r - alpha*FtZa

        dold = dnew*1.
        dnew = np.sum(r**2)# r.T.dot(r) 
        if f_found:
            dnew= MPI_tools.MPI_sum(comm,dnew)
        else:
            dnew = np.sum(dnew)

        beta = dnew/dold

        d = r + beta*d
        if (rank == 0):
            print 'iteration: ', i, 1e-14 * del0/dnew
        if  1e-14 * del0/dnew > 1:
            ilast = i
            break


    # Convergence analysis
    errors -= Data.a0
    e = np.sqrt(np.sum(errors[:ilast,:]**2, axis=1))
    FileTools.WriteH5Py('CGM_ERRORS_{}deg.hdf5'.format(substring), {'errors':e})
    ######################
