import numpy as np
try:
    from mpi4py import MPI
    f_found=True
    from ..Tools import MPI_tools
except ImportError:
    f_found=False

import fBinning
import time

def MPISum2Root(dshare, droot, Nodes):
    # Switch on MPI 
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()


    if rank == 0:
        droot += dshare
    for node in Nodes:
        if node == 0:
            continue

        if rank == node:
            comm.Send([dshare, MPI.DOUBLE], dest=0, tag=node)
        if rank == 0:
            comm.Recv([dshare, MPI.DOUBLE], source=node, tag=node)
            droot += dshare

def MPIRoot2Nodes(dshare, Nodes):
    # Switch on MPI 
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()

    for node in Nodes:
        if node == 0:
            continue

        if rank == 0:
            comm.Send([dshare, MPI.DOUBLE], dest=node, tag=node)
        if rank == node:
            comm.Recv([dshare, MPI.DOUBLE], source=0, tag=node)



def BinMap(x, p, w, m, sw, hw, hits, swroot=None, hwroot=None, hitmap=None, Nodes=None):
    '''
    Return data binned into map pixels.

    x - Data to be binned
    p - Coordinate pixels
    w - weights
    m - Current best map 
    sw - This nodes signal*weight
    hw - This nodes weights
    hits - This maps hit distribution
    swroot - Root node signal*weight
    hwroot - Root node weights
    hitmap - Root node hitmap
    '''

    # Switch on MPI 
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()

    if isinstance(Nodes, type(None)):
        Nodes = np.arange(size, dtype='int')

    # Zero all the temp maps
    m[:] = 0.
    sw[:]= 0.
    hw[:]= 0.        
    hits[:] = 0.


    #LOOP THROUGH EACH MAP PIXEL TO PRODUCE MAP (AND WEIGHT MAP)
    sw[:],hw[:],hits[:] = fBinning.bin_pix_hits(x,
                                                p,
                                                w,
                                                sw,
                                                hw,
                                                hits)
            

    # Sum up all the maps on each node

    MPISum2Root(sw  , swroot, Nodes)
    MPISum2Root(hw  , hwroot, Nodes)
    MPISum2Root(hits, hitmap, Nodes)

    #comm.Reduce([sw  , MPI.DOUBLE] ,[swroot, MPI.DOUBLE] ,op=MPI.SUM, root=0)
    #comm.Reduce([hw  , MPI.DOUBLE] ,[hwroot, MPI.DOUBLE] ,op=MPI.SUM, root=0)
    #comm.Reduce([hits, MPI.DOUBLE] ,[hitmap, MPI.DOUBLE] ,op=MPI.SUM, root=0)        
    #print 'Bye FROM RANK {}'.format(rank)


    if rank==0:
        meh = (hwroot > 0) & (np.isnan(hwroot) == 0) & (np.isinf(hwroot) == 0)
        m[meh] = swroot[meh]/hwroot[meh]

    # Then broadcast it back out to all nodes
    #comm.Bcast([m, MPI.DOUBLE],root=0)
    MPIRoot2Nodes(m, Nodes)

def BinMap_ext(a, bl, p, cn, m, mask, sw, hw, swroot=None, hwroot=None, hitmap=None):
    '''Return data binned into map pixels.
    '''

    # Switch on MPI 
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()

    # Zero all the temp maps
    m[:] = 0.
    sw[:]= 0.
    hw[:]= 0.        


    #LOOP THROUGH EACH MAP PIXEL TO PRODUCE MAP (AND WEIGHT MAP)
    sw[:],hw[:] = fBinning.bin_pix_ext(a,
                                       p, 
                                       cn, 
                                       sw, 
                                       hw, 
                                       mask, int(bl))


    # Sum up all the maps on each node
    comm.Reduce([sw  , MPI.DOUBLE] ,[swroot, MPI.DOUBLE] ,op=MPI.SUM, root=0)
    comm.Reduce([hw  , MPI.DOUBLE] ,[hwroot, MPI.DOUBLE] ,op=MPI.SUM, root=0)


    if rank==0:
        meh = (hwroot > 0) & (np.isnan(hwroot) == 0) & (np.isinf(hwroot) == 0)
        m[meh] = swroot[meh]/hwroot[meh]


    # Then broadcast it back out to all nodes
    comm.Bcast([m, MPI.DOUBLE],root=0)

def BinMap_with_ext(tod, a, bl, p, cn, m, mask, sw, hw, swroot=None, hwroot=None, hitmap=None):
    '''Return data binned into map pixels.
    '''

    # Switch on MPI 
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()

    # Zero all the temp maps
    m[:] = 0.
    sw[:]= 0.
    hw[:]= 0.        


    #LOOP THROUGH EACH MAP PIXEL TO PRODUCE MAP (AND WEIGHT MAP)
    sw[:],hw[:] = fBinning.bin_pix_with_ext(tod,
                                            np.squeeze(a),
                                            p,
                                            cn,
                                            sw,
                                            hw,
                                            mask,
                                            int(bl))


    # Sum up all the maps on each node
    comm.Reduce([sw  , MPI.DOUBLE] ,[swroot, MPI.DOUBLE] ,op=MPI.SUM ,root=0)
    comm.Reduce([hw  , MPI.DOUBLE] ,[hwroot, MPI.DOUBLE] ,op=MPI.SUM ,root=0)


    if rank==0:
        meh = (hwroot > 0) & (np.isnan(hwroot) == 0) & (np.isinf(hwroot) == 0)
        m[meh] = swroot[meh]/hwroot[meh]

    # Then broadcast it back out to all nodes
    comm.Bcast([m, MPI.DOUBLE],root=0)




def BinMapPol_Angs(x,bl,p,phi,cn,map,sw=[None],hw=[None],swroot=None,hwroot=None,hitmap=None,comm=None):
    '''Return data binned into map pixels.
    '''

#     size = comm.Get_size()
#     rank = comm.Get_rank()
    
#     map[:] = 0.
    
#     if sw[0] != None:
#         sw[:]= 0.
#     else:
#         sw = np.zeros(len(m))

#     if hw[0] != None:
#         hw[:]= 0.        
#     else:
#         hw = np.zeros(len(m))
    

    c2 = map*0. 
    s2 = map*0. 
    sc = map*0.
    sw = np.zeros(len(map))
    hw = np.zeros(len(map))

    #Loop through angle maps
    #noweights = cn*0. + 1.
    maps = [sc,s2,c2]
    funcs = [lambda p0:  np.sin(p0)*np.cos(p0),
             lambda p0:  np.sin(p0)*np.sin(p0),
             lambda p0:  np.cos(p0)*np.cos(p0)]

    for i,m in enumerate(maps):
        sw[:] = 0.
        hw[:] = 0.

        sw[:],hw[:] = fBinning.bin_pix(funcs[i](phi),p,cn,sw,hw)
        
        if i == 0:
            gd = np.where(hw != 0)[0]

        m[gd] = sw[gd]/hw[gd]
        

    map[:] = (c2 * s2 - sc**2 )        
        


def DownSample(a,newlen,Errors=False):
    '''
    Return binned version of input array

    Arguments
    a -- Input array
    newlen -- Length of binned output array

    Keyword Arguments
    Errors -- Return errors on bin values (Default: False)


    Notes: The last datum of the output array may contain less samples
    than the rest of the data in the output array.

    '''

    if newlen > a.size:
        print 'WARNING: BIN SIZE GREATER THAN ARRAY SIZE'
        return None

    bins,errs = fBinning.downsample(a,newlen)

    if Errors:
        return bins, errs
    else:
        return bins
