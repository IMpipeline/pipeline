from pipeline.Tools import Parsing
import pipeline


def DefaultDefaults():

    pd = pipeline.__file__
    # remove .py from the end
    pd = pd.split('/')
    del pd[-1]
    pd = '/'.join(pd) + '/'

    DefaultParameters = Parsing.Parser(pd+'Parameters.ini')

    return DefaultParameters

def DefaultParameters(defaultfile):

    if isinstance(defaultfile, type(None)):
        return DefaultDefaults()
    else:
        return Parsing.Parser(defaultfile)
