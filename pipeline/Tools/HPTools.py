import healpy as hp
import numpy as np

def HealpixRotate(m, coords=['C','G']):
    """
    ['C','G'] is galactic to celestial
    ['G','C'] is celestial to galactic
    """
    m[np.isnan(m)] = hp.UNSEEN
    m[m == 0] = hp.UNSEEN

    nside_in = int(np.sqrt(m.size/12))
    nside_up = int(nside_in*2)

    map_up = hp.ud_grade(m,nside_up)
    pix_up = np.arange(12*nside_up**2, dtype='i')


    #Declare the rotator
    rot = hp.rotator.Rotator(coord=coords)

    #Get dec and ra:    
    dec, ra = hp.pix2ang(nside_up, pix_up, False)

    #Galactic coordinates:
    b, l = rot(dec, ra)

    #Convert to pix again:
    pix_rot = hp.ang2pix(nside_up, b, l)

    #Rotate map:
    map_up[:] = map_up[pix_rot]

    #Downsize:
    m = hp.ud_grade(map_up, nside_in)
    m[m == hp.UNSEEN] = 0

    return m
