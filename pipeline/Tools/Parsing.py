import numpy as np
import re


class Parser(object):
    """
    """

    def __init__(self, filename):
        super(Parser, self)
        
        self.infodict = {}

        self.file = open(filename, 'r')
        self.ReadLines()

    def __setitem__(self,k,v):
        self.infodict[k] = v
    
    def __getitem__(self,k):
        try:
            return self.infodict[k]
        except KeyError:
            raise AttributeError
        
    def __contains__(self, k):
        return k in self.infodict
        
        
    def ReadLines(self):
        """
        """
        
        
        for line in self.file:
            #First remove comments
            line_nocomments = line.split('#')[0].strip()

            if len(line_nocomments) > 0:

                if (line_nocomments[0] == '[') & (line_nocomments[-1] == ']'):
                    #Now check for Headers
                    thisHeader = re.split('\\[|\\]', line_nocomments)[1]
                    if thisHeader not in self.infodict:
                        self.infodict[thisHeader] = {}
                else:
                    #Now fill the headers
                    keyword, delim, value = line_nocomments.split()
                    
                    #Want to check for arrays of values split by commas
                    value = value.replace(' ', '').split(',')
                    
                    if len(value) > 1:
                        self.infodict[thisHeader][keyword] = []
                        
                        for v in value:
                            if v == 'None':
                                self.infodict[thisHeader][keyword] += [None]
                            elif ('True' in value):
                                self.infodict[thisHeader][keyword] += [True]            
                            elif ('False' in value):
                                self.infodict[thisHeader][keyword] += [False]         
                            else:
                                try:
                                    self.infodict[thisHeader][keyword] += [float(v)]
                                except ValueError:
                                    self.infodict[thisHeader][keyword] += [v]
                    else:
                        for v in value:
                            if v == 'None':
                                self.infodict[thisHeader][keyword] = None
                            elif ('True' in value):
                                self.infodict[thisHeader][keyword] = True          
                            elif ('False' in value):
                                self.infodict[thisHeader][keyword] = False       
                            else:
                                try:
                                    self.infodict[thisHeader][keyword] = float(v)
                                except ValueError:
                                    self.infodict[thisHeader][keyword] = v
                        
                        

        self.file.close()
