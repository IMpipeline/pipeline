import numpy as np
import scipy.fftpack as sfft
from matplotlib import pyplot

from scipy.signal import blackman
import scipy.signal as signal
from scipy.interpolate import interp1d

# BUTTER WORTH HIGH PASS FILTER:
def butter_bandpass(cutoff, highcut, fs, order=3):
    nyq = 0.5 * fs
    normal_cutoff = cutoff / nyq
    normal_highcut = highcut / nyq * 0.5 # Half because the maximum fourier mode sample is sample-rate/2
    
    if normal_highcut >= 1.0:
        b, a = signal.butter(order, normal_cutoff, btype='low', analog=False)
    else:
        b, a = signal.butter(order, [normal_cutoff, normal_highcut], btype='band', analog=False)
    return b, a

def butter_filter(data, cutoff, highcut, fs, order=3, return_filter=False, axis=1):
    """
    NOTE! Before filtering your data be sure to subtract off the mean along the filter axis!
    """
    b, a = butter_bandpass(cutoff, highcut, fs, order=order)
    
    #data -= np.reshape(data[:,0], (data.shape[0], 1))
    data -= np.reshape(np.mean(data, axis=1), (data.shape[0], 1))
    y = signal.filtfilt(b, a, data, axis=axis)#*signal.hamming(data.shape[axis])
    #y /= signal.hamming(data.shape[axis])


    if return_filter:
        w, h = signal.freqz(b, a, worN=data.size)
        nu = (w/2./np.pi) * fs 
        
        return y, nu, h
    else:
        return y

def butter_filterfft(data, cutoff, highcut, fs, order=3, return_filter=False, axis=1):
    """
    NOTE! Before filtering your data be sure to subtract off the mean along the filter axis!
    """
    b, a = butter_bandpass(cutoff, highcut, fs, order=order)
    
    #data -= np.reshape(data[:,0], (data.shape[0], 1))
    data -= np.reshape(np.mean(data, axis=1), (data.shape[0], 1))
    w, h = signal.freqz(b, a, worN=data.size)
    nu = (w/2./np.pi) * fs 
    n0 = sfft.fftfreq(data.shape[1], d=1./fs)
    fmdl = interp1d(nu, h, bounds_error=False, fill_value=0)
    tfilt = fmdl(np.abs(n0))

    window = signal.hamming(data.shape[1])
    fs = sfft.fft(data*window)
    y = np.real(sfft.ifft(fs*tfilt)/window)

    #y = signal.filtfilt(b, a, data, axis=axis)#*signal.hamming(data.shape[axis])
    #y /= signal.hamming(data.shape[axis])


    if return_filter:
        w, h = signal.freqz(b, a, worN=data.size)
        nu = (w/2./np.pi) * fs 
        
        return y, nu, h
    else:
        return y

## STOLEN FROM SCIPY COOKBOOK AS IS

def savitzky_golay(y, window_size, order, deriv=0, rate=1):
    r"""Smooth (and optionally differentiate) data with a Savitzky-Golay filter.
    The Savitzky-Golay filter removes high frequency noise from data.
    It has the advantage of preserving the original shape and
    features of the signal better than other types of filtering
    approaches, such as moving averages techniques.
    Parameters
    ----------
    y : array_like, shape (N,)
        the values of the time history of the signal.
    window_size : int
        the length of the window. Must be an odd integer number.
    order : int
        the order of the polynomial used in the filtering.
        Must be less then `window_size` - 1.
    deriv: int
        the order of the derivative to compute (default = 0 means only smoothing)
    Returns
    -------
    ys : ndarray, shape (N)
        the smoothed signal (or it's n-th derivative).
    Notes
    -----
    The Savitzky-Golay is a type of low-pass filter, particularly
    suited for smoothing noisy data. The main idea behind this
    approach is to make for each point a least-square fit with a
    polynomial of high order over a odd-sized window centered at
    the point.
    Examples
    --------
    t = np.linspace(-4, 4, 500)
    y = np.exp( -t**2 ) + np.random.normal(0, 0.05, t.shape)
    ysg = savitzky_golay(y, window_size=31, order=4)
    import matplotlib.pyplot as plt
    plt.plot(t, y, label='Noisy signal')
    plt.plot(t, np.exp(-t**2), 'k', lw=1.5, label='Original signal')
    plt.plot(t, ysg, 'r', label='Filtered signal')
    plt.legend()
    plt.show()
    References
    ----------
    .. [1] A. Savitzky, M. J. E. Golay, Smoothing and Differentiation of
       Data by Simplified Least Squares Procedures. Analytical
       Chemistry, 1964, 36 (8), pp 1627-1639.
    .. [2] Numerical Recipes 3rd Edition: The Art of Scientific Computing
       W.H. Press, S.A. Teukolsky, W.T. Vetterling, B.P. Flannery
       Cambridge University Press ISBN-13: 9780521880688
    """
    import numpy as np
    from math import factorial

    try:
        window_size = np.abs(np.int(window_size))
        order = np.abs(np.int(order))
    except ValueError:
        raise ValueError("window_size and order have to be of type int")
    if window_size % 2 != 1 or window_size < 1:
        raise TypeError("window_size size must be a positive odd number")
    if window_size < order + 2:
        raise TypeError("window_size is too small for the polynomials order")
    order_range = range(order+1)
    half_window = (window_size -1) // 2
    # precompute coefficients
    b = np.mat([[k**i for i in order_range] for k in range(-half_window, half_window+1)])
    m = np.linalg.pinv(b).A[deriv] * rate**deriv * factorial(deriv)
    # pad the signal at the extremes with
    # values taken from the signal itself
    firstvals = y[0] - np.abs( y[1:half_window+1][::-1] - y[0] )
    lastvals = y[-1] + np.abs(y[-half_window-1:-1][::-1] - y[-1])
    y = np.concatenate((firstvals, y, lastvals))
    return np.convolve( m[::-1], y, mode='valid')


class CosineWindow():
    
    def __init__(self, nsamps, sr, cutoff, fwidth):

        self.sr = sr
        self.cutoff = cutoff
        self.fwidth = fwidth

        self.nsamps_2bit = 2**int(np.ceil(np.log10(nsamps)/np.log10(2.)))
    
        self.nu = sfft.fftfreq(int(self.nsamps_2bit), d=1./self.sr)
    
        self.window = 0.5 + 0.5*np.cos(np.pi* ( np.abs(self.nu)  - self.cutoff - self.fwidth/2. )/self.fwidth)#0.5*(1.-np.cos(np.pi* ( np.abs(nu)  - cutoff )/fwidth) )

        lo = (np.abs(self.nu) <=  self.cutoff - self.fwidth/2.)
        hi = (np.abs(self.nu) > self.cutoff + self.fwidth/2.)
        self.window[hi] = 1.
        self.window[lo] = 0.

    def ApplyFilter(self, data):

        data -= np.reshape(np.median(data,axis=1), (data.shape[0], 1)) # remove general trend first

        fs = sfft.fft(data)
        data = np.real(sfft.ifft(fs*self.window))

        return data


def cosineWindow(nsamps, sr, cutoff, fwidth):
    """
    Calculates a cosine window function for a re/im FFT using the SciPy conventions.
    
    Arguments 
    nsamps - number of samples in TOD set
    sr     - the sample rate of the data (Hz)
    cutoff   - the minimum frequency from which to start filtering the data (Hz)
    fwidth - the width of the cosine filter kernel in frequency space (Hz)
    """

    nsamps_2bit = 2**int(np.ceil(np.log10(nsamps)/np.log10(2.)))
    
    nu = sfft.fftfreq(int(nsamps_2bit), d=1./sr)
    fc = fwidth/sr
    
    window = 0.5 + 0.5*np.cos(np.pi* ( np.abs(nu)  - cutoff - fwidth/2. )/fwidth)#0.5*(1.-np.cos(np.pi* ( np.abs(nu)  - cutoff )/fwidth) )
    lo = (np.abs(nu) <=  cutoff - fwidth/2.)
    hi = (np.abs(nu) > cutoff + fwidth/2.)
    window[hi] = 1.
    window[lo] = 0.

    return nu, window

def applyFilter(data, window):
    """
    Applies window to data and returns filtered data stream.
    
    Arguments
    data   - the input TOD
    window - window function to apply (should be in scipy FFT format (re/im parts))
    """
    nsamps = data.size
    nsamps_2bit = 2**int(np.ceil(np.log10(nsamps)/np.log10(2.)))

    n = nsamps_2bit - nsamps
    pdata = np.zeros(nsamps_2bit)
    pdata[n/2:nsamps+n/2] = data

    # Add mirror buffers to padded regions to avoid artefacts
    n0 = pdata.size - (nsamps + n/2)
    pdata[nsamps+n/2:] = (data[-n0:])[::-1] 
    
    pdata[:n/2] = (data[:n/2])[::-1]
    
    f1 = sfft.fft(pdata)
    #f2 = sfft.fft(pdata*bwindow)
    fdata = sfft.fft(pdata) * window
    nu = sfft.fftfreq(int(nsamps_2bit), d=1./0.1)
    #pyplot.figure()
    #pyplot.plot(nu, np.abs(f1)/nsamps_2bit)
    #pyplot.xscale('log')

    #pyplot.figure()
    #pyplot.plot(nu, np.abs(fdata)/nsamps_2bit)
    #pyplot.xscale('log')
    
    #pyplot.figure()
    
    #pyplot.plot(nu, np.abs(window)/nsamps_2bit)
    
    #pyplot.xscale('log')
    #pyplot.show()

    pyplot.plot(pdata,'-')
    data  = np.real(sfft.ifft(fdata))
    pyplot.plot(data,'-')
    pyplot.show()
    stop
    return data[n/2:nsamps+n/2] 
    
    
    
