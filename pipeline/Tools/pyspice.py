import numpy as np
import healpy as hp
from subprocess import call
import sys

def callspice(m, mask=None, weights=None, m2=None, apodize=None, thetamax=None, id=None, tmpdir='.'):
    
    if id == None:
        id = np.random.randint(low=1,high=1e6)
    else:
        id = int(id)

    inputmapFile = '{}/test{}.fits'.format(tmpdir,id)
    outputClFile = "{}/test{}.dat".format(tmpdir,id)
    input2mapFile = '{}/map2{}.fits'.format(tmpdir,id)
    inputmaskFile = '{}/mask{}.fits'.format(tmpdir,id)
    inputweightsFile = '{}/weights1{}.fits'.format(tmpdir,id)

    hp.write_map(inputmapFile, m)
    
    if not isinstance(apodize, type(None)):
        cmd = ["spice", "-mapfile", inputmapFile, "-clfile", outputClFile ,"-verbosity","NO", 
               "-apodizesigma", "{}".format(float(apodize)), "-apodizetype", "0",
              "-pixelfile", "NO"]
    else:
        cmd = ["spice", "-mapfile", inputmapFile, "-clfile", outputClFile ,"-verbosity","NO",
              "-pixelfile", "NO"]


    
    if not isinstance(m2, type(None)):
        hp.write_map(input2mapFile,m2)
        cmd += ["-mapfile2",input2mapFile]

    
    if not isinstance(mask, type(None)):
        hp.write_map(inputmaskFile,mask)
        cmd += ["-maskfile", inputmaskFile]

    if not isinstance(weights, type(None)):
        hp.write_map(inputweightsFile, weights)
        cmd += ["-weightfile", inputweightsFile]

    if not isinstance(thetamax, type(None)):
        cmd += ["-thetamax", "{}".format(thetamax)]

    call(cmd)
    d = np.loadtxt(outputClFile)
    if not isinstance(mask, type(None)):
        call(['rm', inputmaskFile])
        
    if not isinstance(m2, type(None)):
        call(['rm',input2mapFile])

    if not isinstance(weights, type(None)):
        call(['rm',inputweightsFile ])



    call(['rm', inputmapFile, outputClFile ])
    return d


def bin_llcl (llcl_in, ubin, flatten=False, uniform=False):
    """ x, y, dx, dy = bin_llcl( llcl_in, bin, flatten=False, uniform=False)
    
    turns llcl_in (= continuous l*(l+1)*cl/2Pi) into a binned version with
       a constant or variable binwidth 'bin'


    INPUTS
      llcl_in : input l*(l+1)*Cl/2Pi, 1D vector, defined for each l from l=0
      bin : can be either a scalar = dl
        or a vector defining the bins boundaries : 
        [low0, low1, low2, ...,low(n-2), low(n-1)+1]
    
    OUTPUTS
        x:  center of bins
        y:  binned l*(l+1)*Cl/2Pi
        dx: width of each bin
        dy: returns on output the rms of C(l) for a full sky observation
           = C(l) * sqrt( 2/ 2l+1 / dl)
    
    
    KEYWORDS
      flatten: if set, the input C(l) is multiplied internally by l*(l+1)/2Pi before being
        binned. By default, the input C(l) is binned as is.
    
      uniform: if set, each l is given the same weight in the bin.
        By default, a weighting propto (2*l+1) (inverse cosmic
        variance) is applied to each l.
        In any cases, the output x is the same

    HISTORY
      2016-11-23: adapted from Healpix/IDL    bin_llcl.pro
      
    """

    bin = np.copy(ubin)
    nb  = np.size(bin)
    lmax_in = np.size(llcl_in)-1
    if (nb > 1):
        k = np.where(bin <= (lmax_in+1))[0]
        nk = np.size(k)
        if (nk == (nb-1)):
            bin = np.minimum( bin , lmax_in+1 ) # shorten last bin
        if (nk < (nb-1)): # shorten last valid bins, and drop the ones beyond lmax
            bin = np.concatenate((bin[k], [lmax_in+1]))
            nb = nk + 1
    

    if nb == 1:
        # regular binning
        nbins = lmax_in/np.int(bin)
        lmax  = nbins * bin  -1
        l     = np.arange(lmax+1,dtype=np.float)
        w     = 2*l + 1
        if (uniform): w = np.ones(lmax+1,dtype=np.float)
        y = np.copy(llcl_in[0:lmax+1])
        if (flatten): y *= l*(l+1.)/(2*np.pi)
        w1 = np.reshape(w,    (nbins, bin))
        y1 = np.reshape(y*w,  (nbins, bin))
        l1 = np.reshape(l,    (nbins, bin))
        n1 = np.ones(         (nbins, bin))
        
        llcl_out = np.sum(y1,1)/np.sum(w1,1)
        l_out    = np.sum(l1,1)/np.sum(n1,1)
        dl       = bin * np.ones(nbins, dtype=np.int)
        
    else:

        # irregular binning
        lmax  = np.max(bin)-1
        nbins = nb-1
        good  = np.where(bin < lmax)[0]
        ng    = np.size(good)
        if (ng == 0):
            print('l-range of binning does not intersect that of data')
            return -1,-1,-1,-1
    
        l  = np.arange(lmax+1, dtype=np.float)
        w  = 2*l + 1
        if (uniform): w = np.ones(lmax+1, dtype=np.float)
        y = np.copy(llcl_in[0:lmax+1])
        if (flatten): y *= l*(l+1.)/(2*np.pi)
        l_out    = np.zeros(nbins,dtype=np.float)
        llcl_out = np.zeros(nbins,dtype=np.float)
        dl       = np.zeros(nbins,dtype=np.int)
        for i in range(nbins):
            l_out[i] = np.mean( l[bin[i]:bin[i+1]])
            dl[i]    = bin[i+1]-bin[i]
            llcl_out[i] = np.sum( (y*w) [bin[i]:bin[i+1]] ) \
                        / np.sum(    w  [bin[i]:bin[i+1]] )
    

    dllcl = llcl_out * np.sqrt(2/(2*l_out+1.)/dl)
    
    return l_out, llcl_out, dl, dllcl


    
