import numpy as np
import scipy.fftpack as sfft
from scipy.interpolate import interp1d



def PSpectrum(xpix, slab, dt, index, b, amp):
    """
    Returns an interpolation model of a radial power spectrum along with 
    the derived Pk and k values. Use slab and b terms to define the type
    of clouds, and amp to scale the amplitudes to those observed.
    
    Arguments:
    xpix  - Resolution in x-pixel space
    slab  - Width of the atmosphere in metres
    dt    - How many blocks of atmosphere in y
    index - Steepness of radial power spectrum
    b     - Maximum scale to generate
    amp   - Amplitude of variations at maximum scale b
    """

    # First generate basic power spectrum
    low = np.min([1./slab, b/10.])
    k = 10**np.linspace(np.log10(low), np.log10(xpix/slab*100), xpix)
    #P = k**index / (np.exp(-b*index/k/2.) - 1 ) #((1.-np.exp(-k/knee))*knee/k)**(index + b*np.log10(k))

    P = k**index  / (np.exp(-b*index/k/2.) - 1 ) #((1.-np.exp(-k/knee))*knee/k)**(index + b*np.log10(k))
        
    
    P = amp**2 * P  / np.max(P) 
    
    Pmdl = interp1d(np.concatenate((-k[::-1], [0]   , k)),
                    np.concatenate(( P[::-1], [P[0]], P)), bounds_error=False, fill_value=np.min(P))

    return Pmdl, P, k

def iFFT1d(Pmdl, slab, imshape):
    """
    Reverse radial fourier transform of a 2D image.
    
    Arguments
    Pmdl    - Powerspectrum model return from PSpectrum function
    slab    - Characteristic scale of the atmosphere block
    imshape - Dimensions of the image to generate (xpix, ypix)
    

    """
    
    # First we need to determine the padded dimensions
    padshape = np.zeros(len(imshape), dtype='int')
    for i in range(padshape.size):
        tmp = np.ceil(np.log10(imshape[i])/np.log10(2))
        padshape[i] = int(2**tmp)
        
    padshape = imshape
                
    # Define fourier component axes
    nux = np.sort(sfft.fftfreq(padshape[0])*padshape[0]/slab)#np.linspace(-imshape[0]/slab/2., imshape[0]/slab/2., padshape[0])
    nuy = np.sort(sfft.fftfreq(padshape[1])*padshape[0]/slab)#np.linspace(-imshape[0]/slab/2., imshape[0]/slab/2., padshape[1])

    # Define the r-coordinate of each pixel
    x, y = np.meshgrid(nux, nuy)
    x = x.T.flatten()
    y = y.T.flatten()
    r = np.sqrt(x**2 + y**2 ) 

    # Y axis first, xaxis, zaxis
    padshape = (padshape[0], padshape[1])

    # Generate fourier cube
    fimg  = Pmdl(r)
    fimg[np.isnan(fimg)] = 0.
    fimg = np.reshape(fimg, padshape )

    
    # Generate some random fourier phases
    phases = np.random.uniform(low = 0, high =np.pi, size=padshape)
    phases = np.cos(phases) + 1j*np.sin(phases)

    # NORMALISATION
    fimg = fimg/np.sqrt(padshape[0]*padshape[1])
        
    # Shift FFT to vertices of cube
    fimg = np.roll(np.roll(fimg, padshape[0]/2, axis=0), padshape[1]/2, axis=1)  
    fimg = np.sqrt(fimg)*phases        
    img = np.real(sfft.fftn(fimg, shape=padshape  ))

    # Crop off any padded pixels
    dx = padshape[0]-imshape[0]
    dy = padshape[1]-imshape[1]

    img = img[dx/2:dx/2+imshape[0],
              dy/2:dy/2+imshape[1]]
    fimg = sfft.ifftn(img, shape=padshape)

    return img

def FFT1d(img, slab):
    """
    Forward radial fourier transform of a 2D image.
    
    Returns k and Pk with dimension min(img.shape)-1
    
    Arguments
    img     - Input image to measure the radial transform
    slab    - Characteristic scale of the atmosphere block
    
    

    """

    imshape = img.shape
    
    # First we need to determine the padded dimensions
    padshape = np.zeros(len(imshape), dtype='int')
    for i in range(padshape.size):
        tmp = np.ceil(np.log10(imshape[i])/np.log10(2))
        padshape[i] = int(2**tmp)
    
    padshape= imshape
    # First get the 2D power spectrum of the map
    fimg = sfft.ifftn(img, shape=padshape)
    pimg = np.abs(fimg)**2
    
    # Shift the map 
    pimg = np.roll(np.roll(pimg, padshape[0]/2, axis=0), padshape[1]/2, axis=1)  

    # Rescale power spectrum
    pimg = pimg*np.sqrt(padshape[0]*padshape[1])    
    pimg = pimg.flatten()
    
    # Define fourier component axes
    nux = np.sort(sfft.fftfreq(padshape[0])*padshape[0]/slab)#np.linspace(-imshape[0]/slab/2., imshape[0]/slab/2., padshape[0])
    nuy = np.sort(sfft.fftfreq(padshape[1])*padshape[0]/slab)#np.linspace(-imshape[0]/slab/2., imshape[0]/slab/2., padshape[1])

    # Define the r-coordinate of each pixel
    x, y = np.meshgrid(nux, nuy)
    x = x.T.flatten()
    y = y.T.flatten()
    r = np.sqrt(x**2 + y**2 ) 
    
    # Now take radial averages
    raxis = np.min(imshape)/1
    rpow = np.zeros(raxis)
    rcen = np.zeros(raxis)


    rb = np.linspace(0, np.max(x), raxis+1)
    rcen = (rb[1:] + rb[:-1])/2.
    
    for i in range(raxis):
        lo = (x**2 + y**2 >= rb[i]**2)
        hi = (x**2 + y**2 < rb[i+1]**2)
    
        rpow[i] = np.mean(pimg[lo & hi])
        
    return rcen[1:], rpow[1:]

