"""
Name: Astrometry

Description: A number of helpful functions for calculating
astronomical observations.

"""

import numpy as np


def _RasterObsTime(dist, veloc, accel, nslews, nsteps):
    """
    Estimate length of time to perform a raster scan.

    """
    #Acceleration dist
    sa = veloc**2/2./accel

    #Need to check max veloc is achieved
    if sa >= dist/2.:
        ta = np.sqrt(dist/accel)

        dT = 2.*ta
    else:
        #Acceleration time
        ta = veloc/accel 
        #Drift time
        t = (dist - sa)/veloc

        dT = t + 2.*ta

    #Multiplied by the number of slews and steps
    return dT*nslews*nsteps
