import numpy as np
from matplotlib import pyplot
import h5py

class Container(object):
    """
    Class: Container
    
    Description: Default container class for the IMS. Used to defined
    Obeservation, Telescope and Receiver classes.

    """
    def __init__(self,dataDict):
        #Inherit setattr method from Python object
        # data attributes will be accessible from object.val
        super(Container,self).__setattr__('data',dataDict)

    def __setattr__(self,k,v):
        '''
        Takes key k and assigns object v.
        '''
        self.data[k] = v

    def __getattr__(self, k):
        try:
            return self.data[k]
        except KeyError:
            raise AttributeError 

    def __str__(self):
        outStr = ''
        for key, value in self.data.iteritems():
            if isinstance(value,type(None)):
                outStr += '%s : %s \n' % (key,'None')
            else:
                outStr += '%s : %s \n' % (key,str(value))

        return outStr

    #def WriteTable(self):
