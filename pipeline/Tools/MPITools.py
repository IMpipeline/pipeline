import numpy as np
from mpi4py import MPI

from scipy.special import factorial

def DistributeNodes(nblocks, rank=None, doThis=True):
    """
    Calculates the range within nblocks of data each node will need to loop over.
    
    Arguments
    nblocks - Integer number of data blocks.
    """
    
    comm = MPI.COMM_WORLD
    if rank == None:
        rank = comm.rank
    size = comm.size
    
    nNode = np.zeros(size)
    if nblocks >= size:
        nNodes  = nblocks//size
        r = nblocks - nNodes*size

        nNode[:] = nNodes
        nNode[:r] += 1
    else:
        nNode[:nblocks] = 1

    nodeLo = int(np.sum(nNode[:rank]))
    nodeHi = int(np.sum(nNode[:rank+1])) 
    nSteps = int(nNode[rank])
        
    return int(nodeLo), int(nodeHi), int(nSteps)

def AssignRandomSeeds():
    """
    Assigns unique seeds to each node
    """
    comm = MPI.COMM_WORLD
    rank = comm.rank
    size = comm.size
    
    if rank == 0:
        seeds = np.random.uniform(low=0, high=2147483647, size=size).astype('d')
    else:
        seeds = np.zeros(size, dtype='d')
        
    comm.Bcast([seeds, MPI.DOUBLE], root=0)
    
    np.random.seed(int(seeds[rank]))

