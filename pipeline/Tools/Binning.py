"""
Some general tools for binning regular and irregular data.
"""

import numpy as np
from scipy.interpolate import interp1d

def RegBinData(x, y, e, nbins):
    """
    Regular binning routine. Will return nbins of data, each bin will
    contain a different number of samples if x/y are not regularly
    spaced.

    Arguments: 
    x - Coordinate of each input value
    y - Value of each input
    e - Error on each y value
    nbins - Number of bins
    """
    xends = np.linspace(np.min(x), np.max(x), nbins+1)
    xbins = (xends[1:] + xends[:-1])/2.
    ybins = np.zeros(nbins)
    n = np.zeros(nbins)
    for i in range(nbins):
        gd = (x > xends[i]) & (x < xends[i+1])
        
        ybins[i] = np.sum(y[gd]/e[gd]**2)/np.sum(1./e[gd]**2)#np.nanmean(y[gd])
        n[i] = len(y[gd])

    return xbins, ybins

def IrrBinData(x, y, e, nbins):
    """
    Irregular binning routine. Will return nbins of data, each bin
    will contain the same number of counts, but the bin x coordinates
    will not be uniform.

    Arguments: 
    x - Coordinate of each input value
    y - Value of each input
    e - Error on each y value
    nbins - Number of bins
    """

    count = x.size/nbins
    xbins = np.zeros(nbins)
    ybins = np.zeros(nbins)
    ebins = np.zeros(nbins)

    xsort = np.argsort(x)
    for i in range(nbins):
        if i == nbins-1:
            hi = x.size
        else:
            hi = (i+1)*count

        xbins[i] = np.mean(x[xsort[i*count:hi]])
        ybins[i] = np.sum(y[xsort[i*count:hi]]/e[xsort[i*count:hi]]**2)/np.sum(1./e[xsort[i*count:hi]]**2)
        ebins[i] = np.std(y[xsort[i*count:hi]])/np.sqrt(hi-i*count)

    return xbins, ybins, ebins

def RegularModel(x, y, e, nbins):
    """
    Takes some non-uniform data and rebins it into a regular
    format. WARNING: This will effectively perform a boxhat smooth on
    your data but in a non-obvious way.

    Arguments: 
    x - Coordinate of each input value
    y - Value of each input
    e - Error on each y value
    nbins - Number of bins
    """

    # First bin the data using irregular binning but equal counts
    xbins, ybins, ebins = IrrBinData(x, y, e, nbins*2)

    # Remove any bad bins
    gdbins = (np.isnan(ybins) == False) & (np.isinf(ybins) == False)
    xbins = xbins[gdbins]
    ybins = ybins[gdbins]
    ebins = ebins[gdbins]

    # Interpolate between the points with high resolution sampling
    mdl = interp1d(xbins, ybins, kind='linear')
    emdl = interp1d(xbins, ebins, kind='linear')
    j_l = np.linspace(np.min(xbins), np.max(xbins), nbins*20)
    r_l = mdl(j_l)
    e_l = emdl(j_l)

    # Regularly bin the interpolated model to get regular bins
    rx, ry = RegBinData(j_l, r_l, e_l, nbins)
    
    # Append front and back with min/max jd
    rx = np.concatenate(([np.min(x)], rx, [np.max(x)]))
    ry = np.concatenate(([ry[0]], ry, [ry[-1]]))

    return rx, ry
