
def SafeInt(a):
    """
    This checks to see if you have been caught by a floating point rounding error
    
    e.g. if a is 10 but REALLY is 9.999999 then int(a) = 9
    9.9999 - int(a) = 0.9999999...
    
    in this case int(a) needs to be increased in size by 1
    
    but if a is really 10.00001 then int(a) = 10 and
    10.00001 - int(a) = 0.00001
        
    in this case int(a) is fine!
    
    """
    
    if (a - int(a)) > 0.5: 
        b = int(a)
        b += 1
    else:
        b = int(a)

    return b