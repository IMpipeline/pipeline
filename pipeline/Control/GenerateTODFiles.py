import numpy as np
from mpi4py import MPI
import h5py

def CreateFileObject(filename, simInfo):

    f = filename+'_FileNo{:04d}.hdf5'
    todfileList = []
    for i in range(simInfo.nFiles):
        nSamples = simInfo.fileSizes[i]

        nFeeds = simInfo.nTelescopes * simInfo.nReceivers
        try:
            todfile = h5py.File(f.format(i),'w')#, driver='mpio', comm=MPI.COMM_WORLD)
        except NameError:
            print ('WARNING: HDF5 NOT COMPILED USING MPI ROUTINES - USING SINGLE THREAD MODE. DO NOT RUN WITH MULTIPLE CORES.')
            todfile = h5py.File(f.format(i),'w') 

                      
        # Create the structures for the file
        if simInfo.Parameters['Inputs']['sim_Receiver']:
            outputTOD = todfile.create_dataset('TOD',(nFeeds, int(simInfo.nchannels), nSamples,), dtype='d')
        outputAz   = todfile.create_dataset('AZ',(nSamples,), dtype='d')
        outputEl   = todfile.create_dataset('EL',(nSamples,), dtype='d')
        outputJD   = todfile.create_dataset('JD',(nSamples,), dtype='d')
        outputPang = todfile.create_dataset('PANG',(nSamples,), dtype='d')
        outputRA   = todfile.create_dataset('RA',(nFeeds, nSamples,), dtype='d')
        outputDec  = todfile.create_dataset('DEC',(nFeeds, nSamples,), dtype='d')
        outputPix  = todfile.create_dataset('PIXELS',(nFeeds, nSamples,), dtype='d')
    
        if (simInfo.Parameters['RFI']['fullOutput']):
            outputSatCount = todfile.create_dataset('SATCOUNTS',(nSamples,), dtype='d')
            outputSatCount = todfile.create_dataset('SATDIST',(nSamples, simInfo.nTLEs), dtype='d')
            outputSatCount = todfile.create_dataset('SATAZ',(nSamples, simInfo.nTLEs), dtype='d')
            outputSatCount = todfile.create_dataset('SATEL',(nSamples, simInfo.nTLEs), dtype='d')

        # Create the incidental information
        header = todfile.create_group('HEADER')

        header.attrs['npix'] = simInfo.npix
        header.attrs['pixelMode'] = simInfo.Parameters['Mapping']['mode']
        header.attrs['sampleRate'] = simInfo.sampleRate
        header.attrs['speed'] = simInfo.Parameters['Telescope']['speed']

        todfileList += [todfile]

    return todfileList

def WriteToFileObject(todfileList, simInfo, iblock, data):# TOD_All, TOD_Coordinates, TOD_Pixels):


    start = simInfo.blocks.nFileStart[iblock]
    end   = start + simInfo.blocks.nSamples[iblock]
    iObs = int(simInfo.blocks.iObsIndex[iblock])
    fObs = int(simInfo.blocks.fObsIndex[iblock])

    #print ('WRITING', len(todfileList), simInfo.nFiles, simInfo.blocks.fileID[iblock])
    todfile = todfileList[simInfo.blocks.fileID[iblock]]
    nSamples = simInfo.blocks.nSamples[iblock]

    # Which feed are we using?
    iFeed = simInfo.recIDs[iObs] + simInfo.nTelescopes * simInfo.telIDs[iObs]

    for k, v in data.items():
        if isinstance(v, type(None)): # Check to see if this data exists
            continue # ...if not skip

        if len(todfile[k].shape) > len(v.shape): # Some data is written in a feed specific way
            todfile[k][iFeed,...,start:end] = v[...,:nSamples]
        else:
            todfile[k][...,start:end] = v[...,:nSamples]

    # Only boresight feed az/el/jd are saved
    # if iFeed == 0:
    #     todfile['AZ'][start:end]     = TOD_Coordinates[0,:nSamples]
    #     todfile['EL'][start:end]     = TOD_Coordinates[1,:nSamples]
    #     todfile['JD'][start:end]     = TOD_Coordinates[2,:nSamples]
    #     todfile['PANG'][start:end]   = TOD_Coordinates[5,:nSamples]

    
    # if simInfo.Parameters['Inputs']['sim_Receiver']:
    #     todfile['TOD'][iFeed, :, start:end] = TOD_All[:,:nSamples]
    # todfile['RA'][iFeed, start:end]     = TOD_Coordinates[3,:nSamples]
    # todfile['DEC'][iFeed, start:end]    = TOD_Coordinates[4,:nSamples]
    # todfile['PIXELS'][iFeed, start:end] = TOD_Pixels[:nSamples]

def COMAP_CreateFileObject(filename, simInfo):

    f = filename+'_FileNo{:04d}.hdf5'
    todfileList = []
    for i in range(simInfo.nFiles):
        nSamples = simInfo.fileSizes[i]

        nFeeds = simInfo.nTelescopes * simInfo.nReceivers
        try:
            todfile = h5py.File(f.format(i),'w')#, driver='mpio', comm=MPI.COMM_WORLD)
        except NameError:
            print ('WARNING: HDF5 NOT COMPILED USING MPI ROUTINES - USING SINGLE THREAD MODE. DO NOT RUN WITH MULTIPLE CORES.')
            todfile = h5py.File(f.format(i),'w') 

                      
        # Create the structures for the file
        if simInfo.Parameters['Inputs']['sim_Receiver']:
            outputTOD = todfile.create_dataset('spectrometer/tod',(nFeeds, 1,  int(simInfo.nchannels), nSamples,), dtype=np.float64)
        outputMJD   = todfile.create_dataset('spectrometer/MJD',(nSamples,), dtype='d')
        outputFeed  = todfile.create_dataset('spectrometer/feeds', (nFeeds,), dtype='int')
        outputBands  = todfile.create_dataset('spectrometer/bands', (1,), dtype='S5' )
        outputFreq  = todfile.create_dataset('spectrometer/frequency', (1, int(simInfo.nchannels), ), dtype='d')
        outputTimeAvg  = todfile.create_dataset('spectrometer/time_average', (nFeeds, 1, int(simInfo.nchannels), ), dtype='d')
        outputBandAvg  = todfile.create_dataset('spectrometer/band_average', (nFeeds, 1, nSamples, ), dtype='d')
        
        outputMJD2   = todfile.create_dataset('pointing/MJD',(nSamples,), dtype='d')
        outputAz   = todfile.create_dataset('pointing/azActual',(nSamples,), dtype='d')
        outputEl   = todfile.create_dataset('pointing/elActual',(nSamples,), dtype='d')
        #outputAzE   = todfile.create_dataset('pointing/azEncoder',(nSamples,), dtype='d')
        #outputElE   = todfile.create_dataset('pointing/elEncoder',(nSamples,), dtype='d')
        
        # Create the incidental information
        header = todfile.create_group('HEADER')

        header.attrs['npix'] = simInfo.npix
        header.attrs['pixelMode'] = simInfo.Parameters['Mapping']['mode']
        header.attrs['sampleRate'] = simInfo.sampleRate
        header.attrs['speed'] = simInfo.Parameters['Telescope']['speed']

        comap = todfile.create_group('comap')
        comap.attrs['comment'] = '{}'.format(simInfo.Parameters['Observations']['mode'])
        comap.attrs['level'] = 1
        comap.attrs['dada_files'] = 'nothing'
        comap.attrs['platform'] = 'simulated'
        comap.attrs['version'] = '2018-04-19'
        comap.attrs['obsId'] = 0
        comap.attrs['iqcalId'] = 0
        comap.attrs['source'] = 'ra: {:.2f} dec: {:.2f}'.format( simInfo.Parameters['Observations']['racen'],simInfo.Parameters['Observations']['deccen'])
        comap.attrs['features'] = 32
        
        todfileList += [todfile]

    return todfileList

def COMAP_WriteToFileObject(todfileList, simInfo, iblock, data):# TOD_All, TOD_Coordinates, TOD_Pixels):


    start = simInfo.blocks.nFileStart[iblock]
    end   = start + simInfo.blocks.nSamples[iblock]
    iObs = int(simInfo.blocks.iObsIndex[iblock])
    fObs = int(simInfo.blocks.fObsIndex[iblock])

    #print ('WRITING', len(todfileList), simInfo.nFiles, simInfo.blocks.fileID[iblock])
    todfile = todfileList[simInfo.blocks.fileID[iblock]]
    nSamples = simInfo.blocks.nSamples[iblock]

    # Which feed are we using?
    iFeed = simInfo.recIDs[iObs] + simInfo.nTelescopes * simInfo.telIDs[iObs]
    
    chanEdges = np.linspace(simInfo.Parameters['Telescope']['minFreq'],
        simInfo.Parameters['Telescope']['maxFreq'],
        simInfo.Parameters['Telescope']['nchannels']+1)
    chanMids = (chanEdges[1:] + chanEdges[:-1])/2.
    chanWidth = chanEdges[1] - chanEdges[0]
    
    if simInfo.Parameters['Inputs']['sim_Receiver']:
        for i in range(data['TOD'].shape[0]):
            todfile['spectrometer/tod'][iFeed,0,i,start:end] = data['TOD'][i,:nSamples].astype('d')
    todfile['spectrometer/MJD'][start:end] = data['JD'][:nSamples] - 2400000.5
    todfile['spectrometer/feeds'][iFeed] = iFeed + 1
    todfile['spectrometer/bands'][0] = b'A:LSB'
    todfile['spectrometer/frequency'][0,:] = chanMids
    todfile['spectrometer/time_average'][iFeed, 0, :] = 1./simInfo.Parameters['Telescope']['sampleRate']
    todfile['spectrometer/band_average'][iFeed, 0, :nSamples]  = chanWidth
    
    todfile['pointing/MJD'][start:end] = data['JD'][:nSamples]  - 2400000.5
    todfile['pointing/azActual'][start:end] = data['AZ'][:nSamples] 
    todfile['pointing/elActual'][start:end] = data['EL'][:nSamples] 
    

def CloseTODObjects(todfilelist):
    
    for todfile in todfilelist:
        todfile.close()
