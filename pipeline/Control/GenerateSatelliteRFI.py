import numpy as np
import healpy as hp
from mpi4py import MPI
from jdcal import gcal2jd
from matplotlib import pyplot
from matplotlib.colors import LogNorm
import scipy.special as sp
from scipy.interpolate import interp1d

import sys
import time
import os

#from pipeline.Observatory.Receivers import Noise
from pipeline.Observatory.Telescope import Coordinates
from pipeline.Observatory.Telescope import Pointing
from pipeline.Observatory.Telescope import Observations

from pipeline.Tools import Parsing
from pipeline.Tools import Container
from pipeline.Tools import FileTools


def GenerateSatellitesRFI(simInfo, az, el, jd, groups, lo=0, hi=1):
    """
    Generate azel and ra/dec for each horn.
    """    
    
    nchannels = int(simInfo.Backends[0].nchannels)
    outputs = np.empty((simInfo.totalSamps, nchannels+2), dtype='f')
    coords  = np.zeros((simInfo.totalSamps, 1))
    iStart = 0
    iEnd = 0
    for i, j in enumerate(range(lo, hi)):
        telescope = simInfo.Telescopes[simInfo.Blocks[j].telescope]
        receiver  = simInfo.Receivers[simInfo.Blocks[j].telescope][simInfo.Blocks[j].receiver]
        sampleRate = telescope.samplerate
        
        iEnd += simInfo.Blocks[j].size
        
        o, d, a, e = run_sats.run_sats(az[iStart:iEnd], el[iStart:iEnd], jd[iStart:iEnd],
                                       simInfo.Parameters['RFI']['beamfile'],
                                       simInfo.Parameters['RFI']['beamCentralFrequency'],
                                       simInfo.Parameters['RFI']['beamFWHM'],
                                       groups,
                                       simInfo.Parameters['RFI']['satellitePower'],
                                       simInfo.Parameters['RFI']['satelliteSigma'],
                                       simInfo.Parameters['RFI']['satelliteGain'],
                                       telescope.Backends.maxFreq,
                                       telescope.Backends.minFreq,
                                       telescope.Backends.nchannels,
                                       telescope.longitude,
                                       telescope.latitude,
                                       telescope.altitude)    

        outputs[iStart:iEnd, :nchannels] = o
        outputs[iStart:iEnd, nchannels] = simInfo.Blocks[j].telescope
        outputs[iStart:iEnd, nchannels+1]= simInfo.Blocks[j].receiver 
        coords[iStart:iEnd, 0] = np.min(d, axis=0)

        #coords[iStart:iEnd, 1] = a
        #coords[iStart:iEnd, 2] = e
        iStart = iEnd*1.
    

    return outputs, coords

    
