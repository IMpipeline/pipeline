import numpy as np
from matplotlib import pyplot

import sys
import time

from mpi4py import MPI
from jdcal import gcal2jd
from pipeline.Observatory.Telescope import Pointing
from pipeline.Observatory.Telescope import Observations
from pipeline.Observatory.Telescope import Coordinates
from pipeline.Tools import General

from pipeline.Tools import CartPix

import healpy as hp
    
ObservationFunctions = {'horizonraster': Pointing.HorizonRaster,
                        'drift': Pointing.Drift,
                        'raskyraster': Pointing.RASkyRaster,
                        'decskyraster': Pointing.DecSkyRaster,
                        'lissajous': Pointing.Lissajous,
                        'lissajousazel': Pointing.LissajousAzEl,
                        'circular': Pointing.Circular,
                        'circularazel': Pointing.CircularAzEl,
                        'continuous': Pointing.Continuous}


def GenerateCoordinates(simInfo, TOD_Coordinates, TOD_Pixels, iblock):
    """
    Generate azel and ra/dec for each horn.

    TOD_Coordinates Indices:
    0 - az
    1 - el
    2 - jd
    3 - ra
    4 - dec
    5 - pixel coordinate
    """    
    
    
    iblock = int(iblock)
    # So I can get from the block the list of observations, receivers, and telescopes to iterate over.
    # To read this can be a bit confusing!
    # SimInfo.obsevations -> An array of [nObs, values]
    # SimInfo.obsIDs -> the array that repeats for each receiver from 0->nObs
    # We want all the observation values for indices block[0] to block[1]
    # The same is then repeated for receivers and telescopes.
    iObs = int(simInfo.blocks.iObsIndex[iblock])
    fObs = int(simInfo.blocks.fObsIndex[iblock])
    nObs = fObs-iObs

    observations = simInfo.observations[simInfo.obsIDs[iObs:fObs]]
    receivers    = simInfo.receivers[simInfo.recIDs[iObs:fObs]]
    telescopes   = simInfo.telescopes[simInfo.telIDs[iObs:fObs]] 

    iStart = 0
    iEnd = 0
    for j in range(nObs): # Loops over observations in this block
        thisObservation = observations[j]
        thisReceiver    = receivers[j]
        thisTelescope   = telescopes[j] 
        
        # Start and end times of observations in this block in seconds
        if j == 0:
            startTime = simInfo.blocks.iObsTime[iblock]  # The first observation may not start at zero
        else:
            startTime = 0
            
        if j == nObs - 1:
            endTime =  simInfo.blocks.fObsTime[iblock] # The last observation may not be completed.
        else:
            endTime = thisObservation[1] 
            
        nSamples = General.SafeInt((endTime - startTime)*simInfo.sampleRate)
        iEnd += nSamples  
        
        ####
        # From this point on we use the start/end times to calculate the actual observations
        ####
            
        mode = thisObservation[0]
        # Other than the first element of thisObservation (assumed to be the mode), all other elements are arbitrary and
        # the function is assumed to know what they should be.
        # The function should always return the BORESIGHT of the telescope in question in az/el and jd (even if the ra/dec is implicitly calculated)

        az, el, jd = ObservationFunctions[mode.lower()](simInfo, startTime, endTime, thisTelescope[0], thisTelescope[1], *thisObservation)
        runSize = TOD_Coordinates[0,iStart:iEnd].size
        az = az[:runSize]
        el = el[:runSize]
        jd = jd[:runSize]
                
        # CHECK WHY iEND/iStart not matching with timeStarts
        if (runSize) - az.shape[0] > 0:
            diff = (runSize) - az.shape[0]
            djd = np.abs(jd[0] - jd[1])
            az = np.concatenate((az, np.ones(diff)*az[-1]))
            el = np.concatenate((el, np.ones(diff)*el[-1]))
            jd = np.concatenate((jd, np.ones(diff)*jd[-1] + (np.arange(diff)+1)*djd))
        # Calculate the offsets
        az, el = Observations.FocalPlaneOffsets(az*np.pi/180.,
                                                el*np.pi/180.,
                                                simInfo.Parameters['Beam']['focalLength'],
                                                thisReceiver[1],
                                                thisReceiver[2])

        az *= 180./np.pi
        el *= 180./np.pi

        TOD_Coordinates[0,iStart:iEnd] = az
        TOD_Coordinates[1,iStart:iEnd] = el
        TOD_Coordinates[2,iStart:iEnd] = jd
             
        # Convert to sky coordinates
        
        ra, dec = Coordinates._hor2equ(az,
                                       el,
                                       jd,
                                       thisTelescope[1],
                                       thisTelescope[0])

                                       
        # if j==0:
        
        TOD_Coordinates[3,iStart:iEnd] = ra
        TOD_Coordinates[4,iStart:iEnd] = dec

        TOD_Coordinates[5,iStart:iEnd] = Coordinates._pang(el, dec, thisTelescope[1])

        if simInfo.Parameters['Mapping']['mode'].upper() == 'HEALPIX':
            try:
                TOD_Pixels[iStart:iEnd] = hp.ang2pix(int(simInfo.Parameters['Mapping']['hp_nside']),
                                                            (90.- dec)*np.pi/180., 
                                                            ra * np.pi/180.)
            except:
                pyplot.plot(ra, dec)
                pyplot.show()
        elif simInfo.Parameters['Mapping']['mode'].upper() == 'CARTESIAN':
            TOD_Pixels[iStart:iEnd] = CartPix.ang2pix([simInfo.Parameters['Mapping']['ct_naxis1'], simInfo.Parameters['Mapping']['ct_naxis2']],
                                                             [simInfo.Parameters['Mapping']['ct_cdelt1'], simInfo.Parameters['Mapping']['ct_cdelt2']],
                                                             [simInfo.Parameters['Mapping']['ct_crval1'], simInfo.Parameters['Mapping']['ct_crval2']],
                                                             dec,  ra)
        else:
            print ('# INVALID MAPPING MODE INPUT {}, PLEASE CHOOSE CARTESIAN OR HEALPIX'.format(simInfo.Parameters['Mapping']['mode'].upper()))
            raise ValueError
            
        iStart += nSamples 
