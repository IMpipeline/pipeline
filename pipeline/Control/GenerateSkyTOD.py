import numpy as np
from matplotlib import pyplot

from pipeline.Tools import Parsing
from pipeline.Tools import Container
import sys
import time

from mpi4py import MPI
from jdcal import gcal2jd
#from pipeline.Observatory.Receivers import Noise
#from pipeline.Observatory.Telescope import Coordinates
#from pipeline.Observatory.Telescope import Pointing
#from pipeline.Observatory.Telescope import Observations
import healpy as hp

from scipy.interpolate import RectBivariateSpline

def GenerateSkyTOD(simInfo, Sky, Coordinates):
    """ 
    
    """
    comm = MPI.COMM_WORLD
    rank = comm.rank
    size = comm.size
    ntemp = np.zeros((Sky.Total.shape[0], Coordinates.shape[1]))

    if simInfo.Parameters['Mapping']['mode'].lower() == 'healpix':
        nside = int(np.sqrt(Sky.Total.shape[1]/12.))
        for i in range(Sky.Total.shape[0]):
            ntemp[i,:] = hp.get_interp_val(Sky.Total[i,:], 
                                           Coordinates[0,:],
                                           Coordinates[1,:], 
                                           lonlat=True)

    else:
        for i in range(Sky.Total.shape[0]):
            ntemp[i,:] = Sky.splines[i].ev(Coordinates[0,:], Coordinates[1,:])

        # naxis = np.array([simInfo.Parameters['Mapping']['ct_naxis1'],simInfo.Parameters['Mapping']['ct_naxis2']]).astype(int)
        # cdelt = [simInfo.Parameters['Mapping']['ct_cdelt1'],simInfo.Parameters['Mapping']['ct_cdelt2']]
        # crval = [simInfo.Parameters['Mapping']['ct_crval1'],simInfo.Parameters['Mapping']['ct_crval2']]
        # glMap = np.linspace(crval[0] - cdelt[0]*naxis[0]/2., crval[0] + cdelt[0]*naxis[0]/2., naxis[0])
        # gbMap = np.linspace(crval[1] - cdelt[1]*naxis[1]/2., crval[1] + cdelt[1]*naxis[1]/2., naxis[1])

        # glAll, gbAll = np.meshgrid(glMap, gbMap)
        # test = Sky.splines[0].ev(glAll, gbAll)
        # from matplotlib import pyplot
        # cmap = pyplot.get_cmap('Greys')
        # pyplot.scatter(glAll.flatten()[::10], gbAll.flatten()[::10], marker=',', c=test.flatten()[::10], cmap=cmap)

        # pyplot.scatter(Coordinates[0,:],Coordinates[1,:],marker=',', c=ntemp[i,:])

        # pyplot.show()

    return ntemp
    #return Sky.Total[:,  Coordinates.astype('int')]
