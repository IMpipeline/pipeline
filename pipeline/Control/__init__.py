"""
Control.py

Control functions are defined as those that will link directly into 
the run.py script. They are integral components to the pipeline and 
cannot be run independently. They will however link to each of the
individual components.

Control functions should take only one input, the main data container.
Control functions should return only one well defined output.

Indepentent functions should always have a full suite of inputs.

Head : Pipeline Dependent : Pipeline Independent
run -> Control.Something(infoContainer) -> Coordinates
                         -> Noise
                         -> Tools


"""

from pipeline.Control.GenerateCoordinates import *
from pipeline.Control.GenerateFNoise import *
from pipeline.Control.GenerateSkyTOD import *
from pipeline.Control.GenerateFileStructure import *
from pipeline.Control.GenerateSky import *
from pipeline.Control.GenerateSatelliteRFI import *
from pipeline.Control.SimInfoClass import *
from pipeline.Control import GenerateTODFiles
