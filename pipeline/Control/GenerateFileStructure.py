import sys
import os
from mpi4py import MPI

def GenerateFileStructure():
    """
    Creates simulation file structure.

    Arguments - simInfo
    """
    print ('---------') # Break up terminal comments.
    comm = MPI.COMM_WORLD
    rank = comm.rank
    size = comm.size 
    
    #Loop through outputs
    sim_dirs = ['SKY_MODEL',
                'TOD_MAPS',
                'TOD',
                'OBSERVATIONS',
                'COMPSEP_MAPS',
                'POWER_SPECTRA',
                'CONFIGS']

    if rank == 0:
        for d in sim_dirs:
            print (d)
            if not os.path.exists('./{}'.format(d)):
                print ('# CREATING DIRECTORY: ./{}'.format(d))
                os.makedirs('./{}'.format(d))
            else:
                print ('# DIRECTORY: ./{} ALREADY EXISTS'.format(d))
