"""
SimInfo is a container class that should hold all the pipeline specific information.

>This should never need to be touched.<

"""

import numpy as np
from mpi4py import MPI
from pipeline.Tools import Container, General
from pipeline.Observatory.Telescope import Observations
from decimal import Decimal
import sys 
from pipeline.Observatory.Telescope import Coordinates, Pointing
from pipeline.Observatory.Beam import BeamClass

class BlockObject:

    def __init__(self, nBlocks):

        self.iObsIndex  = np.zeros(int(nBlocks), dtype='int')
        self.fObsIndex  = np.zeros(int(nBlocks), dtype='int')
        self.iObsTime   = np.zeros(int(nBlocks))
        self.fObsTime   = np.zeros(int(nBlocks))
        self.nSamples   = np.zeros(int(nBlocks), dtype='int')
        self.fullObsTime   = np.zeros(int(nBlocks), dtype='int')
        
        self.nFileStart = np.zeros(int(nBlocks), dtype='int')
        self.fileID     = np.zeros(int(nBlocks), dtype='int')
        
        self.size = int(nBlocks)

        self._arrList = [self.iObsIndex, self.fObsIndex, self.iObsTime,
                         self.fObsTime, self.nSamples, self.nFileStart, self.fileID]


    def TileBlocks(self, step, tileLength):

        for a in self._arrList:
            a[:] = np.tile(a[:int(step)], int(tileLength))



def ReadDat(filename):
    """
    File specifically for reading in the observatory definition files.
    
    """
    
    types = [int, float, str]

    f = open(filename, 'r')
    k = 0
    values = []
    for line in f:
        line_nocomments = line.split('#')[0].strip()
            
        if len(line_nocomments) > 0:
            if k == 0:
                keys = line_nocomments.split()
            else:
                tvals = np.array(line_nocomments.split(), dtype='object')
                for i, t in enumerate(tvals):
                    for c in types:
                        try:
                            tvals[i] = c(t)
                            break
                        except ValueError:
                            pass
                            
                values += [tvals]
            k += 1

    f.close()
    return keys, np.array(values)


class SimInfo(object):
    
    def __init__(self, Parameters):
        """
        A pipeline specific container for holding receiver information.
        
        
        """
        
        comm = MPI.COMM_WORLD
        rank = comm.rank
        size = comm.size 
                
        # Constants that are required
        self.totalSamps = 0.
        
        #super(Container.Container, self).__setattr__('data',  dataDict)
        
        self.Parameters = Parameters
        self.sampleRate = self.Parameters['Telescope']['sampleRate']
        self.nchannels = int(self.Parameters['Telescope']['nchannels'])
        self.maxFreq   = self.Parameters['Telescope']['maxFreq']
        self.minFreq   = self.Parameters['Telescope']['minFreq']
        self.chanWidth = (self.maxFreq - self.minFreq)/float(self.nchannels)

        self.nu = np.arange(self.nchannels)*self.chanWidth + self.minFreq + self.chanWidth/2.        
        
        #####################
        # SETUP BEAM OBJECT #
        # 
        # - Contains Healpix/Cartesian beam transforms + beam model functions #
        #####################
        self.Beam = BeamClass.Beam(self.Parameters['Beam']['model'],
                                   self.Parameters['Beam']['fwhm'],
                                   self.Parameters['Beam']['f0'],
                                   self.maxFreq*1e-6,
                                   self.minFreq*1e-6,
                                   self.nchannels,
                                   self.Parameters['Mapping']['hp_nside'],
                                   beamfile=self.Parameters['Beam']['beamfile'])

        

        # Constants for output files
        self.skyfileformat = '{}/{}_{}_{}.fits'

        # Save the number of pixels in the output maps
        if self.Parameters['Mapping']['mode'].upper() == 'HEALPIX':
            self.npix = int(self.Parameters['Mapping']['hp_nside']**2 * 12)
        elif self.Parameters['Mapping']['mode'].upper() == 'CARTESIAN':
            self.npix = int(self.Parameters['Mapping']['ct_naxis1']*self.Parameters['Mapping']['ct_naxis2'])
        else:
            print ('# INVALID MAPPING MODE INPUT {}, PLEASE CHOOSE CARTESIAN OR HEALPIX'.format(simInfo.Parameters['Mapping']['mode'].upper()))
            raise ValueError

        
        # Read in telescope, receiver and observations config files
        ktele,    self.telescopes   = ReadDat(Parameters['Telescopes']['configurationFile'])
        krcvrs,   self.receivers    = ReadDat(Parameters['Receivers']['configurationFile']) 


        # Need to generate the observation list if one is pregenerated
        if isinstance(Parameters['Observations']['configurationFile'], type(None)):
            obsFile = 'CONFIGS/{}_{}_Observations.dat'.format(Parameters['Inputs']['sim_ID'], Parameters['Inputs']['sim_IDtod'])
            Parameters['Observations']['configurationFile'] = obsFile
            if rank == 0:
                #self.GenerateObsList()
                self.GenerateObsList_test()

        #self.GenerateObsList_test()
        
        Done = np.array([0], dtype='i')
        comm.Bcast([Done, MPI.INT], root=0)

        kobs,     self.observations = ReadDat(Parameters['Observations']['configurationFile'])

        self.nTelescopes = self.telescopes.shape[0] # Number of telescopes
        self.nReceivers  = self.receivers.shape[0] # Number of receivers per telescope 
        self.nObs        = self.observations.shape[0] # Number of observations per receiver

        # Total length of all observations per receiver
        lenIndex = 1 

        obsLens = self.observations[:,lenIndex].flatten()
        totalObsLen = np.sum(self.observations[:,lenIndex])
        


        # These arrays calculate for all nObs*nRec*nTele observations the observation, receiver and telescope 
        # I am in block[i], it has obs[10,11,12], therefore rec[10,11,12] and tele[10,11,12]
        self.obsIDs = np.tile(np.arange(self.nObs), self.nReceivers*self.nTelescopes).astype(int)
        self.recIDs = np.repeat(np.arange(self.nReceivers), self.nObs*self.nTelescopes).astype(int)
        self.telIDs = np.repeat(np.arange(self.nTelescopes), self.nObs*self.nReceivers).astype(int)

        
        bsize = Parameters['FNoise']['blockSize']
        # Number of blocks per full set of observations is:
        nBlocksPerObs = int(np.ceil(totalObsLen/float(bsize)))
        if nBlocksPerObs == 0: # If the block length is way shorter than the obs length
            nBlocksPerObs = 1
            bsize = totalObsLen

            
        # Set some constants related to samples
        self.nSampsPerReceiver = General.SafeInt(totalObsLen*self.sampleRate)
        self.maxBlockSize = int(bsize*self.sampleRate)
        self.totalSampsAllBlocks = int(totalObsLen*self.sampleRate*self.nTelescopes*self.nReceivers)
            
        # Take the cumalative sum of the blocks array and obs array
        # E.g. these arrays contain the running clock time of each receivers observations in blocks and observations
        csBlockLens = np.concatenate(([0], np.cumsum(np.ones(nBlocksPerObs)*bsize) ))
        csObsLens   = np.concatenate(([0], np.cumsum(obsLens).astype('float') ))
        csBlockLens[-1] = csObsLens[-1] # The last block should end at the same time as the observations (may be shorter)        
        

        # CALCULATE THE SIZE OF EACH FILE
        # A FILE CANNOT SPAN MORE THAN 1 TELESCOPE AND HAS A FIXED NUMBER OF BLOCKS PER OBSERVING QUEUE
        bPerFile = int(Parameters['Outputs']['blocksPerFile'])
        self.nFiles = General.SafeInt( np.ceil(float(nBlocksPerObs)/bPerFile) ) # No. files for tele1 and receiver 1 e.g. per obs block
        self.fileSizes = np.zeros(self.nFiles, dtype='int')
        for i in range(self.nFiles):
            lo = i*bPerFile
            if i < self.nFiles - 1:
                hi = (i+1) * bPerFile
            else:
                hi = csBlockLens.size-1

            self.fileSizes[i] = General.SafeInt((csBlockLens[hi]-csBlockLens[lo])*self.sampleRate)

        csFileLens = np.concatenate(([0], np.cumsum(self.fileSizes) ))
                
        # Now we want to calculate the information required by each block
        # Find out which observations fall into which blocks by comparing start/end times
        self.blocks = BlockObject(nBlocksPerObs*self.nReceivers*self.nTelescopes)
        flast = 0
        lo = 0
        for i in range(nBlocksPerObs):
    
            start = csBlockLens[i] # 0 -> time_i
            end   = csBlockLens[i+1] # time_i -> time_i+1
            # test to see which observations are greater than start, and less than end
    
            # How the block is calculating the times:
            # start                       end
            #   |              b1          |
            # |           o1    |  o2  |   o3   |
            # cs0              cs1    cs2      cs3
    
            # start                          end
            #   |              b1             |
            #     |           o1    |  o2  |   o3   |
            #    cs0              cs1    cs2      cs3

            istart= np.max(np.where(( start >= csObsLens ))[0]) 
            iend  = np.min(np.where(( end <= csObsLens))[0])
        
            # Calculate the start and end time in seconds for the first and last obs
            startTime = start - csObsLens[istart]
            endTime   = end - csObsLens[iend-1]  

            self.blocks.iObsIndex[i] = int(istart )    # Starting index of observations
            self.blocks.fObsIndex[i] = int(iend )      # Ending index of observations
            self.blocks.iObsTime[i]  = startTime  # Second to start first obs at
            self.blocks.fObsTime[i]  = endTime    # Second to end last obs at
            nObsPerBlock = (iend-1)-istart
            obsOffset = 0
            if startTime > 0:
                nObsPerBlock -= 1 # remove incomplete first obs
                obsOffset = 1
            if endTime < (csObsLens[iend]-csObsLens[iend-1]):
                nObsPerBlock -= 1

            if nBlocksPerObs == 1:
                nObsPerBlock = 0
                
            fullObsTime = csObsLens[istart+nObsPerBlock+obsOffset] - csObsLens[istart+obsOffset]
            #print(end-start, fullObsTime)
            self.blocks.nSamples[i]  = General.SafeInt((end - start)*self.sampleRate)  # number of samples
            #print(self.blocks.nSamples[i])
            self.blocks.fullObsTime[i] = fullObsTime
            
            fstart= np.max(np.where(( start*self.sampleRate >= csFileLens ))[0]) 
            if fstart > flast:
                lo = i*1
                flast = fstart

            self.blocks.fileID[i]    = fstart
            self.blocks.nFileStart[i] = General.SafeInt((csBlockLens[i] - csBlockLens[lo])*self.sampleRate)
            #self.blocks.nFileStart[i]   = General.SafeInt(start*int(self.sampleRate)) # Absolute sample position
        
        # Now we want to repeat the blocks nRec*nTelescopes times
        # The last loop calculated the blocks needed for all the observations on a given receiver.
        # We want to repeat this for all nRec*nTelescopes receivers in the simulation
        steps = np.repeat(np.arange(self.nReceivers*self.nTelescopes)*self.nObs, nBlocksPerObs)
        self.blocks.TileBlocks(nBlocksPerObs, self.nReceivers*self.nTelescopes)
        self.blocks.iObsIndex += steps.astype('int') # Remember 0 and 1 are the start and end INDICES of the observations
        self.blocks.fObsIndex += steps.astype('int')

        steps = np.repeat(np.arange(self.nTelescopes)*self.nFiles, nBlocksPerObs*self.nReceivers)
        #self.blocks.fileID += steps.astype('int')

        # Now we need nTelescopes of these files
        self.nFiles *= self.nTelescopes
        self.fileSizes = np.tile(self.fileSizes, self.nTelescopes)

        # Done!
            
    def GenerateObsList_test(self):
        """
        Experimental obs list generation for Raster scans.  Should hopefully produce more reliable
        outputs.
        """
        meanLon = np.mean(self.telescopes[:,0])
        meanLat = np.mean(self.telescopes[:,1])
        mode   = self.Parameters['Observations']['mode']
        Pointing.ObservationWriteFunctions[mode.lower()](self.Parameters, meanLon, meanLat)
            
            
    def GenerateObsList(self):
        """
        Generates a the Observations.dat and saves it to CONFIGS.
        
        It will read the Observations section of parameters and
        calculate the optimal scanning strategy that ensures that each
        observation are within limits of a specified min/max
        elevation, and parallactic angle. 

        """

        mode   = self.Parameters['Observations']['mode']
        obsLen = self.Parameters['Observations']['obsLen']

        ra = self.Parameters['Observations']['racen']
        dec = self.Parameters['Observations']['deccen']
        ijd    = self.Parameters['Observations']['ijd']
        ejd    = self.Parameters['Observations']['ejd']

        emin = self.Parameters['Observations']['elmin']
        emax = self.Parameters['Observations']['elmax']
        pmin = self.Parameters['Observations']['pmin']


        offsets = self.Parameters['Observations']['offsets']

        # convert to az/el
        meanLon = np.mean(self.telescopes[:,0])
        meanLat = np.mean(self.telescopes[:,1])

        az, el, jd, parangle = Observations.TransitPath(ijd, ra, dec, meanLat, meanLon)

        minTransitEl = np.min(el)
        if (minTransitEl < emin):
            sourceSets = True
        else:
            sourceSets = False

        # Open configuration file
        f = open(self.Parameters['Observations']['configurationFile'],'w')
        # Repeat N times for the observations
        i = 0

        #Count number of days?
        dayjd = ijd*1.
        day = 1
        jd = ijd * 1.
        obsLen = np.inf
        elast = -10
        newDay = False
        jd, obsLen, lineoutput = Pointing.ObservationWriteFunctions[mode.lower()](self.Parameters, ijd,  meanLon, meanLat)
        stop
        while ijd < ejd:


            jd, obsLen, lineoutput = Pointing.ObservationWriteFunctions[mode.lower()](self.Parameters, ijd,  meanLon, meanLat)
            
            if ((ijd - dayjd) > 1):
                day += 1
                dayjd = ijd * 1.
                newDay = True

            # If source is rising, offset the starting time from the previous day
            if  (np.abs(ijd - jd) > 2.*obsLen/24./3600.) & sourceSets: #(np.mod(day, 2) == 0) &
                op = obsLen/24./3600.* (np.mod(day, offsets))/offsets
                jd, obsLen, lineoutput = Pointing.ObservationWriteFunctions[mode.lower()](self.Parameters, jd + op,  meanLon, meanLat)

            if (sourceSets == False) & newDay: # (np.mod(day, 2) == 0) & 
                op = obsLen/24./3600.* (np.mod(day, offsets))/offsets
                jd, obsLen, lineoutput = Pointing.ObservationWriteFunctions[mode.lower()](self.Parameters, jd + op,  meanLon, meanLat)
                newDay = False

            ijd = jd + obsLen/24./3600.

            if i == 0:
                kstring = [lineoutput[j][0]+' ' for j in range(len(lineoutput))] + ['\n']
                f.write(''.join(kstring))
                    
            vstring = [str(lineoutput[j][1])+' ' for j in  range(len(lineoutput))] + ['\n']
            f.write(''.join(vstring))

            i += 1
        
        f.close()
