
import numpy as np
from matplotlib import pyplot

from pipeline.Tools import Parsing
from pipeline.Tools import Container
import sys
import time

from mpi4py import MPI
from jdcal import gcal2jd
#from pipeline.Observatory.Receivers import Noise
#from pipeline.Observatory.Telescope import Coordinates

#import Pointing
#import MPITools
#import Observations


def GenerateFNoise(simInfo, fnoise, FNoiseObject, iblock):
    """
    1/f noise control function.
    """

    if isinstance(simInfo.Parameters['FNoise']['dknee'], type(None)):
        dknee = 1.
    else:
        dknee = 1. + np.random.uniform(low=simInfo.Parameters['FNoise']['dknee']/2.,
                                       high=simInfo.Parameters['FNoise']['dknee']/2.)

    fnoise[:, :] = FNoiseObject.Realisation(int(simInfo.nchannels), simInfo.blocks.nSamples[iblock], dknee) 
