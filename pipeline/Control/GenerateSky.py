from pipeline.Observatory.Sky import Sky
from pipeline.Observatory.Sky import Synchrotron, AME, Jupiter, Point, COMAPGPS
from pipeline.Observatory.Sky import FreeFree
from pipeline.Observatory.Sky import PointSources
from pipeline.Observatory.Sky import HI
from pipeline.Observatory.Sky import CO
import os
from pipeline.Tools import Container
from mpi4py import MPI
import healpy as hp
import numpy as np
from astropy.io import fits as pyfits

from pipeline.Tools import FileTools

def GenerateSky(simInfo):
    
    comm = MPI.COMM_WORLD
    rank = comm.rank
    size = comm.size 
    
        
    ############################################
    # HERE WE GENERATE ALL THE DIFFERENT SKY MODEL COMPONENTS
    # WE NEED A MODEL FOR EACH TYPE OF OPTICS AND BACKEND
    # THE CODE SHOULD ALSO CHECK IF THE OUTPUT FILE EXISTS BEFORE RUNNING THE GENERATE CODE
    ############################################
    
    ########################################### !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    Sky = Container.Container({'Total':None}) # ALL NODES SHOULD HAVE Sky.Total
    ########################################### !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    #######
    # ADD A CREATE ONLY CLAUSE FOR THE FOREGROUNDS HERE
    ######
    ###############################
    # DO WE WANT COMAPGPS?
    ###############################
    try:
        simCOMAPGPS, readCOMAPGPS = simInfo.Parameters['Inputs']['sim_COMAPGPS'], simInfo.Parameters['Inputs']['read_COMAPGPS']
    except KeyError:
        simCOMAPGPS, readCOMAPGPS = False, False

    if simCOMAPGPS and rank == 0:
        writeFile = '{}/{}_COMAPGPS_{}.fits'.format('./SKY_MODEL', 
                                                    simInfo.Parameters['Inputs']['sim_IDsky'],
                                                    simInfo.Parameters['COMAPGPS']['model'])
        print ('# GENERATING NEW COMAPGPS MAP'     )                                                                           
        COMAPGPS.GenerateCOMAPGPS(simInfo,
                                  writeFile=writeFile,
                                  write=True)

    # FOR READING IN THE DATA ONLINE NOT SIMULATING A NEW MAP
    if simCOMAPGPS | readCOMAPGPS:
        writeFile = '{}/{}_COMAPGPS_{}.fits'.format('./SKY_MODEL', simInfo.Parameters['Inputs']['sim_IDsky'],
                                                       simInfo.Parameters['COMAPGPS']['model'])
        if rank == 0:
            if simInfo.Parameters['Mapping']['mode'].upper() == 'HEALPIX':
                h = pyfits.open(writeFile)
                nchans = len(h[1].columns)
                Sky.COMAPGPS = np.array(hp.read_map(writeFile, np.arange(simInfo.nchannels, dtype='i')))
            else:
                Sky.COMAPGPS = FileTools.ReadH5Py(writeFile)['maps']
                nchans = Sky.COMAPGPS.shape[0]
                
            if nchans != simInfo.nchannels:
                print ('# REQUESTED BACKEND FREQUENCY CHANNELS DOES NOT EQUAL FREQUENCY CHANNELS OF COMAPGPS MAP')
                raise ValueError
        else:
            Sky.COMAPGPS = None


    ###############################
    # DO WE WANT Jupiter?
    ###############################
    try:
        if simInfo.Parameters['Inputs']['sim_Jupiter'] and rank == 0:
            writeFile = '{}/{}_Jupiter_{}.fits'.format('./SKY_MODEL', 
                                                           simInfo.Parameters['Inputs']['sim_IDsky'],
                                                           simInfo.Parameters['Jupiter']['model'])
            print ('# GENERATING NEW JUPITER MAP'     )                                                                           
            Jupiter.GenerateJupiter(simInfo,
                                    writeFile=writeFile,
                                    write=True)

        # FOR READING IN THE DATA ONLINE NOT SIMULATING A NEW MAP
        if simInfo.Parameters['Inputs']['sim_Jupiter'] | simInfo.Parameters['Inputs']['read_Jupiter']:
            writeFile = '{}/{}_Jupiter_{}.fits'.format('./SKY_MODEL', simInfo.Parameters['Inputs']['sim_IDsky'],
                                                       simInfo.Parameters['Jupiter']['model'])
            if rank == 0:
                if simInfo.Parameters['Mapping']['mode'].upper() == 'HEALPIX':
                    h = pyfits.open(writeFile)
                    nchans = len(h[1].columns)
                    Sky.Jupiter = np.array(hp.read_map(writeFile, np.arange(simInfo.nchannels, dtype='i')))
                else:
                    Sky.Jupiter = FileTools.ReadH5Py(writeFile)['maps']
                    nchans = Sky.Jupiter.shape[0]
                
                if nchans != simInfo.nchannels:
                    print ('# REQUESTED BACKEND FREQUENCY CHANNELS DOES NOT EQUAL FREQUENCY CHANNELS OF JUPITER MAP')
                    raise ValueError
            else:
                Sky.Jupiter = None

    except KeyError:
        pass


    ###############################
    # DO WE WANT A POINT SOURCE?
    ###############################
    try:
        if simInfo.Parameters['Inputs']['sim_Point'] and rank == 0:
            writeFile = '{}/{}_Point_{}.fits'.format('./SKY_MODEL', 
                                                     simInfo.Parameters['Inputs']['sim_IDsky'],
                                                     simInfo.Parameters['Point']['model'])
            print ('# GENERATING NEW Point Source MAP'     )                                                                           
            Point.GeneratePoint(simInfo,
                                writeFile=writeFile,
                                write=True)
    except KeyError:
        pass

    ###############################
    # DO WE WANT A SYNCHROTRON COMPONENT?
    ###############################
    try: 
        if simInfo.Parameters['Inputs']['sim_Synchrotron'] and rank == 0:
            writeFile = '{}/{}_Synchrotron_{}.fits'.format('./SKY_MODEL', 
                                                        simInfo.Parameters['Inputs']['sim_IDsky'],
                                                           simInfo.Parameters['Synchrotron']['model'])
            print ('# GENERATING NEW SYNCHROTRON MAP'     )                                                                           
            Synchrotron.GenerateSynchrotron(simInfo,
                                            writeFile=writeFile,
                                        write=True)

        if simInfo.Parameters['Inputs']['sim_Synchrotron'] | simInfo.Parameters['Inputs']['read_Synchrotron']:
            writeFile = '{}/{}_Synchrotron_{}.fits'.format('./SKY_MODEL', simInfo.Parameters['Inputs']['sim_IDsky'],
                                                           simInfo.Parameters['Synchrotron']['model'])
            if rank == 0:
                if simInfo.Parameters['Mapping']['mode'].upper() == 'HEALPIX':
                    h = pyfits.open(writeFile)
                    nchans = len(h[1].columns)
                    Sky.Synchrotron = np.array(hp.read_map(writeFile, np.arange(simInfo.nchannels, dtype='i')))
                else:
                    Sky.Synchrotron = FileTools.ReadH5Py(writeFile)['maps']
                    nchans = Sky.Synchrotron.shape[0]

                if nchans != simInfo.nchannels:
                    print ('# REQUESTED BACKEND FREQUENCY CHANNELS DOES NOT EQUAL FREQUENCY CHANNELS OF SYNCHROTRON MAP')
                    raise ValueError
            else:
                Sky.Synchrotron = None

    except KeyError:
        pass

    ###############################
    # DO WE WANT A FREEFREE COMPONENT?
    ###############################       
    try:
        if simInfo.Parameters['Inputs']['sim_FreeFree'] and rank == 0:
            writeFile = '{}/{}_FreeFree_{}.fits'.format( './SKY_MODEL', 
                                                         simInfo.Parameters['Inputs']['sim_IDsky'],
                                                        simInfo.Parameters['FreeFree']['model'])
            print ('# GENERATING NEW FREE FREE MAP'  )                                                                                                                                                     
            FreeFree.GenerateFreeFree(simInfo,
                                      writeFile=writeFile,
                                      write=True)

        if simInfo.Parameters['Inputs']['sim_FreeFree'] | simInfo.Parameters['Inputs']['read_FreeFree']:
            writeFile = '{}/{}_FreeFree_{}.fits'.format('./SKY_MODEL', simInfo.Parameters['Inputs']['sim_IDsky'],
                                                        simInfo.Parameters['FreeFree']['model'])
            if rank == 0:
                if simInfo.Parameters['Mapping']['mode'].upper() == 'HEALPIX':
                    h = pyfits.open(writeFile)
                    nchans = len(h[1].columns)
                    Sky.FreeFree = np.array(hp.read_map(writeFile, np.arange(simInfo.nchannels, dtype='i')))
                else:
                    Sky.FreeFree = FileTools.ReadH5Py(writeFile)['maps']
                    nchans = Sky.FreeFree.shape[0]

                if nchans != simInfo.nchannels:
                    print ('# REQUESTED BACKEND FREQUENCY CHANNELS DOES NOT EQUAL FREQUENCY CHANNELS OF FREEFREE MAP')
                    raise ValueError

            else:
                Sky.FreeFree = None

    except KeyError:
        pass
                                                
    ###############################
    # DO WE WANT AN HI COMPONENT?
    ###############################             
    try:
        if simInfo.Parameters['Inputs']['sim_HI'] and rank == 0:
            writeFile = '{}/{}_HI_{}.fits'.format('./SKY_MODEL',simInfo.Parameters['Inputs']['sim_IDsky'],
                                                  simInfo.Parameters['HI']['model'])
            print ('# GENERATING NEW HI MAP'    )                                                                                                                                                                                                                                  
            HI.GenerateHI(simInfo,
                          simInfo.Parameters['HI']['model'],
                          simInfo.Parameters['HI']['ancil_dir'],
                          simInfo.Parameters['HI']['ancil_files'],
                          simInfo.maxFreq,
                          simInfo.minFreq,
                          simInfo.nchannels,
                          simInfo.Parameters['Beam']['fwhm'],
                          simInfo.Parameters['Mapping']['coordinate_system'],
                          simInfo.Parameters['Mapping']['hp_nside'],
                          writeFile=writeFile,
                          write=True)

        
    
        if simInfo.Parameters['Inputs']['sim_HI'] | simInfo.Parameters['Inputs']['read_HI']:
            writeFile = '{}/{}_HI_{}.fits'.format('./SKY_MODEL', simInfo.Parameters['Inputs']['sim_IDsky'],
                                                  simInfo.Parameters['HI']['model'])
            if rank == 0:
                h = pyfits.open(writeFile)
                nchans = len(h[1].columns)
                if nchans == simInfo.nchannels:
                    Sky.HI = np.array(hp.read_map(writeFile, np.arange(simInfo.nchannels, dtype='i')))
                else:
                    print ('# REQUESTED BACKEND FREQUENCY CHANNELS DOES NOT EQUAL FREQUENCY CHANNELS OF HI MAP')
                    raise ValueError                
            else:
                Sky.HI = None

    except KeyError:
        pass

    ###############################
    # DO WE WANT AN AME COMPONENT?
    ###############################     
    try:
        if simInfo.Parameters['Inputs']['sim_AME'] and rank == 0:
            writeFile = '{}/{}_AME_{}.fits'.format('./SKY_MODEL',simInfo.Parameters['Inputs']['sim_IDsky'],
                                                   simInfo.Parameters['AME']['model'])
            print ('# GENERATING NEW AME MAP'   )                                                                                                                                                                                                                                   
            AME.GenerateAME(simInfo,
                            writeFile,
                            write=True)
        

        if simInfo.Parameters['Inputs']['sim_AME'] | simInfo.Parameters['Inputs']['read_AME']:
            writeFile = '{}/{}_AME_{}.fits'.format('./SKY_MODEL', 
                                                   simInfo.Parameters['Inputs']['sim_IDsky'],
                                                   simInfo.Parameters['AME']['model'])
            if rank == 0:
                if simInfo.Parameters['Mapping']['mode'].upper() == 'HEALPIX':
                    h = pyfits.open(writeFile)
                    nchans = len(h[1].columns)
                    Sky.AME = np.array(hp.read_map(writeFile, np.arange(simInfo.nchannels, dtype='i')))
                else:
                    Sky.AME = FileTools.ReadH5Py(writeFile)['maps']
                    nchans = Sky.AME.shape[0]

                if nchans != simInfo.nchannels:
                    print ('# REQUESTED BACKEND FREQUENCY CHANNELS DOES NOT EQUAL FREQUENCY CHANNELS OF AME MAP')
                    raise ValueError
            else:
                Sky.AME = None

    except KeyError:
        pass

    ############################################
    # READ IN SKY MAP COMPONENTS
    ############################################
        
    #######
    # ADD A READ ONLY CLAUSE FOR THE FOREGROUNDS HERE
    #######
            
    # Bcast total sky to all nodes
            
    keys = Sky.data.keys()
    temp = np.zeros((simInfo.nchannels, simInfo.npix))
    for k in keys:
        if k == 'Total':
            continue        
        if rank == 0:
            print (k, Sky.data[k].shape)
            temp += Sky.data[k]
    
    comm.Barrier()
    comm.Bcast([temp, MPI.DOUBLE], root=0)
    Sky.Total = temp

    if simInfo.Parameters['Mapping']['mode'].lower() == 'cartesian':
        from pipeline.Tools import CartPix
        from scipy.interpolate import RectBivariateSpline
        
        naxis = np.array([simInfo.Parameters['Mapping']['ct_naxis1'],simInfo.Parameters['Mapping']['ct_naxis2']]).astype(int)
        cdelt = [simInfo.Parameters['Mapping']['ct_cdelt1'],simInfo.Parameters['Mapping']['ct_cdelt2']]
        crval = [simInfo.Parameters['Mapping']['ct_crval1'],simInfo.Parameters['Mapping']['ct_crval2']]
        _, glMap = CartPix.pix2ang(naxis, [-cdelt[0], cdelt[1]], crval, np.arange(naxis[0]) , np.zeros(naxis[0]))
        gbMap, _ = CartPix.pix2ang(naxis, [-cdelt[0], cdelt[1]], crval, np.zeros(naxis[1]) , np.arange(naxis[1]))
        glMap = np.linspace(crval[0] - cdelt[0]*naxis[0]/2., crval[0] + cdelt[0]*naxis[0]/2., naxis[0])
        gbMap = np.linspace(crval[1] - cdelt[1]*naxis[1]/2., crval[1] + cdelt[1]*naxis[1]/2., naxis[1])

        xpix, ypix = np.meshgrid(np.arange(naxis[0]), np.arange(naxis[1]), indexing='ij')
        theta,phi = CartPix.pix2ang(naxis, [cdelt[0], cdelt[1]],crval,  xpix, ypix)
        theta = theta.flatten()
        phi = phi.flatten()
        splines = []
        for i in range(Sky.Total.shape[0]):
            splines += [RectBivariateSpline(glMap,
                                            gbMap,
                                            (np.reshape(Sky.Total[i], (int(naxis[1]),int(naxis[0]))) )[:,:] )]
            # if i == 0:
            #     glAll, gbAll = np.meshgrid(glMap, gbMap)
            #     test = splines[0].ev(glAll, gbAll)
            #     from matplotlib import pyplot
            #     pyplot.scatter(glAll.flatten()[::10], gbAll.flatten()[::10], marker=',', c=test.flatten()[::10])
            #     pyplot.show()
            #splines += [RectBivariateSpline(np.linspace(crval[0]-cdelt[0]*naxis[0]/2.,crval[0]+cdelt[0]*naxis[0]/2., naxis[0]),
            #                                np.linspace(crval[1]-cdelt[1]*naxis[1]/2.,crval[1]+cdelt[1]*naxis[1]/2., naxis[1]),
            #                                np.reshape(Sky.Total[i], (int(naxis[1]),int(naxis[0]))).T )]
        Sky.splines = splines


    return Sky
