""" NAME :
	pca_script.py
    PURPOSE :
	Measure correlated signal in frequency in the data stream and clean it with PCA
    CALL :
	file_setup : instrument, sky configuration
		format : dat
	in_fname : cube with the observed maps by frequency range
		format : fits
    OUTPUT :
	out_fname : cube with the observed maps by frequency range, after removing modes
	INFO : 9 January 2014 - MABS   
"""
from __future__ import division
import numpy as np
import matplotlib.pyplot as pyplot
#from scipy.stats import ss
import scipy as sp
import numpy.ma as ma
import scipy.linalg as linalg
import healpy as hp

from matplotlib.colors import LogNorm
def ft(P,x):
    return P[0] + np.log10(1. + (P[2]/x)**P[1])

def errfunc(P,x,y):
    return y - ft(P,x)

from scipy.optimize import leastsq

from scipy.linalg import inv


class PCA():
    """
    Calculate eigenmode components of a datacube. 

    datacube shape should be (N_frequencies, N_pixels)

    zero values are assumed to be masked
    """

    def __init__(self, data, noise=None):

        self.data = data

        self.nfreqs = data.shape[0]
        self.npix   = data.shape[1]

        ############
        # PAD DATA #
        ############
        
        #self.bigdata = np.zeros((self.nfreqs*3, self.npix))
        #self.bigdata[:self.nfreqs,:] = self.data[::-1,:]
        #self.bigdata[self.nfreqs:self.nfreqs*2,:] = self.data
        #self.bigdata[self.nfreqs*2:,:] = self.data[::-1,:]

        

        #######################
        ###  Get covariance ###
        #######################
        self.covariance = np.array(self.CalcCovariance(self.data))
        #self.covariance = np.array(self.CalcCovariance(self.bigdata))

        #############################################
        ###  Get the eigenvalues and eigenvectors ###
        #############################################
        self.eigen_val, self.eigen_vect = linalg.eigh(self.covariance)
        self.eigen_val = self.eigen_val[::-1]


        # PCA Component Maps
        self.components = self.eigen_vect.T.dot(self.data)


        # Calculate noise components
        if not isinstance(noise, type(None)):
            self.CN = np.abs(np.array(self.CalcCovariance(noise)))
            self.CN[self.CN < np.max(self.CN)/1e6] = 0.
            # Invert CN and take the sqrt
            C2 = inv(np.sqrt(self.CN))
            N = C2.dot(self.covariance).dot(C2)

            # Calculate eigenvalues of N matrix
            self.N_eigen_val, self.N_eigen_vect = linalg.eigh(N)
            self.N_eigen_val = self.N_eigen_val[::-1]


    def GetMap(self, eigenmode):
        """
        Returns datacube with X eigenmodes removed.
        """
        return self.data - self.eigen_vect[:,self.nfreqs-eigenmode:].dot(self.components[self.nfreqs-eigenmode:,:])
        #d = self.bigdata - self.eigen_vect[:,self.nfreqs-eigenmode:].dot(self.components[self.nfreqs-eigenmode:,:])

        #return d[self.nfreqs:self.nfreqs*2,:]

    def CalcCovariance(self, data):
    
        mask = ma.getmaskarray(ma.masked_equal(data,0))  ### if needed mask, otherwise = False
        weights = np.logical_not(mask)

        cstd = ma.std(data, 1)
        cmean= ma.mean(data, 1)
        cube = (data.T - cmean).T
        
        cube = cube * weights
        
        covar = cube.dot(cube.T)
        counts_matrix = weights.dot(weights.T)

        counts_matrix[counts_matrix==0] = 1
        covar /= counts_matrix

        return covar

# def PCA(data_cube, mode, full_output=False, Plot=None, Fit=False, return_components=False):
#     """
#     Returns slice of eigen data for a given eigen mode id.

#     Kwargs: 

#     - full_output (False): return eigen_vals and eigen vectors as well
#     as the map
#     """
    
#     if type(mode) == int:
#         mode = [mode]
    

#     npix   = data_cube.shape[1]
#     nfreqs = data_cube.shape[0]
    
#     if np.max(mode) >= nfreqs:
#         print '# MODE PCA MODES REQUESTED THAN SKY MAP FREQUENCIES'
#         raise ValueError
        
#     #######################
#     ###  Get covariance ###
#     #######################
#     covar_matrix, counts_matrix = covariance_cube(data_cube)
#     counts_matrix[counts_matrix==0] = 1
#     covar_matrix = covar_matrix / counts_matrix

#     #############################################
#     ###  Get the eigenvalues and eigenvectors ###
#     #############################################
#     eigen_val, eigen_vect = linalg.eigh(covar_matrix)

#     eigen_val = eigen_val[::-1]

#     if Fit:
#         P0 = [np.log10(np.min(eigen_val)), 2., nfreqs/2]
#         x = np.arange(1, eigen_val.size+1)
#         P1,s  = leastsq(errfunc,P0,  args=(x[1:], np.log10(eigen_val[1:])))
#         ek = int(P1[-1]+1)
#         mode = np.arange(ek-1,ek+2)
#         if np.min(mode) < 0:
#             mode -= np.min(mode)

#     if not isinstance(Plot, type(None)):
#         pyplot.plot(x, eigen_val, label='Eigenvalues')
#         pyplot.plot(x, 10**ft(P1,x), label='Model', alpha=0.5, linestyle='-.')
#         pyplot.axvline(ek, color='k', linewidth=2, linestyle='--',label='Eigenvalue knee: {}'.format(ek))
#         pyplot.ylabel(r'Eigenvalue (K$^2$)')
#         pyplot.xlabel(r'$\lambda$')
#         pyplot.yscale('log')
#         pyplot.xscale('log')
#         pyplot.legend(loc='best')

#         pyplot.savefig(Plot[0], format='pdf')
#         pyplot.clf()


#     if return_components:
#         return eigen_val, eigen_vect, s

#     output_pca = np.zeros((len(mode), nfreqs, npix))        

#     s = eigen_vect.T.dot(data_cube)
#     for i,m in enumerate(mode):
#         #outputs = eigen_vect[:,:nfreqs-m].dot(s[:nfreqs-m,:])#data_cube - remove
#         output_pca[i,...] = data_cube - eigen_vect[:,nfreqs-m:].dot(s[nfreqs-m:,:])#outputs

#     if full_output:
# 	    return output_pca, eigen_vect, eigen_val[::-1], covar_matrix, int(np.min(mode)), int(np.max(mode))
#     else:
#         return output_pca
                
#         #modes = eigen_vect[:, nfreqs-m:]
# 	    # s = modes.T.dot(data_cube)
# 	    # remove = modes.dot(s)
