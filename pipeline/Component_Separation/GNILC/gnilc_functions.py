import numpy as np
import healpy as hp
import pyfits 
from pipeline.Tools import FileTools

def gaussbeam_ell_bands(beamsizes, lmax):

    """This function generates gaussian-shaped windows in spherical harmonic space, used for wavelet (needlet) analysis of spherical maps.
    """

    sortedbeamsizes = (np.sort(beamsizes))[::-1]

    sortedbeamsizes = sortedbeamsizes * 0.000290888209  # arcmin to rad

    nbands = beamsizes.size + 1
    
    bands = np.zeros((lmax + 1, nbands))

    # First band

    bands[:, 0] = (hp.sphtfunc.gauss_beam(sortedbeamsizes[0], lmax))**2

    # Intermediate bands

    if nbands > 2:
        for i in range(1, nbands - 1):
            bands[:, i] = (hp.sphtfunc.gauss_beam(sortedbeamsizes[i], lmax))**2 - (hp.sphtfunc.gauss_beam(sortedbeamsizes[i - 1], lmax))**2

    # Last band

    bands[:, nbands - 1] = 1.0 - (hp.sphtfunc.gauss_beam(sortedbeamsizes[nbands - 2], lmax))**2

    # Take the square root

    bands = np.sqrt(bands)    

    return bands

def cosine_ell_bands(bandcenters):

    """This function generates cosine-shaped windows in spherical harmonic space, used for wavelet (needlet) analysis of spherical maps.
    """

    lmax = max(bandcenters)
    nbands = bandcenters.size
    bands = np.zeros((lmax + 1, nbands))

    b = bandcenters[0]
    c = bandcenters[1]

    # Left part
    
    ell_left = np.linspace(0, b, b + 1, endpoint=True)
    if b > 0:
        bands[ell_left.astype(int), 0] = 1.0

    # Right part

    ell_right = b + np.linspace(0, c - b, c - b + 1, endpoint=True) 
    bands[ell_right.astype(int), 0] = (np.cos(((ell_right - b) / (c - b)) * (np.pi / 2.0)))

    bands_l_max = np.zeros(nbands) 
    bands_l_max[0] = c
    bands_l_max[-1]= lmax

    if nbands >= 3:
        for i in range(1, nbands - 1):
            a = bandcenters[i - 1]
            b = bandcenters[i]
            c = bandcenters[i + 1]
            
            bands_l_max[i] =  c
            # Left part
            ell_left = a + np.linspace(0, b - a, b - a + 1, endpoint=True)
            bands[ell_left.astype(int), i] = (np.cos(((b - ell_left) / (b - a)) * (np.pi / 2.0)))
            # Right part
            ell_right = b + np.linspace(0, c - b, c - b + 1, endpoint=True)
            bands[ell_right.astype(int), i] = (np.cos(((ell_right - b) / (c - b)) * (np.pi / 2.0)))

        a = bandcenters[nbands - 2]
        b = bandcenters[nbands - 1]
        # Left part
        ell_left = a + np.linspace(0, b - a, b - a + 1, endpoint=True)
        bands[ell_left.astype(int), nbands - 1] = (np.cos(((b - ell_left) / (b - a)) * (np.pi / 2.0)))

    return bands, bands_l_max





def alm2wavelets(alm, bands, bands_nside, lmax=None):

    """This function computes wavelets maps for each wavelet band using the input spherical harmonics coefficients."""

    # Calculate maximum l
    if isinstance(lmax, type(None)):
        lmax = 3 * bands_nside[-1] - 1


    nbands = bands.shape[1]
    npix   = 12*bands_nside**2

    nlm_tot = int(lmax * (lmax + 1.0) / 2.0 + lmax + 1.0)
        
    alm_bands = np.zeros(nlm_tot, dtype=np.complex128)
    index_in = np.zeros(nlm_tot)
    index_out = np.zeros(nlm_tot)

    # Start the band loop
    waveletList = list()# np.zeros((nbands, npix))
    for i in range(0, nbands):
        # Now filter, restricting the alm and index to the needed ones
        j = 0
        for l in range(0, lmax + 1):
            for m in range(0, l + 1):
                index_in[j]  = hp.Alm.getidx(lmax, l, m)
                index_out[j] = hp.Alm.getidx(lmax, l, m)
                j = j + 1

        for k in range(nlm_tot):
            alm_bands[index_out[k]] = alm[index_in[k]]

        alm_write = hp.sphtfunc.almxfl(alm_bands[0:nlm_tot], bands[0:lmax + 1, i])
        
        # Transform back to Healpix format

        #waveletList[i,:] = hp.sphtfunc.alm2map(alm_write, nside, verbose=False)
        waveletList += [hp.sphtfunc.alm2map(alm_write, bands_nside[i], verbose=False)]
    return waveletList


def localcovar(map1, map2, pixperscale):

    """Local covariance of two maps in the pixel space."""


    map = map1 * map2
    npix = map.size
    nside = hp.pixelfunc.npix2nside(npix)
    nsidecovar = nside

    # First degrade a bit to speed-up smoothing

    if (nside / 4) > 4:
        nside_out = nside / 4
    else:
        nside_out = 4

    stat = hp.pixelfunc.ud_grade(map, nside_out = nside_out, order_in = 'RING', order_out = 'RING')
    
    # Compute alm

    lmax = 3 * nside_out - 1
    nlm_tot = float(lmax) * float(lmax + 1.0) / 2.0 + lmax + 1.0
    alm = hp.sphtfunc.map2alm(stat, lmax=lmax, iter=1, use_weights=True)

    # Find smoothing size

    pixsize = np.sqrt(4.0 * np.pi / npix)
    fwhm = pixperscale * pixsize
    bl = hp.sphtfunc.gauss_beam(fwhm, lmax)

    # Smooth the alm

    alm_s = hp.sphtfunc.almxfl(alm, bl)
    
    # Back to pixel space 
 
    stat_out = hp.sphtfunc.alm2map(alm_s, nsidecovar, verbose=False)
    return stat_out


def whitening_matrix(R):

    """This function computes the whitening matrix W of positive definite square matrix R (the Cholesky decomposition of the invert: R^{-1}=W^t*W so that W*R*W^t=I).
    """

    evec0, eval0, V = np.linalg.svd(R, full_matrices=True)
    D = np.diagflat(1./np.sqrt(eval0))
    W = D.dot(evec0.T)

    return W



def multi_ilc(Ls_minus, Ps, wmm, covar):

    """Computes multidimensional ILC weights."""

    # ILC weigths matrix

    
    B = Ls_minus.dot(wmm)
    #c = Ps.T.dot(np.linalg.inv(covar)).dot(Ps)
    c = Ps.T.dot(np.linalg.solve(covar, Ps))
    D = B.T.dot(c)
    #C = np.linalg.inv(D.dot(B))
    #D = np.transpose(np.matrix(Ls_minus) * np.matrix(O)) * np.transpose(np.matrix(Ps)) * np.linalg.inv(np.matrix(covar)) * np.matrix(Ps))

    A = B.dot(np.linalg.solve(D.dot(B), D))#.dot(C).dot(D)


    #B = np.array(np.matrix(Ls_minus) * np.matrix(O) * np.linalg.inv(np.transpose(np.matrix(Ls_minus) * np.matrix(O)) * np.transpose(np.matrix(Ps)) * np.linalg.inv(np.matrix(covar)) * np.matrix(Ps) * np.matrix(Ls_minus) * np.matrix(O)) * np.transpose(np.matrix(Ls_minus) * np.matrix(O)) * np.transpose(np.matrix(Ps)) * np.linalg.inv(np.matrix(covar)) * np.matrix(Ps))

    return A


def wavelets2map(bands, waveletMaps, nside):

    """This function combines wavelets maps into a single map (procedure reverse of map2wavelets)."""
    
    # Get synthesis windows

    #windows = wavelets[0]#pyfits.getdata(wavelets, 0)
    nbands = bands.shape[1]# (windows[:,0]).size

    # Create output alm, alm index and ell values

    lmax = bands.shape[0] - 1
    nalm = hp.sphtfunc.Alm.getsize(lmax)

    alm_out= np.zeros(nalm, dtype=np.complex128)

    for i in range(nbands):
        map = waveletMaps[i]#pyfits.getdata(wavelets, i)
        wh = np.where(bands[:,i] != 0)
        if max(max(wh)) < lmax:
            band_lmax = max(max(wh))
        else:
            band_lmax = lmax



        alm = hp.sphtfunc.map2alm(map, lmax=band_lmax, iter=1, use_weights=True)

        alm_win = hp.sphtfunc.almxfl(alm, bands[:band_lmax + 1,i])

        for l in range(band_lmax + 1):
            for m in range(l + 1):
                ind_0 = hp.sphtfunc.Alm.getidx(band_lmax, l, m)
                ind_1 = hp.sphtfunc.Alm.getidx(lmax, l, m)
                alm_out[ind_1] = alm_out[ind_1] + alm_win[ind_0]

    # Make map

    map_o = hp.sphtfunc.alm2map(alm_out, nside, verbose=False)

    return map_o
