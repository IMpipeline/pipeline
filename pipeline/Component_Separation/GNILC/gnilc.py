import numpy as np
import healpy as hp
import pyfits
import subprocess
import gnilc_functions
import time

from matplotlib import pyplot
from mpi4py import MPI
from pipeline.Tools import FileTools

d2r = np.pi/180.

def CosineBands(bandcenters, fwhm, nside_max):
    '''
    Returns series of  for each bandcenter.

    Arguments:
    bandcenters - Width of each needlet (Typically max size is 3*nside + 1)
    '''
    beam_out = hp.sphtfunc.gauss_beam(fwhm*d2r, 10000)   # output beam transfer function - limits the effective ell max of the maps
    wl = max(np.where(beam_out >= 7.0*10**(-12))[0])   # effective ell max of the max
    bandcenters = np.asarray(bandcenters)
    bandcenters = bandcenters[np.where(bandcenters <= wl)]
    
    bands, bands_l_max = gnilc_functions.cosine_ell_bands(bandcenters)

    #Band max is the highest l that a particular wavelet goes out to.

    nsides = (bands_l_max + 1.)/3.
    bands_nside = np.ceil(np.log(nsides)/np.log(2) + 1)
    bands_nside = 2.**bands_nside
    bands_nside[(bands_nside > nside_max)] = nside_max

    return bands, bands_l_max.astype('i'), bands_nside.astype('i')



def GenerateHIWavelets(hi_maps, bands, bands_nside):
    """
    
    """
            
    npix   = hi_maps.shape[1]
    nside  = int(np.sqrt(npix/12.))
    nfreqs = hi_maps.shape[0]
    lmax   = 3*nside - 1
    nbands = bands.shape[1]

    re = np.zeros((nfreqs, lmax * (lmax + 3.0) / 2.0 + 1.0))
    im = np.zeros((nfreqs, lmax * (lmax + 3.0) / 2.0 + 1.0))
    nlm_tot = lmax * (lmax + 1.0) / 2.0 + lmax + 1.0   # number of terms on alm
    alm_21cm = np.zeros((nlm_tot), dtype=np.complex128)

    cl21 = np.zeros((lmax + 1, nfreqs, nfreqs))

    for i in range(nfreqs):
        cl21[:, i, i] = hp.anafast(hi_maps[i, :], lmax=lmax, iter=1, use_weights=True)
        for k in range(0, i):
            cl21[:, i, k] = hp.anafast(hi_maps[i, :], map2=hi_maps[k, :], lmax=lmax, iter=1, use_weights=True)
            cl21[:, k, i] = cl21[:, i, k]

    Svbig = np.zeros((nfreqs, lmax * (lmax + 3.0) / 2.0 + 1.0, 2))
    for l in range(lmax + 1):
        U, S, V = np.linalg.svd(cl21[l, :, :], full_matrices=True)
            
        Sbig = np.repeat(S, (l+1)*2)
        Sbig = np.reshape(Sbig, (nfreqs, l+1, 2))

        almi =  np.sqrt(Sbig) * np.random.randn(nfreqs, l + 1.0, 2) / np.sqrt(2.0)
        
        mvalues = np.arange(l+1, dtype='i')
        indexes = hp.sphtfunc.Alm.getidx(lmax, l, mvalues)

        Svbig[:, indexes, :] = almi
        
        re[:, indexes] = U.dot(Svbig[:, indexes, 0])
        im[:, indexes] = U.dot(Svbig[:, indexes, 1])

    hiWavelets =  [np.zeros((nfreqs, 12*bands_nside[i]**2)) for i in range(nbands)]
    for i in range(nfreqs):
        alm_21cm[:] = re[i,:] + im[i,:]*1j
        output = gnilc_functions.alm2wavelets(alm_21cm, bands, bands_nside, lmax=lmax)
        for j in range(len(output)):
            hiWavelets[j][i,:] = output[j]

    return hiWavelets 

def GenerateInputWavelets(input_maps, bands, bands_nside):
    npix   = input_maps.shape[1]
    nside  = int(np.sqrt(npix/12.))
    nfreqs = input_maps.shape[0]
    lmax   = 3*nside - 1
    nbands = bands.shape[1]

    inputWavelets = [np.zeros((nfreqs, 12*bands_nside[i]**2)) for i in range(nbands)]
    for i in range(nfreqs):
        alm_map = hp.map2alm(input_maps[i,:], lmax=lmax, iter=1, use_weights=True)

        output = gnilc_functions.alm2wavelets(alm_map, bands,  bands_nside, lmax=lmax)
        for j in range(len(output)):
            inputWavelets[j][i,:] = output[j]
    
    return inputWavelets



def gnilc(input_maps, hi_maps, mask, fwhm, needlet_type = 'cosine',bandcenters=None,  bias=0.03, output_fits=False, write_wavelets=False, write_wavelets_filename='wavelets.hdf5'):
    """
    Generalised Needlet Internal Linear Combination (GNILC) is a
    component seperation algorithm detailed in Olivari et al. 2015.

    This implementation is fully MPI ready. Expects Node 0 to hold all the maps.

    Arguments:
    input_maps - The total measured emission measured, shape(nfreq, npixels)
    hi_maps    - The model HI maps of the expected signal, shape(nfreq, npixels)
    mask       - Mask to cover bright sources or unobserved areas of the sky, shape(npixels)
    fwhm       - Full width half maximum (degrees)

    Keywords:
    needlet_type - Type of needlet to use (cosine: Cosine (recommended), gauss: Gaussian)
    bandcenters  - Width of each needlet (default: None, assumes max band is 3*nside + 1)
    bias         - Not sure what this does? (default: 0.03)
    output_fits  - Write output to file (default: False)

    """

    # Setup the MPI functions
    comm = MPI.COMM_WORLD
    rank = comm.rank
    size = comm.size

    if rank >= 0:

        mask = mask.astype('f') # Make sure the mask is a float array

        if rank == 0:
            print ('GNILC: STARTING rank({})'.format(rank))

        # Setup some constants that are shared over all nodes
        npix   = input_maps.shape[1]    #N pixels in the input maps
        nside  = int(np.sqrt(npix/12.)) #HEALPix nside of the input maps ... 
        nfreqs = input_maps.shape[0]    #Number of frequency channels input
        lmax   = 3*nside - 1            #Maximum multipole of input maps


        if isinstance(bandcenters, type(None)):
            maxPower = np.log(3*nside - 1)/np.log(2)
            bandcenters = 2**np.append(np.arange(1,int(maxPower)+1), [maxPower])
            #bandcenters[(bandcenters < 8)] = 8
        bandcenters[0] = 0
        
        #Bands contains the C_l values for each wavelet band, and bands_max is the multipole of the peak response for each wavelet
        if needlet_type == 'cosine':
            bands, bands_max, bands_nside = CosineBands(bandcenters, fwhm, nside)
            bands_npix = 12*bands_nside**2 #Just helpful to have!
        else:
            bands, bands_max = GaussianBands(bandcenters, fwhm)
        nbands = bands.shape[1] #How many bandcenters are to be calculated.

        #Need to have a mask for each of the wavelet nsides
        masks = list()
        for n in bands_nside:
            tmp = hp.ud_grade(mask, n)
            tmp[tmp >= 0.9] = 1.
            tmp[tmp < 0.9]  = 0.
            masks += [tmp]

                
        #Calculate the 'istart' value
        istart = np.zeros(nfreqs)

        # Calculate the wavelets of input maps         
        cl21 = np.zeros((lmax + 1, nfreqs, nfreqs))

        #This will be a nested list of the form: [Band][Frequency,PIXEL]
        inputWavelets = GenerateInputWavelets(input_maps, bands, bands_nside)
        hiWavelets    = GenerateHIWavelets(hi_maps, bands, bands_nside)
        if write_wavelets:
            wavedict = {'{}'.format(i): inputWavelets[i][:, :bands_npix[i]]*masks[i] for i in range(nbands)}
            FileTools.WriteH5Py(write_wavelets_filename, wavedict)


        #So we now high inputWavelets, and hiWavelets of form [Band][Frequency,Pixel]
        #All frequencies for a given wavelet band will have the same nside

        #Containers for covariance matrices
        Rq_map = np.zeros((np.max(bands_npix), nfreqs, nfreqs)) 
        Rq_hi  = np.zeros((np.max(bands_npix), nfreqs, nfreqs))

        total_map1_band = np.zeros(np.max(bands_npix))
        total_map2_band = np.zeros(np.max(bands_npix))
        hi_map1_band = np.zeros(np.max(bands_npix))
        hi_map2_band = np.zeros(np.max(bands_npix))

        nmodes_band = np.zeros(nbands)

        w_21 = np.zeros((np.max(bands_npix), nfreqs, nfreqs))   # ILC weight
        freqIndexes = np.arange(nfreqs, dtype='int')
        w_21[:, freqIndexes, freqIndexes] = 1. #Set all diagonals to 1 by default and zero for cross-terms
        w21List = [np.zeros((bands_npix[i], nfreqs, nfreqs)) for i in range(nbands)]#[Band][Frequency, Pixel]

        #Akaike Information Criterion Container
        AIC = np.zeros(nfreqs)          
        Un  = np.zeros((nfreqs, nfreqs))


        if rank ==0:
            print ('GNILC: MAIN LOOP rank({})'.format(rank))
        #Now we need to perform the main loop
        for i in range(nbands):
            lmodes = 0.
            for l in range(bands_max[-1]):
                lmodes = lmodes + (2.0 * l + 1.0) * bands[l, i]**2
            
            if rank == 0:
                print ('BAND {}'.format(i))

            #Generate covariance matrices of data and HI signal
            for j in range(nfreqs):
                total_map1_band[:bands_npix[i]] = inputWavelets[i][j, :] * masks[i]
                hi_map1_band[:bands_npix[i]]    = hiWavelets[i][j,:] * masks[i]

                startTime = time.time()
                for k in range(0, j + 1):
                    total_map2_band[:bands_npix[i]] = inputWavelets[i][k, :] * masks[i]
                    hi_map2_band[:bands_npix[i]]    = hiWavelets[i][k, :] * masks[i]
                    

                    pps = np.sqrt(bands_npix[i] * (nfreqs - 1) / (bias * lmodes))

                    cov_map = gnilc_functions.localcovar(total_map1_band[:bands_npix[i]], 
                                                          total_map2_band[:bands_npix[i]], 
                                                          pps)

                    cov_hi  = gnilc_functions.localcovar(hi_map1_band[:bands_npix[i]]   , 
                                                          hi_map2_band[:bands_npix[i]]   , 
                                                          pps)



                    Rq_map[:bands_npix[i], j, k] = cov_map
                    Rq_map[:bands_npix[i], k, j] = cov_map
                    Rq_hi[:bands_npix[i], j, k]  = cov_hi
                    Rq_hi[:bands_npix[i], k, j]  = cov_hi
            #Solve for each pixel
            gdpix = np.where((masks[i] != 0))[0]
            for p in np.arange(masks[i].size, dtype='int64'):

                covar = Rq_map[p, :, :]
                covar_hi = Rq_hi[p,:,:]

                inv_sq_root_hi = gnilc_functions.whitening_matrix(covar_hi)  # inverse square root hi covariance matrix
                white_covar= (inv_sq_root_hi.dot(covar)).dot(inv_sq_root_hi.T)# whitened total matrix - eigenvalues ~ 1 corresponds to the HI signal


                evecr, evalr, Vr = np.linalg.svd(white_covar, full_matrices=True)

                #AIC CALCULATING STUFF
                fun = evalr - np.log(evalr) - 1.0 #Something to do with the eigenvalues... PCA stuff i guess?
                total = np.sum(fun)
                for j in range(1, nfreqs+ 1):
                    if j < nfreqs:
                        total = total - fun[j - 1]
                        AIC[j - 1] = 2 * j + total
                    else:
                        AIC[j - 1] = 2 * j

                aics = np.where(AIC == np.min(AIC[:nfreqs-1]))[0]
                if len(aics) > 0:
                    n_dim = max(aics) + 1  # foregrounds plus noise degrees fo freedom
                else:
                    n_dim = 0


                if nfreqs - n_dim == 0:
                    w_21[p, :, :] = 0.
                else:

                    #Now to solve the ILC problem!
                    Un[:, 0:nfreqs-n_dim] = evecr[:, n_dim:]
                    Dn  = np.diagflat(np.sqrt(evalr[n_dim:]))
                    iDn = np.diagflat(np.sqrt(1./evalr[n_dim:]))


                    iDnUnT = iDn.dot(Un[:, 0:nfreqs-n_dim].T)
                    
                    ortho = np.transpose(iDnUnT.dot(evecr[:, n_dim:]))
                    ortho = ortho/np.linalg.norm(ortho, ord=np.inf)
                
                    Ln =  iDnUnT.dot(inv_sq_root_hi)
                    Ln_minus =np.linalg.solve(inv_sq_root_hi, Un[:, 0:nfreqs-n_dim].dot(Dn) )
                    Pn = Ln_minus.dot(Ln)
                    w_21[p, :, :] = gnilc_functions.multi_ilc(Ln_minus, Pn, ortho, covar)

            w21List[i][...] = w_21[:bands_npix[i], :, :] 

            w_21[...] = 0.
            w_21[:, freqIndexes, freqIndexes] = 1. #Set all diagonals to 1 by default and zero for cross-terms

            Rq_map[...] = 0.
            Rq_hi[...] = 0.
            total_map1_band[...] = 0.
            total_map2_band[...] = 0.
            hi_map1_band[...] = 0.
            hi_map2_band[...] = 0.

        
        # Apply GNILC weights to wavelets
        needletILCMaps = [np.zeros((nfreqs, bands_npix[i])) for i in range(nbands)]
        #needletBkgdMaps = [np.zeros((nfreqs, bands_npix[i])) for i in range(nbands)]
        #for i in range(nbands):
        #    pixsize = 4*np.pi/bands_npix[i]
        #    for j in range(nfreqs):


        for i in range(nbands):

            pixsize = 4*np.pi/bands_npix[i]

            for j in range(nfreqs):
                for k in range(nfreqs):
                    total_map1_band[:bands_npix[i]] = inputWavelets[i][k, :] 

                    needletILCMaps[i][j, :] = needletILCMaps[i][j, :] +  w21List[i][:, j, k] * total_map1_band[:bands_npix[i]]

        ilc_map_21 = np.zeros((nfreqs, npix))
        for i in range(nfreqs):
            ilc_map_21[i,:] = gnilc_functions.wavelets2map(bands, [needletILCMaps[j][i,:bands_npix[j]] for j in range(nbands)], nside)*mask

        return ilc_map_21

if __name__ == "__main__":

    maps= np.array(hp.read_map('/local/scratch/sharper/IM_Simulations/workingDir/hi_input_maps_3bands_30arcmin.fits', (0,1,2,3,4,5)))
    mask = hp.read_map('/local/scratch/sharper/IM_Simulations/workingDir/mask_input_maps.fits')
    hi= np.array(hp.read_map('/local/scratch/sharper/IM_Simulations/workingDir/total_input_maps_3bands_30arcmin.fits', (0,1,2,3,4,5)))

    gnilc(maps, hi, mask, 0.5)
