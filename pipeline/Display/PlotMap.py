# Create quick-view plots of the TODs from the BINGO pipeline
# Mike Peel, 16 August 2018 - v1

from pipeline.Tools import FileTools
import pprint
import numpy as np
import healpy as hp
import matplotlib.pyplot as plt

def prettyPrint(variable):
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(variable)

def plotMap(infile='',output_prefix='plotMap_'):

	if infile == '':
		print 'No input file defined!'
		return
	dataset = FileTools.ReadH5Py(infile)
	prettyPrint(dataset)

	# Hits
	hp.mollview(dataset['HITS'], xsize=2000)
	plt.savefig(output_prefix+'hits.jpg')

	# Maps and weights
	nummaps = np.shape(dataset['MAPS'])[0]
	for i in range(0,nummaps):
		hp.mollview(dataset['MAPS'][i], xsize=2000)
		plt.savefig(output_prefix+'map_'+str(i)+'.jpg')
		hp.mollview(dataset['WEIGHTS'][i], xsize=2000)
		plt.savefig(output_prefix+'weights_'+str(i)+'.jpg')
	return

if __name__ == "__main__":

	print "Use as, e.g., plotMap(infile='BINGO_Simulation_BINGO_Simulation_ALL_sim_SkyTOD_Realisation0.hdf5',output_prefix='BINGO_test_')"
	exit()