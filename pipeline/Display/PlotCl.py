# Create quick-view plots of the TODs from the BINGO pipeline
# Mike Peel, 16 August 2018 - v1

from pipeline.Tools import FileTools
import pprint
import numpy as np
import matplotlib.pyplot as plt

def prettyPrint(variable):
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(variable)

def plotCl(infile='',output_prefix='plotCl_'):

	if infile == '':
		print 'No input file defined!'
		return
	dataset = FileTools.ReadH5Py(infile)
	prettyPrint(dataset)

	dpi = 90
	fig1 = plt.figure(1, figsize=(1000/dpi, 600/dpi), dpi=dpi)
	numplots = np.shape(dataset['cl'])[0]
	for i in range(0,numplots):
		plt.xlabel('l')
		plt.ylabel('Value')
		plt.plot(dataset['l'],dataset['cl'][i], label='Cl')
		plt.savefig(output_prefix+'Cl'+str(i)+'.png')
		plt.clf()
	return

if __name__ == "__main__":
	
	print "Use as, e.g., plotCl(infile='HI_Cl_Model_980-1260MHz_2Channels_.hdf5',output_prefix='test_')"
	exit()
