from pipeline.Tools import FileTools
import pprint
import numpy as np
import matplotlib.pyplot as plt

def prettyPrint(variable):
    pp = pprint.PrettyPrinter(indent=4)
    pp.pprint(variable)

def plotTOD(infile='',output_prefix='plotTOD_'):

	if infile == '':
		print 'No input file defined!'
		return
	dataset = FileTools.ReadH5Py(infile)
	prettyPrint(dataset)

	dpi = 90
	fig1 = plt.figure(1, figsize=(1000/dpi, 600/dpi), dpi=dpi)
	plt.xlabel('i')
	plt.ylabel('Value')
	try:
		plt.plot(dataset['AZ'], label='Azimuth')
		plt.plot(dataset['EL'], label='Elevation')
		plt.plot(dataset['RA'], label='RA')
		plt.plot(dataset['DEC'], label='Declination')
		plt.plot(dataset['PANG'], label='Position angle')
		plt.savefig(output_prefix+'pointing.png')
		plt.clf()
	except:
		print 'There was a problem with the pointing plot!'
	try:
		plt.plot(dataset['JD'], label='JD')
		plt.savefig(output_prefix+'JD.png')
		plt.clf()
	except:
		print 'There was a problem with the JD plot!'
	try:
		print len(dataset['PIXELS'][0])
		plt.plot(dataset['PIXELS'][0], label='pixels')
		plt.savefig(output_prefix+'pixels.png')
		plt.clf()
	except:
		print 'There was a problem with the pixels plot!'
	try:
		print len(dataset['TOD'])
		print len(dataset['TOD'][0])
		plt.plot(dataset['TOD'][0], label='TOD')
		plt.savefig(output_prefix+'TOD.png')
		plt.clf()
	except:
		print 'There was a problem with the TOD plot!'
	return

if __name__ == "__main__":
	
	print "Use as, e.g., plotTOD(infile='Example1Main_example1_outputs_TOD-DATA_realisation0_FileNo0000.hdf5',output_prefix='test_')"
	exit()
