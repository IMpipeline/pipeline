import numpy as np

from matplotlib import pyplot

def RemoveAtmosphere(simInfo, TOD_All, TOD_Coordinates):
    
    # first we want to know the scan mode:
    obsMode = simInfo.Parameters['Observations']['mode']
    
    # Then for each mode there will be different criteria:
    if 'horizonraster' in obsMode.lower():
        pass
    elif 'circularazel' in obsMode.lower():
        pass
    elif 'lissajousazel' in obsMode.lower():
        pass
    else:
        print('No recognised observing mode for analysis pipeline')
        raise TypeError
        
    